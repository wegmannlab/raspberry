#include "snpDB.h"
#include "data.h"
#include "hmm.h"
#include "TLog.h"
#include "TParameters.h"
#include "modelParams.h"
#include "TAnalyzer.h"
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>


//---------------------------------------------------------------------------
void showExplanations(){
	std::cout << "\n This program estimates the probability of an ancestry "
              << "\n switch between two adjascent SNPs based on the HAPMIX "
		      << "\n model. It takes the following parameters (optional    "
		      << "\n parameters in brackets):                              "
              << "\n           1 : name of the input file                  "
              << "\n          (2): parameters on the command line in the   "
              << "\n               format tag=value. This will override    "
              << "\n               parameters given in the input file.     " << std::endl << std::endl;
}

//---------------------------------------------------------------------------
//function inferring switch points using an HMM algorithm
void runswitchPointInference(TParameters* myInputfile, TLog * logfile){
	logfile->startIndent("Inferring switch points and / or ancestries:");
	//read model parameters
	modelParams* myModelParams = new modelParams(myInputfile, logfile);
	myModelParams->buildParameterCombinations();

	//read SNP DB -> a bim file containing physical distance, genetic distance and the alleles for each SNP
	snpDB* mySnpDB=new snpDB(myInputfile->getParameterString("bimFile"), logfile);

	//read reference population data -> automatically only keeps SNPs which are in the SNP DB and encodes alleles 0/1/-9
	logfile->startIndent("Reading reference populations:");
	data** refpop=new data*[myModelParams->nRefpop];
	std::string filename;
	for(int i=0; i<myModelParams->nRefpop; ++i){
		filename=myInputfile->getParameterString("refPop"+ toString(i+1));
		if(readAfterLast(filename, '.')!="phased") throw ".phased format required for all reference populations!";
		refpop[i]=new data(mySnpDB, filename, logfile);
		if(refpop[i]->genotypingRate<1.0) throw "Missing data in reference population " + toString(i+1) + "!";
	}
	logfile->endIndent();

	//read admixed data -> keeps only SNPs which are in the SNP DB automatically and encodes alleles 0/1/-9
	logfile->startIndent("Reading data of admixed individuals:");
	filename=myInputfile->getParameterString("admPop");
	std::string ext=readAfterLast(filename, '.');
	if(ext!="ped" && ext!="tped" && ext!="phased") throw "ped, tped or phased format required for admixed population!";
	data* admpop=new data(mySnpDB, filename, logfile);
	logfile->endIndent();

	//get list of common SNPs in proper order and for proper chromosome
	logfile->startIndent("retaining common SNPs:");
	std::map<long,long> snpMap; //<position,id>
	std::map<long,long>::iterator mapIt, toDel;
	std::string chr = myInputfile->getParameterString("chr");
	logfile->list("Restricting to SNPs on chromosome '" + chr + "'");
	mySnpDB->fillSnpMap(&snpMap, chr);

	//test if the SNPs are present in all three data sets, otherwise remove
	clock_t start=clock();
	logfile->listFlush("Intersecting data ...");
	mapIt=snpMap.begin();
	bool test;
	while(mapIt!=snpMap.end()){
		test=true;
		for(int i=0; i<myModelParams->nRefpop; ++i) test*=refpop[i]->snpIsTyped(mapIt->second);
		if(!test || !admpop->snpIsTyped(mapIt->second)){
			toDel=mapIt;
			++mapIt;
			snpMap.erase(toDel);
		} else ++mapIt;
	}
	logfile->write(" done (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)!");
	logfile->conclude(toString(snpMap.size()) + " SNPs kept for further analysis");
	if(snpMap.size()<2) throw "Less than 2 SNPs remain after intersecting the data!";
	logfile->endIndent();

	//now build up condensed structure and store data in a better way
	start=clock();
	logfile->listFlush("Build data structure for HMM ...");
	int* popsizes=new int[myModelParams->nRefpop];
	for(int i=0; i<myModelParams->nRefpop; ++i) popsizes[i]=2*refpop[i]->nIndividuals;
	hmm* myHMM=new hmm(myModelParams->nRefpop, popsizes, admpop->nIndividuals, &snpMap, logfile);
	delete[] popsizes;
	long snp=0;
	for(mapIt=snpMap.begin();mapIt!=snpMap.end();++mapIt,++snp){
		for(int i=0; i<myModelParams->nRefpop; ++i) refpop[i]->fillHaplotypeArray(myHMM->getHaplotypeStoragePointer(i, snp), mapIt->second);
		admpop->fillGenotypeArray(myHMM->getAdmixpopGenotypeStoragePointer(snp), mapIt->second);
	}
	logfile->write(" done (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)!");

	//set prior on recombination rates
	if(myInputfile->parameterExists("ratesFile")) myHMM->setGeneticDistance(myInputfile->getParameterString("ratesFile"), myInputfile->getParameterDouble("defaultRate"));
	else myHMM->setGeneticDistance(myInputfile->getParameterDouble("defaultRate"));

	//Collapse states
	myHMM->collapsReferencePopulations(myInputfile->getParameterDouble("collapsingDistance"), myInputfile->getParameterDouble("maxNTypes", false));

	//free some memory.... and use it again...
	for(int i=0; i<myModelParams->nRefpop; ++i) delete refpop[i];
	delete[] refpop;
	delete admpop;
	int recomputeWindowSize=1;
	if(myInputfile->parameterExists("recomputeWindowSize")) recomputeWindowSize=myInputfile->getParameterDouble("recomputeWindowSize");
	myHMM->initializeHMMvariables(recomputeWindowSize);

	//now run through all parameter combinations and launch the HMM for each individual
	bool onlyLL=false;
	if(myInputfile->parameterExists("onlyLL")) onlyLL=true;
	bool doComputeSwitchProb=true;
	if(myInputfile->parameterExists("skipSwitchPointDetection")) doComputeSwitchProb=false;
	myModelParams->runAllParameterCombinations(myHMM, myInputfile->getParameterString("outputPrefix"), onlyLL, doComputeSwitchProb, mySnpDB);

	//clean up
	start=clock();
	logfile->listFlush("Cleaning up memory ...");
	delete myModelParams;
	delete myHMM;
	delete mySnpDB;
	logfile->write(" done (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)!");
}
//---------------------------------------------------------------------------
int main(int argc, char* argv[]){
	struct timeval start, end;
	gettimeofday(&start, NULL);

	TLog logfile;
	logfile.newLine();
	logfile.write(" RASPberry ");
	logfile.write("***********");

	try {
		if(argc<2) throw "Wrong number of arguments!";
		//read parameters from the command line
		TParameters myInputfile(argc, argv, &logfile);

		//verbose?
		bool verbose=myInputfile.parameterExists("verbose");
		logfile.setVerbose(verbose);

		//open log file that handles the output
		std::string  logFilename=myInputfile.getParameterString("logFile", false);
		if(logFilename.length()>0){
			logfile.openFile(logFilename.c_str());
			logfile.writeFileOnly(" RASPberry ");
			logfile.writeFileOnly("***********");
		}

		if(!verbose) logfile.listNoFile("Running in silent mode (use 'verbose' to get a status report on screen)");

		if(myInputfile.parameterExists("suppressWarnings"))
			logfile.suppressWarings();


		//switch among tasks
		std::string task=myInputfile.getParameterString("task");
		if(task=="inferSwitchPoints") runswitchPointInference(&myInputfile, &logfile);
		else {
			//is it a task from the analyzer?
			TAnalyzer analyzer(&myInputfile, &logfile);
			if(task=="compileMap") analyzer.produceMaps();
			else if(task=="compileSwitchPointDB") analyzer.produceIntervalDB();
			//tools
			else if(task=="import") analyzer.importMap();
			else if(task=="update") analyzer.updateMap();
			else if(task=="correlation") analyzer.computeCorrelations();
			else if(task=="twoWayCorrelation") analyzer.computeTwoWayCorrelations();
			else if(task=="populationAncestries") analyzer.calculatePopulationAncestry();
			else if(task == "callAncestries") analyzer.callAncestries();
			/*
			else if(task=="HOTSPOT") analyzer.produceHotspotUsage();
			else if(task=="HOTSPOT_EB") analyzer.produceHotspotUsageEmpiricalBayes();
			else if(task=="CALL") analyzer.callSwitchpoints();
			else if(task=="ENRICHMENT") analyzer.computeEnrichment();

			else if(task=="ANCESTRIES") analyzer.compileAncestries();
			else if(task=="ANCESTRIES_INTERVAL") analyzer.compileAncestriesInIntervals();
			else if(task=="ASSOCIATION") analyzer.associationTest();
			else if(task=="AVERAGE_ANCESTRY") analyzer.getIndividualAverageAncestry();
			else if(task=="ANCESTRY_MAP") analyzer.getAncestryMap();
			//else if(task=="DIFFERENCES") analyzer.writeLargesDifferences();
			*/
			else throw "Unknown task '" + task +"'!";
		}
	}
	catch (std::string & error){
		logfile.error(error);
		showExplanations();
	}
	catch (const char* error){
		logfile.error(error);
		showExplanations();
	}
	catch (...){
		logfile.error("unhandeled error!");
		showExplanations();
	}

	//finish
	logfile.clearIndent();
	gettimeofday(&end, NULL);
	float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	logfile.list("Program terminated in ", runtime, " min!");
	logfile.close();
    return 0;
}


