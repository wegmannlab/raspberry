# RASPberry #

RASPberry us a software to infer recombination maps from admixed individuals, along with the ancestries along a chromosome. To obtain the source of this program, navigate to the [Downloads page](https://bitbucket.org/WegmannLab/raspberry/downloads) (see menu on left) and download the full repository. It will contain a folder "manual" that contains a pdf of the manual describing how to compile and use RASPberry in great detail.

**Citation:**

Wegmann D, Kessner D, Veeramah KR, Mathias RA, Nicolae DL, Yanek LR, Sun YV, Torgerson DG, Rafaels N, Mosley T, Becker LC, Ruczinski I, Beaty TH, Kardia SLR, Meyers DA, Barnes KC, Becker DM, Freimer N and Novembre J (2011). Recombination rates in admixed individuals identified by ancestry-based inference. Nat Genet 43:9 847-853.