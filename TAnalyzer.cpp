/*
 * TAnalyzer.cpp
 *
 *  Created on: Dec 30, 2010
 *      Author: wegmannd
 */

#include "TAnalyzer.h"

TAnalyzer::TAnalyzer(TParameters* Parameters, TLog* Logfile){
	myParameters=Parameters;
	logfile = Logfile;
	start = -1;
	end = -1;
	increment = -1;

	//initialize random generator
	logfile->listFlush("Initializing random generator ...");
	if(myParameters->parameterExists("fixedSeed")){
		randomGenerator=new TRandomGenerator(myParameters->getParameterLong("fixedSeed"), true);
	} else if(myParameters->parameterExists("addToSeed")){
		randomGenerator=new TRandomGenerator(myParameters->getParameterLong("addToSeed"), false);
	} else randomGenerator=new TRandomGenerator();
	logfile->flush(" done with seed ");
	logfile->flush(randomGenerator->usedSeed);
	logfile->write("!");

	//prefix
	prefix=myParameters->getParameterString("prefix", false);
	if(!prefix.empty()) prefix+='_';
	suffix=myParameters->getParameterString("suffix", false);
	if(!suffix.empty()) suffix='_'+suffix;
}

//---------------------------------------------------------------------------
//a function to get all files matching the tags...
void TAnalyzer::getFiles(){
	std::string filenametag=myParameters->getParameterString("filename");
	logfile->listFlush("compiling files matching '" + filenametag + "' ...");

	//remove directory if contained in tag
	std::string filenamedir="./";
	if(stringContains(filenametag,'/'))
		filenamedir=extractPath(filenametag);


	//deal with wildcards
	std::vector<std::string> tags;
	std::string temp;
	if(filenametag.find_first_of('*')==0) tags.push_back("*");
	while(!filenametag.empty()){
		temp=extractBefore(filenametag, '*');
		if(!temp.empty()){
			if(temp!="*" || (*tags.rbegin())!="*"){
				tags.push_back(temp);
			}
		}
		if(filenametag.length()>0) tags.push_back("*");
		filenametag.erase(0,1);
	}

	//find all files to process.
	DIR *dp;
	struct dirent *dirp;
    if((dp  = opendir(filenamedir.c_str())) == NULL) {
    	throw "Error opening the directory '" + filenamedir +"'!";
    }

    std::vector<std::string>::iterator tagsIT;
    bool ok=true;
    int pos;
    while ((dirp = readdir(dp)) != NULL){
    	temp=dirp->d_name;
    	//only if it does not start with '.'
    	if(temp.find('.')!=0){
    		//check if the filename contains the tag(s)
    		ok=true;
    		tagsIT=tags.begin();
    		while(tagsIT!=tags.end()){
    			if((*tagsIT)=="*"){
    				++tagsIT;
    				if(tagsIT!=tags.end() && (*tagsIT)!="*"){
    					pos=temp.find((*tagsIT));
    					if(pos<0){
    						ok=false;
    						break;
    					} else {
    						temp.erase(0, pos+(*tagsIT).length());
    						++tagsIT;
    						if(tagsIT==tags.end() && temp.length()>0){
	    						ok=false;
	    						break;
    						}
    					}
    				}
    			} else {
    				pos=temp.find((*tagsIT));
    				if(pos!=0){
    					ok=false;
    					break;
    				} else {
	   					temp.erase(0, (*tagsIT).length());
	   					++tagsIT;
    					if(tagsIT==tags.end() && temp.length()>0){
	   						ok=false;
	   						break;
    					}
    				}
    			}
    		}
    		if(ok) files.push_back(filenamedir+dirp->d_name);
    	}
    }
    closedir(dp);
    logfile->write( "done!");
    logfile->conclude("found " + toString(files.size()) + " files.");
    if(files.size()<1) throw "No files to process!";
}
//---------------------------------------------------------------------------
void TAnalyzer::readMapDetails(){
	//read boundaries of map
	logfile->startIndent("Reading map details:");

	start=myParameters->getParameterDouble("start");
	logfile->list("start = " + toString(start));
	end=myParameters->getParameterDouble("end");
	logfile->list("end = " + toString(end));

	//read window sizes
	std::string buf=myParameters->getParameterString("windowSize");
	//this may be a list with several window sizes
	std::string temp;
	bool first=true;
	windowSizes.clear();
	logfile->listFlush("windowSize(s) = ");
	long w;
	while(!buf.empty()){
		temp=extractBefore(buf, ';');
		if(first) first=false;
		else logfile->flush(", ");
		w=stringToLong(temp);
		logfile->flush(w);
		windowSizes.push_back(w);
		buf.erase(0,1);
	}
	logfile->newLine();

	//read increment (a fraction)
	increment=myParameters->getParameterDouble("increment");
	logfile->list("increment = " + toString(increment));
	logfile->endIndent();
}
//---------------------------------------------------------------------------
void TAnalyzer::produceMaps(){
	//producing recombination maps from the recombest output
	logfile->startIndent("Producing Maps from recombest output:");
	getFiles();
	readMapDetails();

	//will we do empirical Bayes?
	if(myParameters->parameterExists("empiricalBayesSimulations") || myParameters->parameterExists("empiricalBayesFile")){
		produceEmpiricalBayesMaps();
	} else produceStandardMaps();
	logfile->endIndent();
}
//---------------------------------------------------------------------------
void TAnalyzer::produceStandardMaps(){
	//generate maps
	generateStandardMapObjects();
	//run through all individuals
	loopIndividuals();
	//output maps
	writeMaps();
}
//---------------------------------------------------------------------------
void TAnalyzer::produceEmpiricalBayesMaps(){
	bool empiricalBayesSimulationsRead=false;
	TIntervalDB* myIntervalDB;
	if(myParameters->parameterExists("empiricalBayesFile")){
		logfile->startIndent("Importing switch point DB from file(s):");
		//migth be a list of files
		std::string buf=myParameters->getParameterString("switchPointDB");
		std::string empiricalBayesFile;
		while(!buf.empty()){
			empiricalBayesFile=extractBefore(buf, ';');
			trimString(empiricalBayesFile);
			logfile->listFlush("importing '" + empiricalBayesFile + "' ...");
			if(empiricalBayesSimulationsRead) myIntervalDB->import(empiricalBayesFile);
			else{
				myIntervalDB=new TIntervalDB(empiricalBayesFile);
				empiricalBayesSimulationsRead=true;
			}
			buf.erase(0,1);
			logfile->write(" done!");
		}
		logfile->endIndent();
	}

	//generate mapswith the argument \texttt{empiricalBayesFile}
	//vector<TMapEmpiricalBayes*> maps;
	for(std::vector<long>::iterator it=windowSizes.begin(); it!=windowSizes.end(); ++it){
		maps.push_back(new TMapEmpiricalBayes((*it), start, end, increment));
	}

	//run through all individuals
	loopIndividuals();
	logfile->startIndent("performing empirical Bayes correction:");
	//save DB
	if(myParameters->parameterExists("writeSwitchPointDBto")){
		std::string filename=myParameters->getParameterString("writeSwitchPointDBto");
		logfile->listFlush("writing switch point DB to '" + filename + "' ...");
		myIntervalDB->write(filename);
		logfile->write(" done!");
	}

	//prepare maps
	logfile->listFlush("initializing map objects ...");
	for(std::vector<TMapEmpiricalBayes*>::size_type j=0; j<maps.size(); ++j){
		maps[j]->initializeEmpiricalBayes(myIntervalDB);
	}
	logfile->write(" done!");


	//generate new maps
	logfile->listFlush("looping through all individuals ...");
	for (unsigned int i = 0;i < files.size();i++) {
		TInd* myInd=new TInd(files[i]);
		if(myInd->ok){
			for(unsigned int j=0; j<maps.size(); ++j){
				maps[j]->addProbabilitiesEmpiricalBayes(myInd);
			}
		}
		delete myInd;
	}
	logfile->write(" done!");
	logfile->endIndent();

	//output maps
	writeMaps();
	delete myIntervalDB;
}
//---------------------------------------------------------------------------
void TAnalyzer::generateStandardMapObjects(){
	for(std::vector<long>::iterator it=windowSizes.begin(); it!=windowSizes.end(); ++it){
		maps.push_back(new TMapEmpiricalBayes((*it), start, end, increment));
	}
}

void TAnalyzer::writeMaps(){
	//output maps
	logfile->listFlush("writing maps ...");
	for(unsigned int j=0; j<maps.size(); ++j){
		maps[j]->writeMap(prefix, suffix);
	}
	logfile->write(" done!");
}
//---------------------------------------------------------------------------
void TAnalyzer::produceIntervalDB(){
	logfile->startIndent("Compiling an switch point DB (used for the empirical Bayes correction) from simulated data:");
	//read window sizes
	std::string buf=myParameters->getParameterString("windowSize");
	//this may be a list with several window sizes
	std::string temp;
	bool first=true;
	windowSizes.clear();
	logfile->listFlush("windowSize(s) = ");
	long w;
	while(!buf.empty()){
		temp=extractBefore(buf, ';');
		if(first) first=false;
		else logfile->flush(", ");
		w=stringToLong(temp);
		logfile->flush(w);
		windowSizes.push_back(w);
		buf.erase(0,1);
	}
	logfile->newLine();

	int numBins=myParameters->getParameterDouble("numBins");
	logfile->list("number of bins set to " + toString(numBins));
	double maxProb=myParameters->getParameterDouble("maxProb");
	logfile->list("max probability for binning set to " + toString(maxProb));
	long edgeRemoval=myParameters->getParameterDouble("endRemoval", false);
	logfile->list("removing " + toString(edgeRemoval) + " bases from each chromosome end");

	std::string filename=myParameters->getParameterString("writeSwitchPointDBto");
	//generate interval DB
	TIntervalDB* myIntervalDB;
	myIntervalDB=new TIntervalDB(myParameters->getParameterString("simulatedDataFiles"), edgeRemoval, numBins, maxProb, logfile);
	for(unsigned int j=0; j<windowSizes.size(); ++j){
		myIntervalDB->addWindow(windowSizes[j]);
	}
	//fill DB
	myIntervalDB->fillDB();

	//save DB
	logfile->listFlush("writing switch point DB to '" + filename + "' ...");
	myIntervalDB->write(filename);
	logfile->write(" done!");

	logfile->endIndent();
	delete myIntervalDB;
}
//---------------------------------------------------------------------------
void TAnalyzer::loopIndividuals(){
	logfile->listFlush("looping through all individuals ... (0%)");
	int percent=0;
	int oldPercent=0;
	for (unsigned int i = 0;i < files.size();i++) {
		TInd* myInd=new TInd(files[i]);
		if(!myInd->hasSwitchProbabilities){
			logfile->warning("File '" + files[i] + "' does not contain switch point probabilities: skipping this file!");
		} else if(myInd->ok){
			for(unsigned int j=0; j<maps.size(); ++j){
				maps[j]->addProbabilitiesAndAncestries(myInd);
			}
		}
		delete myInd;
		percent=100 * (double) i / (double) files.size();
		if(percent>oldPercent){
			logfile->listOverFlush("looping through all individuals ... (" + toString(percent) + "%)");
			oldPercent=percent;
		}
	}
	logfile->overList("looping through all individuals ... done!");
}
void TAnalyzer::loopIndividualsProbabilitiesOnly(){
	logfile->listFlush("looping through all individuals ... (0%)");
	int percent=0;
	int oldPercent=0;
	for (unsigned int i = 0;i < files.size();i++) {
		TInd* myInd=new TInd(files[i]);
		if(!myInd->hasSwitchProbabilities){
			logfile->warning("File '" + files[i] + "' does not contain switch point probabilities: skipping this file!");
		} else if(myInd->ok){
			for(unsigned int j=0; j<maps.size(); ++j){
				maps[j]->addProbabilities(myInd);
			}
		}
		delete myInd;
		percent=100 * (double) i / (double) files.size();
		if(percent>oldPercent){
			logfile->listOverFlush("looping through all individuals ... (" + toString(percent) + "%)");
			oldPercent=percent;
		}
	}
	logfile->overList("looping through all individuals ... done!");
}
void TAnalyzer::loopIndividualsAncestriesOnly(){
	logfile->listFlush("looping through all individuals ... (0%)");
	int percent=0;
	int oldPercent=0;
	for (unsigned int i = 0;i < files.size();i++) {
		TInd* myInd=new TInd(files[i]);
		if(myInd->ok){
			for(unsigned int j=0; j<maps.size(); ++j){
				maps[j]->addAncestries(myInd);
			}
		}
		delete myInd;
		percent=100 * (double) i / (double) files.size();
		if(percent>oldPercent){
			logfile->listOverFlush("looping through all individuals ... (" + toString(percent) + "%)");
			oldPercent=percent;
		}
	}
	logfile->overList("looping through all individuals ... done!");
}
//---------------------------------------------------------------------------
//produces hotspot usage
void TAnalyzer::produceHotspotUsage(){
	logfile->startIndent("Compute Hostpot Usage:");
	getFiles();

	//read boundaries of map
	logfile->startIndent("reading hotspot definitions (and limits):");
	start=myParameters->getParameterDouble("start");
	logfile->list("start = " + toString(start));
	end=myParameters->getParameterDouble("end");
	logfile->list("end = " + toString(end));
	std::string hotspotFile=myParameters->getParameterString("hotspotFile");
	logfile->list("hotspot definitions from file '" + hotspotFile + "'");
	TMapEmpiricalBayes hotspots(hotspotFile, (std::string) "definition", start, end);
	TMapEmpiricalBayes oneInd(&hotspots);
	logfile->endIndent();

	//prepare output file
	std::ofstream file;
	std::string filename=hotspotFile;
	extractAfterLast(filename, '.');
	filename = prefix + filename + "_individualUsage" + suffix + ".txt";
	logfile->list("writing output to '" + filename + "'");
	file.open(filename.c_str());

	//now compute!
	logfile->listFlush("looping through all individuals ...");
	double sum, total, totalSum=0;
	long totalLength=(end-start);
	double binLengthFrac=hotspots.getTotalBinLength()/(double)totalLength;

	//get first ind to get num ref pop
	TInd* myInd=new TInd(files[0]);
	oneInd.addProbabilitiesAndAncestries(myInd);
	//write header
	file << "filename\ttotalRate\tinHotspots\tfraction\ttotalLength\thotspotLength\tfractionOfHotspotsInBasepairs";
	for(int p=0; p<oneInd.numRefPop; ++p) file << "\tanc_" << (p+1);
	file << std::endl;

	bool first=true;
	//now compute!
	for(unsigned int i = 1;i < files.size();i++){
		TInd* myInd=new TInd(files[i]);
		if(!myInd->hasSwitchProbabilities){
			logfile->warning("File '" + files[i] + "' does not contain switch point probabilities: skipping this file!");
		} else if(myInd->ok){
			oneInd.empty();
			if(myInd->numRefPop==oneInd.numRefPop || first){
				oneInd.addProbabilitiesAndAncestries(myInd);
				if(first){
					//write header
					file << "filename\ttotalRate\tinHotspots\tfraction\ttotalLength\thotspotLength\tfractionOfHotspotsInBasepairs";
					for(int p=0; p<oneInd.numRefPop; ++p) file << "\tanc_" << (p+1);
					file << std::endl;
				}
				oneInd.addProbabilitiesAndAncestries(myInd);
				hotspots.addMapWithSameBins(oneInd);
				sum=myInd->getTotalRate(start, end);
				total=oneInd.getTotalRate();
				file << files[i] << "\t" << sum << "\t" << total << "\t" << total/sum << "\t" << totalLength << "\t" << hotspots.getTotalBinLength() << "\t" <<binLengthFrac;
				for(int p=0; p<oneInd.numRefPop; ++p)
					file << "\t" << oneInd.getAverageAncestry(p) << std::endl;
				totalSum+=sum;
			}else {
				std::cerr << "WARNING: Unexpected number of reference populations in file '" + filename + "! Skipping this file..." << std::endl;
			}
		}
		delete myInd;
	}
	file << "total\t" << totalSum << "\t" << hotspots.getTotalRate() << "\t" << hotspots.getTotalRate()/totalSum << "\t" << totalLength << "\t" << hotspots.getTotalBinLength() << "\t" << binLengthFrac;
	for(int p=0; p<oneInd.numRefPop; ++p)
		file << "\t" << hotspots.getAverageAncestry(p);
	file << std::endl;
	file.close();
	logfile->write(" done!");

	//write map
	logfile->listFlush("writing map ...");
	hotspots.writeMap(prefix, suffix);
	logfile->write(" done!");
	logfile->endIndent();
}
//---------------------------------------------------------------------------
//produces hotspot usage with empirical Bayes
//read in bins from another map -> teh hotspot bins have bin defined on these bins
//read htospots as a list of those bins
//construct individual maps usign these bins
//Then get the phenotype as rate in teh selected bin vs rate in all bins
//This HAS TO BE DONE ON A PER CHROMOSOM BASIS
void TAnalyzer::produceHotspotUsageEmpiricalBayes(){
	logfile->startIndent("Compute Hostpot Usage empirical Bayes:");
	getFiles();

	//read details
	std::string mapfile=myParameters->getParameterString("mapFile");
	logfile->list("Using bins defined in '" + mapfile + "'");
	maps.push_back(new TMapEmpiricalBayes(mapfile));
	maps[0]->empty();

	std::vector<std::string> hotspotFiles;
	myParameters->fillParameterIntoVector("hotspotFile", hotspotFiles, ';');

	//read and parse hotspot definitions
	//hotspots are a list of bins that have to be present in the map with the bin definitions
	//the file contains start and end of the bin in the first two columsn, further columns are optional
	logfile->startIndent("Hotspot definitions:");
	long* numIds=new long[hotspotFiles.size()];
	long** ids=new long*[hotspotFiles.size()];

	std::string buf;
	for(std::vector<std::string>::size_type i=0; i<hotspotFiles.size(); ++i){
		logfile->listFlush("Reading file '" + hotspotFiles[i] + " ... ");
		std::vector<std::string> val;
		std::vector<long> binStart;
		std::vector<long> binEnd;
		std::ifstream hotspots;
		hotspots.open(hotspotFiles[i].c_str());
		long line=1;
		while(hotspots.good() && !hotspots.eof()){
			getline(hotspots, buf); // read the line
			trimString(buf);
			if(!buf.empty()){ //skip empty lines
				val.clear();
				fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
				if(val.size()<2){
					throw "Wrong number of values in file '" + hotspotFiles[i] + " on line " + toString(line) + ": expect at least two columns (start and end)!";
				}
				binStart.push_back(stringToLong(val[0]));
				binEnd.push_back(stringToLong(val[1]));
			}
			++line;
		}
		hotspots.close();
		//get bin ids matching those start / end psoitions from map
		numIds[i]=binStart.size();
		ids[i]=new long[numIds[i]];
		if(!maps[0]->fillBinID(binStart, binEnd, ids[i])) throw "Some of the hotspot bins are missing in the map!";
		logfile->write(" done!");
	}
	logfile->endIndent();

	//intial run through all individuals and store individual ancestry!
	loopIndividualsProbabilitiesOnly();

	//import empirical Bayes interval DB
	TIntervalDB* myIntervalDB;
	logfile->startIndent("Importing interval DB for empirical Bayes from file(s):");
	//migth be a list of files
	std::vector<std::string> vec;
	myParameters->fillParameterIntoVector("empiricalBayesFile", vec, ';');
	bool first=true;
	for(std::vector<std::string>::iterator it=vec.begin(); it!=vec.end(); ++it){
		logfile->listFlush("Importing from file '" + *it + "' ...");
		if(first){
			myIntervalDB=new TIntervalDB(*it);
			first=false;
		} else myIntervalDB->import(*it);
		logfile->write(" done!");
	}
	logfile->endIndent();
	maps[0]->initializeEmpiricalBayes(myIntervalDB);

	//prepare output file
	std::ofstream file;
	std::string filename = prefix + "individualUsage" + suffix + ".txt";
	logfile->list("Writing output to '" + filename + "'");
	file.open(filename.c_str());
	//header
	file << "filename\ttotalRate\tnumBins";
	for(std::vector<std::string>::size_type i=0; i<hotspotFiles.size(); ++i){
		file << "\tRateinHotspots" << i << "\thotspotNumBins" << i;
	}
	file << std::endl;

	//now compute!
	logfile->listFlush("Looping through all individuals, performing empirical Bayes and computing hotspot usage ... (0%)");
	int percent=0;
	int oldPercent=0;
	for(unsigned int i = 0;i < files.size();i++){
		TInd* myInd=new TInd(files[i]);
		if(!myInd->hasSwitchProbabilities){
			logfile->warning("File '" + files[i] + "' does not contain switch point probabilities: skipping this file!");
		} else if(myInd->ok){
			maps[0]->addProbabilitiesEmpiricalBayes(myInd);
			file << files[i] << "\t" << maps[0]->getTotalRateEmpiricalBayes() << "\t" << maps[0]->getNumBins();
			for(std::vector<std::string>::size_type j=0; j<hotspotFiles.size(); ++j){
				file << "\t" << maps[0]->getTotalRateEmpiricalBayes(ids[j], numIds[j]) << "\t" << numIds[j];
			}
			file << std::endl;
			maps[0]->emptyEmpiricalBayes();
		}
		delete myInd;
		percent=100 * (double) i / (double) files.size();
		if(percent>oldPercent){
			logfile->listOverFlush("Looping through all individuals, performing empirical Bayes and computing hotspot usage ... (" + toString(percent) + "%)");
			oldPercent=percent;
		}
	}
	file.close();
	logfile->overList("Looping through all individuals, performing empirical Bayes and computing hotspot usage ... done!");
	logfile->endIndent();
}
//---------------------------------------------------------------------------
//generate map from HapMap map
void TAnalyzer::importMap(){
	logfile->startIndent("Generate map from another map:");
	std::string format=myParameters->getParameterString("format");
	if(format!="impute" && format!="decode")
			throw "Unknown format '" + format +"'!";

	readMapDetails();
	generateStandardMapObjects();

	//read map file
	std::string mapFile=myParameters->getParameterString("mapFile");
	logfile->listFlush("reading '" + mapFile + "' ...");
	TMapEmpiricalBayes hapmap(mapFile, format);
	logfile->write(" done!");

	//generate maps
	logfile->listFlush("computing maps ...");
	for(unsigned int j=0; j<maps.size(); ++j){
		maps[j]->addProbabilities(hapmap);
	}
	logfile->write(" done!");

	//write maps
	extractAfterLast(mapFile, '.');
	prefix=prefix+mapFile;
	writeMaps();
	logfile->endIndent();
}
//---------------------------------------------------------------------------
//calling switch-points
void TAnalyzer::callSwitchpoints(){
	logfile->startIndent("Making Ancestry-Swith Calls:");
	getFiles();

	logfile->startIndent("reading calling parameters:");
	long start=myParameters->getParameterDouble("start");
	logfile->list("start = " + toString(start));
	long end=myParameters->getParameterDouble("end");
	logfile->list("end = " + toString(end));
	long windowsize=myParameters->getParameterDouble("windowsize");
	logfile->list("windowsize = " + toString(windowsize));
	if(windowsize<10) throw "Window size too small!";
	double cutoff=myParameters->getParameterDouble("cutoff");
	logfile->list("cutoff = " + toString(cutoff));
	logfile->endIndent();

	//prefix
	std::string filename=prefix+"_windowsize_" + toString(windowsize) + "_cutoff_" + toString(cutoff) + ".switchpoints";
	logfile->list("writing results to '" + filename + "'");
	std::ofstream* out;
	out = new std::ofstream(filename.c_str());

	//now call the events!
	logfile->listFlush("calling events ...");
	for (unsigned int i = 0;i < files.size();i++){
		TInd* myInd=new TInd(files[i]);
		if(!myInd->hasSwitchProbabilities){
			logfile->warning("File '" + files[i] + "' does not contain switch point probabilities: skipping this file!");
		} else if(myInd->ok){
			myInd->callAndWriteSwitchPoints(start, end, windowsize, cutoff, out);
		}
	}
	logfile->write(" done!");
	logfile->endIndent();
}
//---------------------------------------------------------------------------
void TAnalyzer::computeEnrichment(){
	//this function computes the fraction of recombination
	//that occurs in the top fraction of the whole map
	logfile->startIndent("Computing enrichment of recombination in hotspots:");

	logfile->startIndent("reading parameters:");
	//first read in map
	std::string mapfile=myParameters->getParameterString("mapFile");
	logfile->list("map file = " + mapfile);
	//fractions to consider
	std::string buf=myParameters->getParameterString("fraction");
	std::vector<double> fractions;
	myParameters->fillParameterIntoVector("fraction", fractions, ';');
	//this may be a list with several window sizes
	bool first=true;
	logfile->listFlush("fraction(s) = ");
	for(std::vector<double>::iterator it=fractions.begin(); it!=fractions.end(); ++it){
		if(first) first=false;
		else logfile->flush(", ");
		logfile->flush(*it);
	}
	logfile->newLine();
	logfile->endIndent();

	//read in map file
	logfile->listFlush("Reading map file '" + mapfile + "' ...");
	TMapEmpiricalBayes map(mapfile);
	logfile->write("done!");

	//perform enrichment analysis
	logfile->listFlush("Computing enrichment(s):");
	map.writeEnrichment(fractions, prefix, suffix, logfile);
	logfile->write("done!");
	logfile->endIndent();
}
//---------------------------------------------------------------------------
void TAnalyzer::updateMap(){
	//this function reads in a map and:
	// - excludes bins (if requested)
	// - update the ranks (or adds them if absent)
	//if no prefix is given, the map is overwritten!
	logfile->startIndent("Updating map:");

	std::string mapfile=myParameters->getParameterString("mapFile");
	logfile->list("map file = '" + mapfile + "'");
	TMapEmpiricalBayes* map;

	//prepare TExclude
	TExclude* exclude;
	bool doExclude=false;
	std::string excludefile=myParameters->getParameterString("excludeFile", false);
	if(!excludefile.empty()){
		logfile->list("exclude file = '" + excludefile + "'");
		exclude= new TExclude(excludefile);
		doExclude=true;
	}
	if(myParameters->parameterExists("start")){
		start=myParameters->getParameterDouble("start");
		logfile->list("new start = ", start);
		if(doExclude) exclude->addInterval(0, start);
		else {
			exclude=new TExclude(0, start);
			doExclude=true;
		}
	}
	if(myParameters->parameterExists("end")){
		end=myParameters->getParameterDouble("end");
		logfile->list("new end = ", end);
		if(doExclude) exclude->addInterval(end, 9999999999999);
		else {
			exclude=new TExclude(end, 9999999999999);
			doExclude=true;
		}
	}

	if(doExclude) map=new TMapEmpiricalBayes(mapfile, exclude);
	else map=new TMapEmpiricalBayes(mapfile);

	bool recomputeRanks=myParameters->getParameterDouble("recomputeRanks", false);
	if(recomputeRanks){
		logfile->listFlush("recomputing ranks ...");
		map->computeRanks(true);
		logfile->write("done!");
	}
	if(!prefix.empty() || !suffix.empty()){
		logfile->listFlush("writing map with prefix = '" + prefix + "' and suffix '" + suffix + "' ...");
	} else logfile->listFlush("overwriting map ...");
	map->writeMap(prefix, suffix);
	logfile->write("done!");
	delete map;
	logfile->endIndent();
}
//---------------------------------------------------------------------------
void TAnalyzer::computeCorrelations(){
	//this function computes correlations between maps
	//it excluded bins, if requested
	logfile->startIndent("computing correlations:");
	std::string mapfile=myParameters->getParameterString("mapFile1");
	logfile->list("map file 1 = " + mapfile);
	std::string mapfile2=myParameters->getParameterString("mapFile2");
	logfile->list("map file 2 = " + mapfile2);

	TMapEmpiricalBayes* map;
	TMapEmpiricalBayes* map2;
	std::string excludefile=myParameters->getParameterString("excludeFile", false);
	if(!excludefile.empty()){
		logfile->list("exclude file = " + excludefile);
		TExclude exclude(excludefile);
		map=new TMapEmpiricalBayes(mapfile, &exclude);
		map2=new TMapEmpiricalBayes(mapfile2, &exclude);
	} else {
		map=new TMapEmpiricalBayes(mapfile);
		map2=new TMapEmpiricalBayes(mapfile2);
	}

	//will we trimm the data?
	std::string trimmType=myParameters->getParameterString("trimType", false);
	if(trimmType.empty()) trimmType="lowest";
	std::vector<double> trimmFractions;
	if(myParameters->parameterExists("trimFraction")){
		myParameters->fillParameterIntoVector("trimFraction", trimmFractions, ';');
		bool first=true;
		logfile->startIndent("trimming:");
		logfile->list("trimming type = " + trimmType);
		logfile->listFlush("trimming fraction(s) = ");
		for(std::vector<double>::iterator it=trimmFractions.begin(); it!=trimmFractions.end(); ++it){
			if(first) first=false;
			else logfile->flush(", ");
			logfile->flush(*it);
		}
		logfile->newLine();
		logfile->endIndent();
	} else {
		trimmFractions.push_back(0.0);
	}

	//transformation
	std::string transformation=myParameters->getParameterString("transformation", false);
	if(transformation.empty()) transformation="none";
	else {
		if(transformation!="sqrt" && transformation!="log10") throw "Unknown transformation '"+transformation+"'!";
		logfile->list("applying the following transformation: " + transformation);
	}

	//open output file
	std::ofstream out;
	std::string temp=prefix+"correlations"+suffix+".txt";
	out.open(temp.c_str());
	out << "map1\tmapType1\tmap2\tmapType2\ttrimFrac\ttrimType\ttransformation\tSpearman\tPearson" << std::endl;

	//compute correlations
	logfile->listFlush("computing correlations ...");
	//check that we have the same bins!
	if(!map->checkBins(map2)) throw "The two maps have different bins!";
	TCorrelator* corr=new TCorrelator;
	corr->setTrimming(trimmType);
	bool corrComputed=false;
	for(std::vector<double>::iterator tf=trimmFractions.begin(); tf!=trimmFractions.end(); ++tf){
		if(map->recombinationComputed() && map2->recombinationComputed()){
			map->getNumBins();
			map->getPointerToRecombination();
			map->getPointerToRanks();
			corr->addDataset(map->getNumBins(), map->getPointerToRecombination(), map->getPointerToRanks());
			corr->addDataset(map2->getNumBins(), map2->getPointerToRecombination(), map2->getPointerToRanks());
			out << map->getFilename() << "\tstandard\t" << map2->getFilename() << "\tstandard\t" << *tf << "\t" << trimmType << "\t" << transformation;
			out << "\t" << corr->getSpearmanCorrelation(*tf);
			if(transformation!="none"){
				//transform data
				corr->reset();
				corr->addDataset(map->getNumBins(), map->getPointerToRecombination(), transformation);
				corr->addDataset(map2->getNumBins(), map2->getPointerToRecombination(), transformation);
			}
			out << "\t" << corr->getPearsonCorrelation(*tf) << std::endl;;
			out << std::endl;
			corr->reset();
			corrComputed=true;
		}
		if(map->recombinationComputed() && map2->empiricalBayesComputed()){
			corr->addDataset(map->getNumBins(), map->getPointerToRecombination(), map->getPointerToRanks());
			corr->addDataset(map2->getNumBins(), map2->getPointerToRecombinationEmpricalBayes(), map2->getPointerToRanksEmpricalBayes());
			out << map->getFilename() << "\tstandard\t" << map2->getFilename() << "\tstandard\t" << *tf << "\t" << trimmType << "\t" << transformation;
			out << "\t" << corr->getSpearmanCorrelation(*tf);
			if(transformation!="none"){
				//transform data
				corr->reset();
				corr->addDataset(map->getNumBins(), map->getPointerToRecombination(), transformation);
				corr->addDataset(map2->getNumBins(), map2->getPointerToRecombinationEmpricalBayes(), transformation);
			}
			out << "\t" << corr->getPearsonCorrelation(*tf) << std::endl;;
			out << std::endl;
			corr->reset();
			corrComputed=true;
		}
		if(map->empiricalBayesComputed() && map2->recombinationComputed()){
			corr->addDataset(map->getNumBins(), map->getPointerToRecombinationEmpricalBayes(), map->getPointerToRanksEmpricalBayes());
			corr->addDataset(map2->getNumBins(), map2->getPointerToRecombination(), map2->getPointerToRanks());
			out << map->getFilename() << "\tstandard\t" << map2->getFilename() << "\tstandard\t" << *tf << "\t" << trimmType << "\t" << transformation;
			out << "\t" << corr->getSpearmanCorrelation(*tf);
			if(transformation!="none"){
				//transform data
				corr->reset();
				corr->addDataset(map->getNumBins(), map->getPointerToRecombinationEmpricalBayes(), transformation);
				corr->addDataset(map2->getNumBins(), map2->getPointerToRecombination(), transformation);
			}
			out << "\t" << corr->getPearsonCorrelation(*tf) << std::endl;;
			out << std::endl;
			corr->reset();
			corrComputed=true;
		}
		if(map->empiricalBayesComputed() && map2->empiricalBayesComputed()){
			corr->addDataset(map->getNumBins(), map->getPointerToRecombinationEmpricalBayes(), map->getPointerToRanksEmpricalBayes());
			corr->addDataset(map2->getNumBins(), map2->getPointerToRecombinationEmpricalBayes(), map2->getPointerToRanksEmpricalBayes());
			out << map->getFilename() << "\tstandard\t" << map2->getFilename() << "\tstandard\t" << *tf << "\t" << trimmType << "\t" << transformation;
			out << "\t" << corr->getSpearmanCorrelation(*tf);
			if(transformation!="none"){
				//transform data
				corr->reset();
				corr->addDataset(map->getNumBins(), map->getPointerToRecombinationEmpricalBayes(), transformation);
				corr->addDataset(map2->getNumBins(), map2->getPointerToRecombinationEmpricalBayes(), transformation);
			}
			out << "\t" << corr->getPearsonCorrelation(*tf) << std::endl;;
			out << std::endl;
			corr->reset();
			corrComputed=true;
		}
	}
	if(!corrComputed) throw "Maps do not contain recombination rates, no correlations have been computed!";
	logfile->write("done!");
	logfile->endIndent();
	out.close();
	delete map;
	delete map2;
	delete corr;
}


//---------------------------------------------------------------------------
void TAnalyzer::fillCorrelationsBetweenMaps(TCorrelator* corr, double & trimFrac, std::string & transformation, long numBins, double* ratesMap1, double* ratesMap2, double & spearman, double & pearson){
	corr->reset();
	//get Spearman
	corr->addDataset(numBins, ratesMap1);
	corr->addDataset(numBins, ratesMap2);
	spearman=corr->getSpearmanCorrelation(trimFrac);
	//get Pearson
	if(transformation!="none"){
		//transform data
		corr->reset();
		corr->addDataset(numBins, ratesMap1, transformation);
		corr->addDataset(numBins, ratesMap2, transformation);
	}
	pearson=corr->getPearsonCorrelation(trimFrac);
}

void TAnalyzer::fillCorrelationsBetweenMapsRanks(TCorrelator* corr, double & trimFrac, std::string & transformation, long numBins, double* ratesMap1, double* ratesMap2, double* ranksMap1, double* ranksMap2, double & spearman, double & pearson){
	corr->reset();
	//get Spearman
	corr->addDataset(numBins, ratesMap1, ranksMap1);
	corr->addDataset(numBins, ratesMap2, ranksMap2);
	spearman=corr->getSpearmanCorrelation(trimFrac);
	//get Pearson
	if(transformation!="none"){
		//transform data
		corr->reset();
		corr->addDataset(numBins, ratesMap1, transformation);
		corr->addDataset(numBins, ratesMap2, transformation);
	}
	pearson=corr->getPearsonCorrelation(trimFrac);
}


void TAnalyzer::computeAndWriteTwoWayCorrelations(TCorrelator* corr, std::ofstream & out, double & trimFrac, std::string & transformation, int bootstrap, long numBins, double* ratesMap1, double* ratesMap2, double* ratesMap3, double* ranksMap1, double* ranksMap2, double* ranksMap3){
	double* corPearson=new double[2];
	double* corSpearman=new double[2];
	double* corPearsonBootstrap=new double[2];
	double* corSpearmanBootstrap=new double[2];

	double** bootstrappedData;
	double bootstrapSuccessesSpearman=0;
	double bootstrapSuccessesPearson=0;
	if(bootstrap>0){
		bootstrappedData=new double*[3];
		for(int i=0; i<3; ++i) bootstrappedData[i]=new double[numBins];
	}

	fillCorrelationsBetweenMapsRanks(corr, trimFrac, transformation, numBins, ratesMap1, ratesMap2, ranksMap1, ranksMap2, corSpearman[0], corPearson[0]);
	fillCorrelationsBetweenMapsRanks(corr, trimFrac, transformation, numBins, ratesMap1, ratesMap3, ranksMap1, ranksMap3, corSpearman[1], corPearson[1]);

	//test correlation using bootstrap
	if(bootstrap>0){
		bootstrapSuccessesSpearman=0;
		bootstrapSuccessesPearson=0;
		for(int b=0; b<bootstrap; ++b){
			fillBootstrappedDataForCorrelations(numBins, ratesMap1, ratesMap2, ratesMap3,  bootstrappedData);

			fillCorrelationsBetweenMaps(corr, trimFrac, transformation, numBins, bootstrappedData[0], bootstrappedData[1], corSpearmanBootstrap[0], corPearsonBootstrap[0]);
			fillCorrelationsBetweenMaps(corr, trimFrac, transformation, numBins, bootstrappedData[0], bootstrappedData[2], corSpearmanBootstrap[1], corPearsonBootstrap[1]);

			if(corSpearmanBootstrap[0]<corSpearmanBootstrap[1]) bootstrapSuccessesSpearman+=1;
			else if(corSpearmanBootstrap[0]==corSpearmanBootstrap[1]) bootstrapSuccessesSpearman+=0.5;
			if(corPearsonBootstrap[0]<corPearsonBootstrap[1]) bootstrapSuccessesPearson+=1;
			else if(corPearsonBootstrap[0]==corPearsonBootstrap[1]) bootstrapSuccessesPearson+=0.5;
		}
	}
	//output
	out << "\t" << corSpearman[0] << "\t" << corSpearman[1] << "\t";
	if(bootstrap>0) out << bootstrapSuccessesSpearman/(double) bootstrap;
	else out << "NA";
	out << "\t" << corPearson[0] << "\t" << corPearson[1] << "\t";
	if(bootstrap>0) out << bootstrapSuccessesPearson/(double) bootstrap;
	else out << "NA";
	out << std::endl;
	delete[] corPearson;
	delete[] corSpearman;
	delete[] corPearsonBootstrap;
	delete[] corSpearmanBootstrap;
}
//---------------------------------------------------------------------------
void TAnalyzer::computeTwoWayCorrelations(){
	//this function computes correlations between maps
	//Two correlations will be computed: map1 vs map2 and mp1 vs map3
	//it excluded bins, if requested
	logfile->startIndent("computing correlations:");
	std::string mapfile1=myParameters->getParameterString("mapFile1");
	logfile->list("map file 1 = " + mapfile1);
	std::string mapfile2=myParameters->getParameterString("mapFile2");
	logfile->list("map file 2 = " + mapfile2);
	std::string mapfile3=myParameters->getParameterString("mapFile3");
	logfile->list("map file 3 = " + mapfile3);

	TMapEmpiricalBayes* map1; TMapEmpiricalBayes* map2; TMapEmpiricalBayes* map3;
	std::string excludefile=myParameters->getParameterString("excludeFile", false);
	if(!excludefile.empty()){
		logfile->list("exclude file = " + excludefile);
		TExclude exclude(excludefile);
		map1=new TMapEmpiricalBayes(mapfile1, &exclude);
		map2=new TMapEmpiricalBayes(mapfile2, &exclude);
		map3=new TMapEmpiricalBayes(mapfile3, &exclude);
	} else {
		map1=new TMapEmpiricalBayes(mapfile1);
		map2=new TMapEmpiricalBayes(mapfile2);
		map3=new TMapEmpiricalBayes(mapfile3);
	}

	//will we trimm the data?
	std::string trimmType=myParameters->getParameterString("trimType", false);
	if(trimmType.empty()) trimmType="lowest";
	std::vector<double> trimmFractions;
	if(myParameters->parameterExists("trimFraction")){
		myParameters->fillParameterIntoVector("trimFraction", trimmFractions, ';');
		bool first=true;
		logfile->startIndent("trimming:");
		logfile->list("trimming type = " + trimmType);
		logfile->listFlush("trimming fraction(s) = ");
		for(std::vector<double>::iterator it=trimmFractions.begin(); it!=trimmFractions.end(); ++it){
			if(first) first=false;
			else logfile->flush(", ");
			logfile->flush(*it);
		}
		logfile->newLine();
		logfile->endIndent();
	} else {
		trimmFractions.push_back(0.0);
	}

	//transformation
	std::string transformation=myParameters->getParameterString("transformation", false);
	if(transformation.empty()) transformation="none";
	else {
		if(transformation!="sqrt" && transformation!="log10") throw "Unknown transformation '"+transformation+"'!";
		logfile->list("applying the following transformation: " + transformation);
	}

	//will we bootstrap correlations
	int bootstrap=myParameters->getParameterDouble("bootstrap", false);

	//open output file
	std::string temp=prefix+"correlations"+suffix+".txt";
	std::ofstream out; out.open(temp.c_str());
	out << "map1\tmapType1\tmap2\tmap3\tmapType2/3\ttrimFrac\ttrimType\ttransformation\tSpearman1vs2\tSpearman1vs3\tSpearman.p\tPearson1vs2\tPearson1vs3\tPearson.p" << std::endl;

	//compute correlations
	logfile->listFlush("computing correlations ...");
	//check that we have the same bins!
	if(!map2->checkBins(map1)) throw "Maps 1 and 2 have different bins!";
	if(!map3->checkBins(map1)) throw "Maps 1 and 3 have different bins!";
	TCorrelator* corr=new TCorrelator;
	bool corrComputed=false;
	corr->setTrimming(trimmType);
	for(std::vector<double>::iterator tf=trimmFractions.begin(); tf!=trimmFractions.end(); ++tf){
		if(map1->recombinationComputed() && map2->recombinationComputed() && map3->recombinationComputed()){
			out << map1->getFilename() << "\tstandard\t" << map2->getFilename() << "\t" << map3->getFilename() <<"\tstandard\t" << *tf << "\t" << transformation;
			computeAndWriteTwoWayCorrelations(corr, out, *tf, transformation, bootstrap, map1->getNumBins(),
					map1->getPointerToRecombination(), map2->getPointerToRecombination(), map3->getPointerToRecombination(),
					map1->getPointerToRanks(), map2->getPointerToRanks(), map3->getPointerToRanks());
			corrComputed=true;
		}
		if(map1->recombinationComputed() && map2->empiricalBayesComputed() && map3->empiricalBayesComputed()){
			out << map1->getFilename() << "\tstandard\t" << map2->getFilename() << "\t" << map3->getFilename() <<"\tempiricalBayes\t" << *tf << "\t" << transformation;
			computeAndWriteTwoWayCorrelations(corr, out, *tf, transformation, bootstrap, map1->getNumBins(),
								map1->getPointerToRecombination(), map2->getPointerToRecombinationEmpricalBayes(), map3->getPointerToRecombinationEmpricalBayes(),
								map1->getPointerToRanks(), map2->getPointerToRanksEmpricalBayes(), map3->getPointerToRanksEmpricalBayes());
			corrComputed=true;
		}
		if(map1->empiricalBayesComputed() && map2->recombinationComputed() && map3->recombinationComputed()){
			out << map1->getFilename() << "\tempiricalBayes\t" << map2->getFilename() << "\t" << map3->getFilename() <<"\tstandard\t" << *tf << "\t" << transformation;
			computeAndWriteTwoWayCorrelations(corr, out, *tf, transformation, bootstrap, map1->getNumBins(),
								map1->getPointerToRanksEmpricalBayes(), map2->getPointerToRecombination(), map3->getPointerToRecombination(),
								map1->getPointerToRanksEmpricalBayes(), map2->getPointerToRanks(), map3->getPointerToRanks());
			corrComputed=true;
		}
		if(map1->empiricalBayesComputed() && map2->empiricalBayesComputed() && map3->empiricalBayesComputed()){
			out << map1->getFilename() << "\tempiricalBayes\t" << map2->getFilename() << "\t" << map3->getFilename() <<"\tempiricalBayes\t" << *tf << "\t" << transformation;
			computeAndWriteTwoWayCorrelations(corr, out, *tf, transformation, bootstrap, map1->getNumBins(),
								map1->getPointerToRanksEmpricalBayes(), map2->getPointerToRecombinationEmpricalBayes(), map3->getPointerToRecombinationEmpricalBayes(),
								map1->getPointerToRanksEmpricalBayes(), map2->getPointerToRanksEmpricalBayes(), map3->getPointerToRanksEmpricalBayes());
			corrComputed=true;
		}
	}

	if(!corrComputed) throw "Maps do not contain recombination rates, no correlations have been computed!";
	logfile->write(" done!");
	logfile->endIndent();
	out.close();
	delete map1; delete map2; delete map3;
	delete corr;
}
void TAnalyzer::fillBootstrappedDataForCorrelations(long length, double* source1, double* source2, double* source3, double** fill){
	//bootstrap intervals from source 1 and source 2 into fill1 and fill2, respectively
	long index;
	for(long i=0; i<length; ++i){
		index=randomGenerator->getRand((long) 0, length);
		fill[0][i]=source1[index];
		fill[1][i]=source2[index];
		fill[2][i]=source3[index];
	}
}
//---------------------------------------------------------------------------
/*
void TAnalyzer::writeLargesDifferences(){
	//write largest differences between maps
	//use two methods:
	// - quantile normalization
	// - regression
	//if sliceSize is given, a file for each region that can be used for plotting.
	//it excluded bins, if requested
	logfile->startIndent("find largest differences:");
	std::string mapfile=myParameters->getParameterString("mapFile1");
		logfile->list("map file 1 = " + mapfile);
		std::string mapfile2=myParameters->getParameterString("mapFile2");
		logfile->list("map file 2 = " + mapfile2);

	TMapEmpiricalBayes* map;
	TMapEmpiricalBayes* map2;
	std::string excludefile=myParameters->getParameterString("excludeFile", false);
	if(!excludefile.empty()){
		logfile->list("exclude file = " + excludefile);
		TExclude exclude(excludefile);
		map=new TMapEmpiricalBayes(mapfile, &exclude);
		map2=new TMapEmpiricalBayes(mapfile2, &exclude);
	} else {
		map=new TMapEmpiricalBayes(mapfile);
		map2=new TMapEmpiricalBayes(mapfile2);
	}

	//first quantile normalization


	logfile->endIndent();
	delete map;
	delete map2;
}
*/
//---------------------------------------------------------------------------
/*
void TAnalyzer::compileAncestries(){
	//this function parses through all files and writes the expected ancestry for each SNP and each Individual in a
	//format that can then be used to go through all ancestries SNP by SNP
	//In the current format, this is prohibitive, because to find the ancestries of all individuals for a given SNP
	//one would need to read all individuals in memory, which is prohibitive.
	//the anticipated format follows the plink map / ped format: one file with all individual filenames listed, one with the ancestries for each SNP

	std::cout << "Compile Ancestries:" << std::endl;
	getFiles();

	//writing individuals to file
	std::string filename=prefix+"compiledAncestries"+suffix+".individuals";
	std::cout << "    - writing individuals to '" << filename << "' ..." << std::flush;
	std::ofstream file(filename.c_str());
	if(!file) throw "Unable to open file '" + filename +"' for writing!";
	for (unsigned int i = 0;i < files.size();i++){
		file << files[i] << std::endl;
	}
	file.close();
	std::cout << " done!" << std::endl;

	//open output file
	filename=prefix+"compiledAncestries"+suffix+".ancestries";
	file.open(filename.c_str());
	if(!file) throw "Unable to open file '" + filename +"' for writing!";

	//idea is to only read a bunch of SNPs per iteration
	//some individuals have no ancestry at a SNP, but -1
	//prepare storage
	std::vector<std::string> snp;
	std::vector<long> pos;
	std::vector<double*> ancestries;

	bool reachedEnd=false;
	int snpsPerIteration=myParameters->getParameterDouble("snpsPerIteration");
	int numSnpsRead=0;
	long maxPos=0;
	long oldMaxPos=0;
	long currentSnpInDb;
	int numFilesOk=files.size();
	int percent=0;
	int oldPercent=0;
	std::vector<std::string>::iterator snpIt;
	std::vector<long>::iterator posIt;
	std::vector<double>::iterator probHetIt;
	std::vector<double>::iterator probHomo2It;

	while(!reachedEnd){
		std::cout << " - reading batch of " << snpsPerIteration << " SNPs ... (0%)" << std::flush;
		//empty vectors
		snp.clear();
		pos.clear();
		for(std::vector<double*>::iterator it=ancestries.begin(); it!=ancestries.end(); ++it) delete (*it);
		ancestries.clear();

		//go through each file and read SNPs
		numSnpsRead=0;
		percent=0;
		oldPercent=0;
		for (unsigned int i = 0;i < files.size();++i){
			TInd* myInd=new TInd(files[i], true);
			if(myInd->ok){
				posIt=myInd->indPositions.begin();
				snpIt=myInd->snpNames.begin();
				probHetIt=myInd->probHet.begin();
				probHomo2It=myInd->probHomo2.begin();
				//skip SNPs already written
				while((*posIt)<=oldMaxPos && posIt!=myInd->indPositions.end()){
					++posIt; ++snpIt; ++probHetIt; ++probHomo2It;
				}
				if(posIt!=myInd->indPositions.end()){
					//read snp by snp and check if it is already present
					while((numSnpsRead<snpsPerIteration || (*posIt)<maxPos) && posIt!=myInd->indPositions.end()){
						//snp is present?
						currentSnpInDb=-1;		percent=0;
						oldPercent=0;
						for(std::vector<std::string>::size_type idInDB=0; idInDB<snp.size(); ++idInDB){
							if(pos.at(idInDB)==(*posIt)){
								currentSnpInDb=idInDB;
								break;
							}
						}
						if(currentSnpInDb<0){ //SNP not yet known
							snp.push_back((*snpIt));
							pos.push_back((*posIt));
							ancestries.push_back(new double[files.size()]);
							currentSnpInDb=numSnpsRead;
							++numSnpsRead;
							if((*posIt)>maxPos) maxPos=(*posIt);
							//fill up ancestries with -1 for everyone
							for(std::vector<std::string>::size_type j=0; j<files.size(); ++j) ancestries.at(currentSnpInDb)[j]=-1;
						}
						//save ancestry
						ancestries.at(currentSnpInDb)[i]=(double) (*probHetIt)+2.0*(*probHomo2It);

						//move to next SNP
						++posIt; ++snpIt; ++probHetIt; ++probHomo2It;
						if(posIt==myInd->indPositions.end()) --numFilesOk;
					}
				}
			} //otherwise  -1 for all SNPs is automatically added
			delete myInd;
			percent=100 * (double) i / (double) files.size();
			if(percent>oldPercent){
				std::cout << '\xd' << " - reading batch of " << snpsPerIteration << " SNPs ... (" << percent << "%)" << std::flush;
				oldPercent=percent;
			}
		}
		std::cout << '\xd' << " - reading batch of " << snpsPerIteration << " SNPs ... done! " << std::endl;

		//write ancestries to file!
		std::cout << "    - writing ancestries to '" << filename << "' ..." << std::flush;
		for (unsigned int j = 0; j < snp.size(); ++j){
			file << snp.at(j) << "\t" << pos.at(j);
			for (unsigned int i = 0;i < files.size();++i){
				file << "\t" << ancestries.at(j)[i];
			}
			file << std::endl;
		}
		std::cout << " done!" << std::endl;

		if(numFilesOk<1 || numSnpsRead==0) reachedEnd=true;
		else oldMaxPos=maxPos;	//reset maxPos
	}

	file.close();
}
*/
//---------------------------------------------------------------------------
void TAnalyzer::calculatePopulationAncestry(){
	//This function parses through all files and calculates the posterior distribution on
	//the frequency of each ancestry at every SNP. Works only for two pos at the moment!
	//To do so, the function specifically calculates the posterior probability of having 0, 1, 2, 3, ..., 2N
	//alleles of one ancestry by looping over all combinations of having such an ancestry.
	logfile->startIndent("Calculating Population Ancestries:");
	getFiles();

	//writing individuals to file
	std::string filename=prefix+"populationAncestries"+suffix+".individuals";
	logfile->listFlush("writing individuals to '" + filename + "' ...");
	std::ofstream file(filename.c_str());
	if(!file) throw "Unable to open file '" + filename +"' for writing!";
	for (unsigned int i = 0;i < files.size();i++){
		file << files[i] << std::endl;
	}
	file.close();
	logfile->write(" done!");

	//idea is to only read a bunch of SNPs per iteration
	//some individuals have no ancestry at a SNP, but -1
	//prepare storage
	std::vector<std::string> snp;
	std::vector<long> pos;
	std::vector<std::string>::iterator snpIt;
	std::vector<long>::iterator posIt;
	std::vector< std::vector<TAncestryProbs*> > probVec;
	std::vector< std::vector<TAncestryProbs*> >::iterator probVecIt;
	std::vector<TAncestryProbs*>::iterator ancestryProbIt;
	std::vector<double*>::iterator probIndAncIt;
	std::vector<double> expectedAncestry;
	std::vector<double>::iterator expAncIt;

	//other variables
	bool reachedEnd=false;
	int snpsPerIteration=myParameters->getParameterDoubleWithDefault("snpsPerIteration", 10000);
	int numSnpsRead=0;
	long maxPos=0;
	long oldMaxPos=0;
	long currentSnpInDb;
	int numFilesOk=files.size();
	int percent=0;
	int oldPercent=0;

	//frequency bins
	int numFrequencies=myParameters->getParameterDoubleWithDefault("numFrequencies", 200);
	double* frequencies = new double[numFrequencies];
	double step = 1.0 / ((double) numFrequencies - 1.0);
	double* logFSquared  = new double[numFrequencies];
	double* logTwoFOneMinusF  = new double[numFrequencies];
	double* logOneMinusFSquared  = new double[numFrequencies];
	for(int i=0; i<numFrequencies; ++i){
		frequencies[i] = i * step;
		logFSquared[i] = log(frequencies[i] * frequencies[i] + 0.000000000001);
		logTwoFOneMinusF[i] = log(2.0 * frequencies[i] * (1.0 - frequencies[i])  + 0.000000000001);
		logOneMinusFSquared[i] = log((1.0 - frequencies[i]) * (1-0 - frequencies[i])  + 0.000000000001);
	}
	double* freqLogPostProb = new double[numFrequencies];
	int numAncestryCombinations=myParameters->getParameterDoubleWithDefault("numAncestryCombinations", 1000);
	int* ancestryCombination = new int[3];

	//stats
	double* hpdi = new double[3];
	hpdi[0] = 0.5; hpdi[1] = 0.9; hpdi[2] = 0.99;
	double* hpdiLocation_lower = new double[3];
	double* hpdiLocation_upper = new double[3];

	//open output file
	filename=prefix+"populationAncestries"+suffix+".ancestries";
	file.open(filename.c_str());
	if(!file) throw "Unable to open file '" + filename + "' for writing!";
	file << "snp\tpos\tE(f)\tmode";
	for(int i=0; i<3; ++i) file << "\tHPDI(" << hpdi[i] << ")_lower\tHPDI(" << hpdi[i] << ")_upper";
	//for(int i=0; i<numFrequencies; ++i) file << "\tlog_P(f=" << frequencies[i] << ")";
	file << std::endl;

	//go through files
	while(!reachedEnd){
		logfile->listFlush("reading batch of " + toString(snpsPerIteration) + " SNPs ... (0%)");
		//empty vectors
		snp.clear();
		pos.clear();
		expectedAncestry.clear();
		for(probVecIt = probVec.begin(); probVecIt != probVec.end(); ++probVecIt){
			for(ancestryProbIt = probVecIt->begin(); ancestryProbIt != probVecIt->end(); ++ancestryProbIt){
				delete *ancestryProbIt;
			}
		}
		probVec.clear();

		//go through each file and read SNPs
		numSnpsRead=0;
		percent=0;
		oldPercent=0;
		for (unsigned int i = 0;i < files.size();++i){
			TInd* myInd=new TInd(files[i], true);
			if(myInd->ok){
				if(myInd->numRefPop != 2) throw "Calculating population ancestries has only been implemented for the case of two different ancestries!";

				//start iterators in individual
				snpIt=myInd->snpNames.begin();
				posIt=myInd->indPositions.begin();
				probIndAncIt = myInd->probAnc.begin();

				//skip SNPs already written
				while((*posIt)<=oldMaxPos && posIt!=myInd->indPositions.end()){
					++posIt; ++snpIt; ++probIndAncIt;
				}
				if(posIt!=myInd->indPositions.end()){
					//read snp by snp and check if it is already present
					while((numSnpsRead<snpsPerIteration || (*posIt)<=maxPos) && posIt!=myInd->indPositions.end()){
						//snp is present?
						currentSnpInDb=-1;
						percent=0;
						oldPercent=0;
						for(std::vector<std::string>::size_type idInDB=0; idInDB<snp.size(); ++idInDB){
							if(pos.at(idInDB)==(*posIt)){
								currentSnpInDb=idInDB;
								break;
							}
						}

						if(currentSnpInDb<0){ //SNP not yet known
							snp.push_back((*snpIt));
							pos.push_back((*posIt));
							expectedAncestry.push_back((*probIndAncIt)[2] + 0.5 * (*probIndAncIt)[1]);
							probVec.push_back(std::vector<TAncestryProbs*>());
							currentSnpInDb=numSnpsRead;
							++numSnpsRead;
							if((*posIt)>maxPos) maxPos=(*posIt);
						} else {
							expectedAncestry.at(currentSnpInDb) += (*probIndAncIt)[2] + 0.5 * (*probIndAncIt)[1];
						}
						//save ancestry probabilities
						(probVec.at(currentSnpInDb)).push_back(new TAncestryProbs((*probIndAncIt)[0], (*probIndAncIt)[1]));

						//move to next SNP
						++posIt; ++snpIt; ++probIndAncIt;
						if(posIt==myInd->indPositions.end()) --numFilesOk;
					}
				}
			}
			delete myInd;
			percent=100 * (double) i / (double) files.size();
			if(percent>oldPercent){
				logfile->listOverFlush("reading batch of " + toString(snpsPerIteration) + " SNPs ... (" + toString(percent) + "%)");
				oldPercent=percent;
			}
		}
		logfile->overList("reading batch of " + toString(snpsPerIteration) + " SNPs ... done!   ");

		//calculate population ancestry posterior probabilities and write them to file
		logfile->listFlush("calculating posterior probabilities and writing results to '" + filename + "' ... (0%)");
		//loop over snps
		snpIt = snp.begin(); posIt = pos.begin(); expAncIt = expectedAncestry.begin();
		oldPercent = 0;
		int i = 0;
		for(probVecIt = probVec.begin(); probVecIt!=probVec.end(); ++probVecIt, ++snpIt, ++posIt, ++expAncIt, ++i){
			//write snp identifiers and expected
			file << *snpIt << "\t" << *posIt << "\t" << *expAncIt / (double) probVecIt->size();

			//reset things
			for(int j=0; j<numFrequencies; ++j) freqLogPostProb[j] = 0.0;
			//loop over ancestry combinations
			for(int a = 0; a < numAncestryCombinations; ++a){
				//sample an ancestry combination
				ancestryCombination[0] = 0;
				ancestryCombination[1] = 0;
				ancestryCombination[2] = 0;
				for(ancestryProbIt = probVecIt->begin(); ancestryProbIt != probVecIt->end(); ++ancestryProbIt){
					++ancestryCombination[(*ancestryProbIt)->sample(randomGenerator->getRand())];
				}
				//calculate posterior probability using binomial for each f
				//since we assume a uniform prior on f, this is simply p(A|f) where A is the ancestry combination chosen
				for(int j=0; j<numFrequencies; ++j){
					freqLogPostProb[j] += ancestryCombination[0]*logOneMinusFSquared[j] + ancestryCombination[1] * logTwoFOneMinusF[j] + ancestryCombination[2] * logFSquared[j];
				}
			}

			//normalize
			for(int j=0; j<numFrequencies; ++j) freqLogPostProb[j] = exp(freqLogPostProb[j] / (double) numAncestryCombinations);
			//double area = (exp(freqLogPostProb[0]) + exp(freqLogPostProb[numFrequencies-1]))/ 2.0;
			double area = (freqLogPostProb[0] + freqLogPostProb[numFrequencies-1])/ 2.0;
			for(int j=1; j<(numFrequencies-1); ++j){
				area += freqLogPostProb[j];
			}
			area = area*step;

			//calculate credible intervals (High Posterior Density Interval)
			//start at mode and always add from the bordering bin with the higher density
			int modeLocation = 0;
			for(int j=0; j<numFrequencies; ++j){
				if(freqLogPostProb[j] > freqLogPostProb[modeLocation]) modeLocation = j;
			}
			file << "\t" << frequencies[modeLocation];
			//start at mode
			int upper, lower;
			double area_tmp = freqLogPostProb[modeLocation] * step;
			upper = modeLocation;
			lower = modeLocation;
			int h = 0;
			double desired_area = hpdi[h] * area;
			double area_new;
			while(area_tmp < desired_area){
				//choose bin to add
				if(lower == 0 || freqLogPostProb[upper + 1] > freqLogPostProb[lower - 1]){
					//add from upper side
					++upper;
					area_new = freqLogPostProb[upper] * step;
					if(area_tmp + area_new > desired_area){
						hpdiLocation_upper[h] = (upper + 0.5 - (area_tmp - desired_area) / area_new) * step;
						hpdiLocation_lower[h] = (lower - 0.5) * step;
						--upper;
						++h; if(h==3) break;
						desired_area = hpdi[h] * area;
					} else {
						area_tmp += area_new;
					}
				} else {
					//add from lower side
					--lower;
					area_new = freqLogPostProb[lower] * step;
					if(area_tmp + area_new > desired_area){
						hpdiLocation_lower[h] = (lower - 0.5 + (area_tmp - desired_area) / area_new) * step;
						hpdiLocation_upper[h] = (upper + 0.5) * step;
						++lower;
						++h; if(h==3) break;
						desired_area = hpdi[h] * area;
					} else {
						area_tmp += area_new;
					}
				}
			}

			//print
			for(h=0; h<3; ++h) file << "\t" << hpdiLocation_lower[h] << "\t" << hpdiLocation_upper[h];
			/*
			for(int j=0; j<numFrequencies; ++j){
				file << "\t" << freqLogPostProb[j] / area;
			}
			*/
			file << std::endl;

			//report progress
			percent=100 * (double) i / (double) probVec.size();
			if(percent>oldPercent){
				logfile->listOverFlush("calculating posterior probabilities and writing results to '" + filename + "' ... (" + toString(percent) + "%)");
				oldPercent=percent;
			}
		}
		logfile->overList("calculating posterior probabilities and writing results to '" + filename + "' ... done!   ");

		if(numFilesOk<1 || numSnpsRead==0) reachedEnd=true;
		else oldMaxPos=maxPos;	//reset maxPos
	}
	file.close();
	delete[] frequencies;
	delete[] logFSquared;
	delete[] logTwoFOneMinusF;
	delete[] logOneMinusFSquared;
	delete[] freqLogPostProb;
	delete[] ancestryCombination;
	for(probVecIt = probVec.begin(); probVecIt != probVec.end(); ++probVecIt){
		for(ancestryProbIt = probVecIt->begin(); ancestryProbIt != probVecIt->end(); ++ancestryProbIt){
			delete *ancestryProbIt;
		}
	}
	probVec.clear();
	logfile->endIndent();
}
/*
//---------------------------------------------------------------------------
void TAnalyzer::compileAncestriesInIntervals(){


	//this function parses through all files and writes the expected ancestry for each Interval and each Individual in a
	//format that can then be used to go through all ancetries interval by interval
	//In the current format, this is prohibitive, because to find the ancestries of all individuals for a given SNP
	//one would need to read all individuals in memory, which is impossible.
	//the anticipated format follows the plink map / ped format: one file with all individual filenames listed, one with the ancestries for each SNP

	//NOTE: intervals are read from a map!

	std::cout << "Compile Ancestries in Intervals:" << std::endl;

	//open map with definitions of intervals
	std::string mapfile=myParameters->getParameterString("mapFile");
	std::cout << "    - Bins defined in '" << mapfile << "'" << std::endl;
	TMapEmpiricalBayes* definitionMap=new TMapEmpiricalBayes(mapfile);
	definitionMap->empty();

	//get file names
	getFiles();

	//writing individuals to file
	std::string filename=prefix+"compiledAncestries"+suffix+".individuals";
	std::cout << "    - writing individuals to '" << filename << "' ..." << std::flush;
	ofstream file(filename.c_str());
	if(!file) throw TException("Unable to open file '" + filename +"' for writing!", _FATAL_ERROR);
	for (unsigned int i = 0;i < files.size();i++){
		file << files[i] << std::endl;
	}
	file.close();
	std::cout << " done!" << std::endl;

	//open output file
	filename=prefix+"compiledAncestries"+suffix+".ancestries";
	file.open(filename.c_str());
	if(!file) throw TException("Unable to open file '" + filename +"' for writing!", _FATAL_ERROR);

	//idea is to only read a bunch of intervals per iteration

	//prepare storage
	vector<long> startPos; //I need two columns to be consistent with the per SNP file
	vector<long> midPos;
	vector<double*> ancestries;

	bool reachedEnd=false;
	int intervalsPerIteration=myParameters->getParameterDouble("intervalsPerIteration");
	int numFilesOk=files.size();
	int percent=0;
	int oldPercent=0;

	TMapEmpiricalBayes* map;
	long lastMaxBin=-1;

	while(!reachedEnd){
		std::cout << " - reading batch of " << intervalsPerIteration << " intervals ... (0%)" << std::flush;
		//empty vectors
		startPos.clear();
		midPos.clear();
		for(vector<double*>::iterator it=ancestries.begin(); it!=ancestries.end(); ++it) delete (*it);
		ancestries.clear();

		//get next set of intervals and generate map to be used
		map=new TMapEmpiricalBayes(definitionMap, definitionMap->getStartOfBin(lastMaxBin+1), definitionMap->getEndOfBin(lastMaxBin+intervalsPerIteration));
		lastMaxBin=lastMaxBin+intervalsPerIteration;
		if(lastMaxBin>=definitionMap->getNumBins()) reachedEnd=true;
		for(long h=0; h<map->getNumBins(); ++h){
			startPos.push_back(map->getStartOfBin(h));
			midPos.push_back((map->getStartOfBin(h) + map->getEndOfBin(h))/ 2.0);
		}

		//go through each file, produce ancestry map and save values
		percent=0;
		oldPercent=0;
		for (unsigned int i = 0;i < files.size();++i){
			TInd* myInd=new TInd(files[i], true);
			ancestries.push_back(new double[map->getNumBins()]);
			if(myInd->ok){
				map->empty();
				map->addAncestries(myInd);
				map->scale();
				//save ancestries
				map->fillAncestries(ancestries.back());
			} else {
				double* x=ancestries.back();
				for(long h=0; h<map->getNumBins(); ++h) x[h]=-1;
			}
			delete myInd;
			percent=100 * (double) i / (double) files.size();
			if(percent>oldPercent){
				std::cout << '\xd' << " - reading batch of " << intervalsPerIteration << " intervals ... (" << percent << "%)" << std::flush;
				oldPercent=percent;
			}
		}
		std::cout << '\xd' << " - reading batch of " << intervalsPerIteration << " intervals ... done! " << std::endl;

		//write ancestries to file!
		std::cout << "    - writing ancestries to '" << filename << "' ..." << std::flush;
		for (unsigned int j = 0; j < map->getNumBins(); ++j){
			file << startPos.at(j) << "\t" << midPos.at(j);
			for (unsigned int i = 0;i < files.size();++i){
				file << "\t" << ancestries.at(i)[j];
			}
			file << std::endl;
		}
		std::cout << " done!" << std::endl;

		if(numFilesOk<1) reachedEnd=true;
		delete map;
	}
	file.close();
	delete definitionMap;
	for(vector<double*>::iterator it=ancestries.begin(); it!=ancestries.end(); ++it) delete (*it);

}
*/
//---------------------------------------------------------------------------
void TAnalyzer::associationTest(){
	//this function computes associations between ancestries at SNPs and a phaenotype
	//Ancestries are expected to have th format produced by teh function compileAncestries():
	// - a file BASE.individuals listing all individuals
	// - a file BASE-ancestries listing teh ancestries for each SNP with individuals IN THE SAME ORDER AS IN THE FILE LISTING INDIVIDUALS
	//the phaentotype then is given as a file with
	// - a column lisiting the individual name that HAS TO MATCH THE NAME IN TEH ANCESTRY FILE
	// - multiple columns listing phaentotypes. This function computes the association for each of those column
	// - The file is expected to have a header lisitng the names of the association columns. The first name is ignored and ssumed to correspond to the column lisitng the individuals

	logfile->startIndent("Performing Association tests:");

	//read individuals
	std::string ancestryBaseName=myParameters->getParameterString("ancestryBaseName");
	std::string filename=ancestryBaseName+".individuals";
	logfile->listFlush("reading individuals from '" + filename + ".individuals' ...");
	std::vector<std::string> ancestryInd;
	std::ifstream file;
	file.open(filename.c_str());
	if(!file) throw "Unable to open file '" + filename +"' for reading!";
	std::string buf;
	long line=1;
	std::vector<std::string> val;
	while(file.good() && !file.eof()){
		getline(file, buf);
		trimString(buf);
		if(!buf.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
			if(val.size()!=1) throw "Wrong number of values in file '" + filename + "' on line " + toString(line)+ ": expected 1, found " + toString(val.size()) + "!";
			ancestryInd.push_back(val.at(0));
		}
		++line;
	}
	file.close();
	logfile->write(" done!");
	logfile->conclude("read " + toString(ancestryInd.size()) + " individuals");

	//reading phaenotypes
	std::string phaenoFile=myParameters->getParameterString("phaenoFile");
	logfile->listFlush("reading phaenotypes from '" + phaenoFile + "' ...");
	std::vector<std::string> phaenoInd;
	std::vector<std::string> phaenoNames;
	std::vector< std::vector<double>* > phaeno;

	file.open(phaenoFile.c_str());
	if(!file) throw "Unable to open file '" + phaenoFile +"' for reading!";
	line=1;
	while(file.good() && !file.eof()){
		getline(file, buf);
		trimString(buf);
		if(!buf.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
			if(line==1){
				if(val.size()<2) throw "No phaenotypes given in file '" + phaenoFile +"'!";
				//interpreting the header
				for(std::vector<std::string>::size_type i=1; i<val.size(); ++i){
					phaenoNames.push_back(val.at(i));
					phaeno.push_back(new std::vector<double>);
				}
			} else {
				if(val.size()!=(phaenoNames.size()+1)) throw "Wrong number of values in file '" + phaenoFile + " on line " + toString(line) + ": expected " + toString(1+phaenoNames.size()) + ", found " + toString(val.size()) + "!";
				phaenoInd.push_back(val.at(0));
				for(std::vector<std::string>::size_type i=0; i<phaenoNames.size(); ++i){
					phaeno.at(i)->push_back(stringToDouble(val.at(i+1)));
				}
			}
		}
		++line;
	}
	file.close();
	logfile->write(" done!");
	logfile->conclude("read " + toString(phaenoNames.size()) + " phaenotypes");

	//make vector of overlapping individuals -> make a map linking the two indexes
	logfile->listFlush("matching individuals ...");
	std::map<int, int> ind;
	for(std::vector<std::string>::size_type aInd=0; aInd<ancestryInd.size(); ++aInd){
		for(std::vector<std::string>::size_type pInd=0; pInd<phaenoInd.size(); ++pInd){
			if(phaenoInd.at(pInd)==ancestryInd.at(aInd)){
				ind.insert(std::pair<int,int>(pInd, aInd));
				break;
			}
		}
	}
	logfile->write(" done!");
	logfile->conclude("found " + toString(ind.size()) + " matches");
	if(ind.size()<1) throw "No individuals match between ancestry and phaenotype files!";

	//open output file
	filename=prefix+"associations"+suffix+".txt";
	logfile->list("Writing output to '" + filename + "'");
	std::ofstream ofile(filename.c_str());
	if(!ofile) throw "Unable to open file '" + filename +"' for writing!";
	ofile << "SNP\tpositions\tN";
	for(std::vector<std::string>::size_type i=0; i<phaenoNames.size(); ++i){
		ofile << "\t" << phaenoNames.at(i) << "_COR" << "\t" << phaenoNames.at(i) << "_P";
	}
	ofile << std::endl;

	//now go through the ancestry file and produce assocation
	filename=ancestryBaseName+".ancestries";
	logfile->listFlush("computing associations for file '" + filename + " ... (0 snps)");
	file.open(filename.c_str());
	if(!file) throw "Unable to open file '" + filename +"' for reading!";

	line=1;
	std::string thisSNP;
	std::string thisPos;
	double* thesePhaeno=new double[ind.size()];
	double* theseAncestries=new double[ind.size()];
	int index;
	double value;
	int oldprintSnp=0;
	TCorrelatorDouble* correlator=new TCorrelatorDouble;
	while(file.good() && !file.eof()){
		getline(file, buf);
		fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
		//write SNP and position
		ofile << val[0] << "\t" << val[1];
		if(val.size()>2){
			if(val.size()!=ancestryInd.size()){
				throw "Wrong number of values in file '" + filename + "' on line " + toString(line) + ": expected " + toString(2+ancestryInd.size()) + ", found " + toString(val.size()-2) + "!";
			}

			//fill vectors with ancestries and phaenotypes
			for(std::vector<std::string>::size_type i=0; i<phaenoNames.size(); ++i){
				//prepare vectors
				index=0;
				if(i==0){
					for(std::map<int,int>::iterator it=ind.begin(); it!=ind.end(); ++it){ //first is phaeno, second ancstry
						value=stringToDouble(val[it->second]);
						if(value>=0){
							thesePhaeno[index]=phaeno.at(i)->at(it->first);
							theseAncestries[index]=value;
							++index;
						}
					}
					ofile << "\t" << index;
				} else {
					for(std::map<int,int>::iterator it=ind.begin(); it!=ind.end(); ++it){ //first is phaeno, second ancstry
						value=stringToDouble(val[it->second]);
						if(value>=0){
							thesePhaeno[index]=phaeno.at(i)->at(it->first);
							++index;
						}
					}
				}
				//compute and correlation
				correlator->reset();
				correlator->addDataset(index, theseAncestries);
				correlator->addDataset(index, thesePhaeno);
				ofile << "\t" << correlator->getSpearmanCorrelation();
				ofile << "\t" << correlator->getSpearmanPValue();
			}
			ofile << std::endl;
			if(line-oldprintSnp==1000){
				logfile->listOverFlush("computing associations for file '" + filename + " ... (" + toString(line) + " snps)");
				oldprintSnp=line;
			}
		}
		++line;
	}
	logfile->overList("computing associations for file '" + filename + " ... (" + toString(line) + " snps) done!");
	file.close();
	logfile->endIndent();
}

//---------------------------------------------------------------------------
//Compute average ancestry per individual per
//This HAS TO BE DONE ON A PER CHROMOSOM BASIS
void TAnalyzer::getIndividualAverageAncestry(){
	logfile->startIndent("Compute average ancestry for each individual:");
	getFiles();

	logfile->startIndent("reading limits:");
	start=myParameters->getParameterDouble("start");
	logfile->list("start = ", start);
	end=myParameters->getParameterDouble("end");
	logfile->list("end = ", end);

	//will some SNPs be excluded?
	TExclude* exclude;
	bool doExclude=false;
	std::string excludefile=myParameters->getParameterString("excludeFile", false);
	if(!excludefile.empty()){
		logfile->list("exclude file = " + excludefile);
		exclude=new TExclude(excludefile);
		doExclude=true;
	}

	//prepare output file
	std::ofstream file;
	std::string filename = prefix + "individualAverageAncestry" + suffix + ".txt";
	logfile->list("writing output to '" + filename + "'");
	file.open(filename.c_str());
	//header
	file << "filename\tweightedSum\tsumOfWeights\taverageAncestry" << std::endl;

	//now compute!
	logfile->listFlush("looping through all individuals ... (0%)");
	int percent=0;
	int oldPercent=0;
	double weightedSum, sumOfWeights;
	for(unsigned int i = 0;i < files.size();i++){
		TInd* myInd=new TInd(files[i]);
		if(myInd->ok){
			if(doExclude) myInd->getAverageAncestry(start, end, exclude, weightedSum, sumOfWeights);
			else myInd->getAverageAncestry(start, end, weightedSum, sumOfWeights);
			//write output
			file << files[i] << "\t" << weightedSum << "\t" << sumOfWeights << "\t" << weightedSum/sumOfWeights << std::endl;
		}
		delete myInd;
		percent=100 * (double) i / (double) files.size();
		if(percent>oldPercent){
			logfile->listOverFlush("looping through all individuals ... (" + toString(percent) +  "%)");
			oldPercent=percent;
		}
	}
	file.close();
	logfile->overList("looping through all individuals ... done!   ");
	delete exclude;
}

//---------------------------------------------------------------------------
//Compute ancestry map across individuals
void TAnalyzer::getAncestryMap(){
	//producing recombination maps from the recombest output
	logfile->startIndent("Producing Ancestry Maps from recombest output:");
	getFiles();
	readMapDetails();

	//generate maps
	generateStandardMapObjects();
	//run through all individuals
	loopIndividualsAncestriesOnly();
	//output maps
	writeMaps();
	logfile->endIndent();
}

//---------------------------------------------------------------------------
//Compute ancestry map across individuals
void TAnalyzer::callAncestries(){
	//This function calls ancestry at each popsition for every sample and writes them
	//Works only for two populations at the moment!
	logfile->startIndent("Calling Ancestries:");
	getFiles();

	//MAP or expected genotype?
	bool MAP = false;
	if(myParameters->parameterExists("MAP")){
		logfile->list("will call Maximum a posteriori (MAP) ancestries");
		MAP = true;
	} else {
		logfile->list("will calculate expected ancestry");
	}

	//how to indicate missing calls?
	std::string missingString = myParameters->getParameterStringWithDefault("missingString", "NA");
	logfile->list("will use '" + missingString + "' to denote missing data");

	//writing individuals to file
	std::string filename=prefix+"ancestryCalls"+suffix+".individuals";
	logfile->listFlush("writing individuals to '" + filename + "' ...");
	std::ofstream file(filename.c_str());
	if(!file) throw "Unable to open file '" + filename +"' for writing!";
	for (unsigned int i = 0;i < files.size();i++){
		file << files[i] << std::endl;
	}
	file.close();
	logfile->write(" done!");

	//idea is to only read a bunch of SNPs per iteration
	//some individuals have no ancestry at a SNP, but -1
	//prepare storage
	std::vector<std::string> snp;
	std::vector<long> pos;
	std::vector< std::vector<float> > expectedAncestry;
	std::vector< std::vector<uint8_t> > MAPAncestry;
	std::vector< std::vector<bool> > ancestryCallMissing;

	//other variables
	bool reachedEnd=false;
	int snpsPerIteration = myParameters->getParameterDoubleWithDefault("snpsPerIteration", 10000);
	int numSnpsRead=0;
	long maxPos=0;
	long oldMaxPos=0;
	long currentSnpInDb;
	size_t numFilesOk = files.size();
	int percent = 0;
	int oldPercent = 0;

	//open output file
	filename = prefix+"ancestryCalls"+suffix+".txt";
	file.open(filename.c_str());
	if(!file) throw "Unable to open file '" + filename + "' for writing!";
	file << "snp\tpos";
	for (unsigned int i = 0;i < files.size();i++){
		file << "\t" << files[i];
	}
	file << std::endl;

	//go through files
	while(!reachedEnd){
		logfile->listFlush("reading batch of " + toString(snpsPerIteration) + " SNPs ... (0%)");
		//empty vectors
		snp.clear();
		pos.clear();
		MAPAncestry.clear();
		expectedAncestry.clear();
		ancestryCallMissing.clear();

		//go through each file and read SNPs
		numSnpsRead = 0;
		percent=0;
		oldPercent=0;
		for (size_t i = 0;i < files.size();++i){
			TInd* myInd = new TInd(files[i], true);
			if(myInd->ok){
				if(myInd->numRefPop != 2) throw "Ancestry calling is only implemented for the case of two ancestries!";

				//start iterators in individual
				std::vector<std::string>::iterator snpIt = myInd->snpNames.begin();
				std::vector<long>::iterator posIt = myInd->indPositions.begin();
				std::vector<double*>::iterator probIndAncIt = myInd->probAnc.begin();

				//skip SNPs already written
				while((*posIt)<=oldMaxPos && posIt!=myInd->indPositions.end()){
					++posIt; ++snpIt; ++probIndAncIt;
				}

				//read snp by snp and check if it is already present
				while((numSnpsRead<snpsPerIteration || (*posIt)<=maxPos) && snpIt != myInd->snpNames.end() && posIt != myInd->indPositions.end() && probIndAncIt != myInd->probAnc.end()){
					//snp is present?
					currentSnpInDb = -1;
					for(std::vector<std::string>::size_type idInDB=0; idInDB<snp.size(); ++idInDB){
						if(pos[idInDB] == (*posIt)){
							currentSnpInDb = idInDB;
							break;
						}
					}

					if(currentSnpInDb < 0){ //SNP not yet known
						snp.push_back((*snpIt));
						pos.push_back((*posIt));

						ancestryCallMissing.emplace_back(files.size(), true);
						MAPAncestry.emplace_back(files.size(), 0);
						expectedAncestry.emplace_back(files.size(), 0.0);

						currentSnpInDb = numSnpsRead;
						++numSnpsRead;
						if((*posIt)>maxPos) maxPos=(*posIt);
					}

					//add call
					ancestryCallMissing[currentSnpInDb][i] = false;
					if(MAP){
						//add MAP ancestry
						//get max
						double max = (*probIndAncIt)[0];
						if((*probIndAncIt)[1] > max) max = (*probIndAncIt)[1];
						if((*probIndAncIt)[2] > max) max = (*probIndAncIt)[2];

						//sample among max
						std::vector<uint8_t> vec;
						if((*probIndAncIt)[0] == max) vec.push_back(0);
						if((*probIndAncIt)[1] == max) vec.push_back(1);
						if((*probIndAncIt)[2] == max) vec.push_back(2);

						MAPAncestry[currentSnpInDb][i] = vec[randomGenerator->pickOne(vec.size())];
					} else {
						//calculate expected ancestry
						expectedAncestry[currentSnpInDb][i] = (*probIndAncIt)[1] + 2.0 * (*probIndAncIt)[2];
					}

					//move to next SNP
					++posIt; ++snpIt; ++probIndAncIt;
					if(posIt==myInd->indPositions.end()) --numFilesOk;
				}
			}
			delete myInd;
			percent = 100 * (double) i / (double) files.size();
			if(percent>oldPercent){
				logfile->listOverFlush("reading batch of " + toString(snpsPerIteration) + " SNPs ... (" + toString(percent) + "%)");
				oldPercent=percent;
			}
		}
		logfile->overList("reading batch of " + toString(snpsPerIteration) + " SNPs ... done!   ");

		//now write out calls
		logfile->listFlush("writing ancestry calls ...");
		std::vector<std::string>::iterator snpIt = snp.begin();
		std::vector<long>::iterator posIt = pos.begin();
		std::vector< std::vector<uint8_t> >::iterator MAPIt = MAPAncestry.begin();
		std::vector< std::vector<float> >::iterator expIt = expectedAncestry.begin();
		std::vector< std::vector<bool> >::iterator missingIt = ancestryCallMissing.begin();

		for(; snpIt != snp.end(); ++snpIt, ++posIt, ++MAPIt, ++expIt, ++missingIt){
			//write snp identifiers
			file << *snpIt << "\t" << *posIt;

			//write MAP or expected ancestry
			if(MAP){
				for(size_t i = 0; i<files.size(); ++i){
					if((*missingIt)[i]){
						file << "\t" << missingString;
					} else {
						file << "\t" << (int) (*MAPIt)[i];
					}
				}
			} else {
				for(size_t i = 0; i<files.size(); ++i){
					if((*missingIt)[i]){
						file << "\t" << missingString;
					} else {
						file << "\t" << (*expIt)[i];
					}
				}
			}
			file << std::endl;
		}
		logfile->done();

		if(numFilesOk<1 || numSnpsRead==0) reachedEnd=true;
		else oldMaxPos=maxPos;	//reset maxPos
	}

	//clean up
	file.close();
	logfile->endIndent();
}
