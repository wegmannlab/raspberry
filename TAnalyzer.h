/*
 * TAnalyzer.h
 *
 *  Created on: Dec 30, 2010
 *      Author: wegmannd
 */

#ifndef TANALYZER_H_
#define TANALYZER_H_

#include <sys/types.h>
#include <dirent.h>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>

#include "TParameters.h"
#include "TIndividual.h"
#include "TMap.h"
#include "TIntervalDB.h"
#include "TExclude.h"
#include "TCorrelator.h"
#include "TCorrelatorDouble.h"
#include "TRandomGenerator.h"
#include "TLog.h"

class TAncestryProbs{
public:
	double probHomA, probHet, probHomB;
	double probHomAPlusHet;

	TAncestryProbs(double ProbHomA, double ProbHet){
		probHomA = ProbHomA;
		probHet = ProbHet;
		probHomAPlusHet = probHomA + probHet;
		probHomB = 1.0 -probHomAPlusHet;
	};

	int sample(double rand){
		if(rand < probHomA) return 0;
		else if(rand < probHomAPlusHet) return 1;
		else return 2;
	};

};

class TAnalyzer{
private:
	std::vector<std::string> files;
	TParameters* myParameters;
	long start, end;
	double increment;
	std::vector<long> windowSizes;
	std::string prefix, suffix;
	std::vector<TMapEmpiricalBayes*> maps;
	TRandomGenerator* randomGenerator;
	TLog* logfile;

public:

	TAnalyzer(TParameters* Parameters, TLog* Logfile);
	~TAnalyzer(){
		for(std::vector<TMapEmpiricalBayes*>::iterator it=maps.begin(); it!=maps.end(); ++it) delete *it;
		maps.clear();
		delete randomGenerator;
	};
	void getFiles();
	void readMapDetails();
	void produceMaps();
	void produceStandardMaps();
	void produceEmpiricalBayesMaps();
	void generateStandardMapObjects();
	void writeMaps();
	void produceIntervalDB();
	void loopIndividuals();
	void loopIndividualsProbabilitiesOnly();
	void loopIndividualsAncestriesOnly();
	void produceHotspotUsage();
	void produceHotspotUsageEmpiricalBayes();
	void importMap();
	void callSwitchpoints();
	void computeEnrichment();
	void updateMap();
	void chopMap();
	void computeCorrelations();
	void fillCorrelationsBetweenMaps(TCorrelator* corr, double & trimFrac, std::string & transformation, long numBins, double* ratesMap1, double* ratesMap2, double & spearman, double & pearson);
	void fillCorrelationsBetweenMapsRanks(TCorrelator* corr, double & trimFrac, std::string & transformation, long numBins, double* ratesMap1, double* ratesMap2, double* ranksMap1, double* ranksMap2, double & spearman, double & pearson);
	void computeAndWriteTwoWayCorrelations(TCorrelator* corr, std::ofstream & out, double & trimFrac, std::string & transformation, int bootstrap, long numBins, double* ratesMap1, double* ratesMap2, double* ratesMap3, double* ranksMap1, double* ranksMap2, double* ranksMap3);
	void computeTwoWayCorrelations();
	void fillBootstrappedDataForCorrelations(long length, double* source1, double* source2, double* source3, double** fill);
	void writeLargesDifferences();
	void compileAncestries();
	//void compileAncestriesInIntervals();
	void calculatePopulationAncestry();
	void associationTest();
	void getIndividualAverageAncestry();
	void getAncestryMap();
	void callAncestries();
};


#endif /* TANALYZER_H_ */
