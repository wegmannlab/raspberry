/*
 * TMap.cpp
 *
 *  Created on: Sep 2, 2010
 *      Author: wegmannd
 */

#include "TBasicMap.h"

TBasicMap::TBasicMap(){
	recombination=false;
	ancestries=false;
	binsStandardized=false;
	ancestriesStandardized=false;
	ancestryStorageInitialized = false;
	nonOverlappingBinLenghtComputed = false;
	numInd=0.0;
	numBins=0;
	bins = NULL;
	ancestryPerPop = NULL;
	numRefPop = -1;
	binStart = NULL;
	binEnd = NULL;
	step = -1;
	start = -1;
	end = -1;
	windowsize = -1;
	nonOverlappingBinLenght = -1.0;
};
//--------------------------------------------------------------------
TBasicMap::TBasicMap(long Windowsize, long Start, long End){
	start=Start;
	end=End;
	windowsize=Windowsize;
	step=windowsize;
	initialize();
}
TBasicMap::TBasicMap(long Windowsize, long Start, long End, double Increment){
	start=Start;
	end=End;
	windowsize=Windowsize;
	step=ceil(Increment*(double)Windowsize);
	initialize();
}
TBasicMap::TBasicMap(long Windowsize, long Start, long End, long Step){
	start=Start;
	end=End;
	windowsize=Windowsize;
	step=Step;
	initialize();
}
//--------------------------------------------------------------------
void TBasicMap::initialize(){
	//check start and end
	if(end<start) throw "Fail to initialize map, end < start!";
	if(start<0) throw "Fail to initialize map, start < 0!";
	if(end<0) throw "Fail to initialize map, end < 0!";
	if((end-start)<windowsize) throw "Fail to initialize map, window size is larger than (end -start)!";
	//some indicators
	recombination=false;
	ancestries=false;
	binsStandardized=false;
	ancestryStorageInitialized=false;
	ancestriesStandardized=false;

	numInd=0.0;
	//prepare bins
	numBins=1+ceil((double)(end-start-windowsize-1)/(double)step);
	bins=new double[numBins];

	binStart=new long[numBins];
	binEnd=new long[numBins];
	for(int i=0; i<numBins; ++i){
		bins[i]=0;
		binStart[i]=start+i*step;
		binEnd[i]=start+i*step+windowsize;
	}
	binEnd[numBins-1]=end;
	//construct output filename
	filename="window_" + toString(windowsize) + "_step_" + toString(step);
}

void TBasicMap::initializeAncestryStorage(int NumRefPop){
	//error when doing twice!
	if(ancestryStorageInitialized) throw "Ancestry storage has already been initialized!";

	numRefPop=NumRefPop;
	ancestryPerPop=new double*[numRefPop];
	for(int p=0; p<numRefPop; ++p) ancestryPerPop[p]=new double[numBins];
	for(int i=0; i<numBins; ++i){
		for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]=0;
	}
	ancestryStorageInitialized=true;
	ancestriesStandardized=false;
}

TBasicMap::~TBasicMap(){
	delete[] bins;
	if(ancestryStorageInitialized){
		for(int p=0; p<numRefPop; ++p) delete[] ancestryPerPop[p];
		delete[] ancestryPerPop;
	}

	delete[] binStart;
	delete[] binEnd;
}
//--------------------------------------------------------------------
TBasicMap::TBasicMap(TBasicMap* map){
	//construct bins from an other map
	//create bins
	numBins=map->numBins;
	bins=new double[numBins];
	binStart=new long[numBins];
	binEnd=new long[numBins];
	for(int i=0; i<map->numBins; ++i){
		bins[i]=0;
		binStart[i]=map->binStart[i];
		binEnd[i]=map->binEnd[i];
	}
	//ancestries
	if(map->ancestryStorageInitialized){
		initializeAncestryStorage(map->numRefPop);
		for(int i=0; i<numBins; ++i){
			for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]=map->ancestryPerPop[p][i];
		}
	}
	//other
	numInd=0.0;
	windowsize=map->windowsize;
	filename="copy_of_";
	filename+=map->filename;

	recombination=false;
	ancestries=false;
	binsStandardized=false;
	ancestriesStandardized=false;
}
TBasicMap::TBasicMap(TBasicMap* map, long start, long end){
	//check start and end
	if(end<start) throw "Fail to initialize map, end < start!";
	if(start<0) throw "Fail to initialize map, start < 0!";
	if(end<0) throw "Fail to initialize map, end < 0!";
	//construct bins from an other map
	//check if bins are within limits...
	long s=0;
	while(map->binStart[s] < start) ++s;
	long e=map->numBins-1;
	while(map->binEnd[e] > end) --e;

	//copy the bins...
	//create bins
	numBins=(e-s)+1;
	bins=new double[numBins];
	binStart=new long[numBins];
	binEnd=new long[numBins];

	for(long i=0; i<numBins; ++i){
		bins[i]=0;
		binStart[i]=map->binStart[i+s];
		binEnd[i]=map->binEnd[i+s];
	}
	//ancestries
	if(map->ancestryStorageInitialized){
		initializeAncestryStorage(map->numRefPop);
		for(int i=0; i<numBins; ++i){
			for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]=map->ancestryPerPop[i+s][p];
		}
	}

	//other
	numInd=0.0;
	windowsize=map->windowsize;
	filename="copy_of_"+map->filename;

	recombination=false;
	ancestries=false;
	binsStandardized=false;
	ancestriesStandardized=false;


}
//--------------------------------------------------------------------
TBasicMap::TBasicMap(std::string filename, std::string format){
	//construct from a specific file format
	if(format=="hapmap") readHapMapFile(filename);
	else if(format=="decode") readDeCodeFile(filename);
	else throw "Unknown file format '" + format + "'!";
}
//--------------------------------------------------------------------
TBasicMap::TBasicMap(std::string filename, std::string format, long start, long end){
	//construct from a specific file format
	if(format=="definition") readDefinitionFile(filename, start, end);
	else throw "Unknown file format '" + format + "'!";
}
//--------------------------------------------------------------------
double* TBasicMap::getPointerToRecombination(){
	if(!recombination) throw "getPointerToRecombination: recombination rates have not yet been computed!";
	return bins;
}
//--------------------------------------------------------------------
void TBasicMap::readHapMapFile(std::string hapmapfile){
	recombination=true;
	ancestries=false;
	binsStandardized=false;
	ancestriesStandardized=false;
	ancestryStorageInitialized=false;
	//read a hapmap file
	std::ifstream file;
	file.open(hapmapfile.c_str());
	if(!file){
		throw "File '" + hapmapfile + " could not be read!";
	}

	std::vector<long> pos;
	std::vector<double> rate;
	std::string buf;
	getline(file, buf);
	long line=1;
	std::vector<std::string> val;
	while(file.good() && !file.eof()){
		getline(file, buf);
		trimString(buf);
		if(!buf.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
			if(val.size()!=3){
				throw "Wrong number of values in file '" + hapmapfile + " on line " + toString(line) + "!";
			}
			pos.push_back(stringToLong(val[0]));
			rate.push_back(stringToDouble(val[1]));
		}
		++line;
	}
	file.close();

	//create bins
	numBins=pos.size()-1;
	bins=new double[numBins];
	binStart=new long[numBins];
	binEnd=new long[numBins];

	for(int i=0; i<numBins; ++i){
		bins[i]=rate.at(i);
		binStart[i]=pos.at(i);
		binEnd[i]=pos.at(i+1);
		if(binEnd[i]<=binStart[i]) throw "Positions in file '" + hapmapfile + " are not ordered! Is it a HapMap map file?";
	}

	//other
	numInd=0.0;
	windowsize=-1;
	filename=extractBeforeLast(hapmapfile, '.');
}
//--------------------------------------------------------------------
void TBasicMap::readDeCodeFile(std::string decodeFile){
	recombination=true;
	ancestries=false;
	binsStandardized=false;
	ancestriesStandardized=false;
	ancestryStorageInitialized=false;
	//read a hapmap file
	std::ifstream file;
	file.open(decodeFile.c_str());
	if(!file){
		throw "File '" + decodeFile + " could not be read!";
	}

	std::vector<long> pos;
	std::vector<double> rate;
	std::string buf;
	getline(file, buf);
	long line=1;
	std::vector<std::string> val;
	while(file.good() && !file.eof()){
		getline(file, buf);
		trimString(buf);
		if(!buf.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
			if(val.size()!=4){
				throw "Wrong number of values in file '" + decodeFile + " on line " + toString(line) + "!";
			}
			pos.push_back(stringToLong(val[1]));
			rate.push_back(stringToDouble(val[3]));
		}
		++line;
	}
	file.close();

	//create bins
	numBins=pos.size();
	bins=new double[numBins];
	binStart=new long[numBins];
	binEnd=new long[numBins];

	//given are midpoints, so we need the spacing  (should be 10Kb). We assuem non-overlappign bins
	long halfdiff=(double) (pos[1]-pos[0]) / 2.0;
	//if(pos[1]-pos[0]!=10000) throw "Wrong bin spacing in file '" + decodeFile + ", currently only non-overlappign 10Kb maps can be read!";
	long oldPos=-1;
	for(int i=0; i<numBins; ++i){
		if(pos.at(i)<=oldPos) throw "Positions in file '" + decodeFile + " are not ordered! Is it a decode map file?";
		oldPos=pos.at(i);
		bins[i]=rate.at(i);
		binStart[i]=pos.at(i)-halfdiff;
		binEnd[i]=pos.at(i)+halfdiff;
	}

	//other
	numInd=0.0;
	windowsize=2*halfdiff;
	filename=extractBeforeLast(decodeFile, '.');
}
//--------------------------------------------------------------------
void TBasicMap::readDefinitionFile(std::string definitionFile, long start, long end){
	//check start and end
	if(end<start) throw "Fail to initialize map, end < start!";
	if(start<0) throw "Fail to initialize map, start < 0!";
	if(end<0) throw "Fail to initialize map, end < 0!";
	//construct bins from a file name (e.g. hotspot definition)
	recombination=false;
	ancestries=false;
	binsStandardized=false;
	ancestriesStandardized=false;
	ancestryStorageInitialized=false;
	windowsize=-1;
	std::ifstream file;
	file.open(definitionFile.c_str());
	if(!file){
		throw "File '" + definitionFile + " could not be read!";
	}

	std::vector<long> startVec;
	std::vector<long> endVec;
	long tempStart, tempEnd;
	std::string buf;
	long line=1;
	std::vector<std::string> val;
	while(file.good() && !file.eof()){
		getline(file, buf);
		trimString(buf);
		if(!buf.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
			if(val.size()!=2){
				throw "Wrong number of values in file '" + definitionFile + " on line " + toString(line) + "!";
			}
			tempStart=stringToLong(val[0]);
			tempEnd=stringToLong(val[1]);
			if(tempStart>start && tempEnd<end){
				startVec.push_back(tempStart);
				endVec.push_back(tempEnd);
			} else {
				if(tempEnd>start && tempStart<end){
					//only a fraction!
					if(tempEnd>end) endVec.push_back(end);
					else endVec.push_back(tempEnd);
					if(tempStart<start) startVec.push_back(start);
					else startVec.push_back(tempStart);
				}
			}
		}
		++line;
	}
	file.close();

	//create bins
	numBins=startVec.size();
	bins=new double[numBins];
	binStart=new long[numBins];
	binEnd=new long[numBins];
	for(int i=0; i<numBins; ++i){
		bins[i]=0;
		binStart[i]=startVec.at(i);
		binEnd[i]=endVec.at(i);
	}

	//other
	numInd=0.0;
	filename=extractBeforeLast(definitionFile, '.');
}
//--------------------------------------------------------------------
void TBasicMap::addMapWithSameBins(TBasicMap & map){
	if(!checkBins(&map)) throw "Merging of unequal maps!";
	//rate
	for(int i=0; i<numBins; ++i){
		bins[i]+=map.bins[i];
	}
	numInd+=map.numInd;
	//ancestries
	if(ancestryStorageInitialized){
		if(!map.ancestryStorageInitialized || map.numRefPop!=numRefPop) throw "Merging of unequal maps!";
		for(int i=0; i<numBins; ++i){
			for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]=map.ancestryPerPop[p][i];
		}
	}
}
//--------------------------------------------------------------------
void TBasicMap::empty(){
	//rates
	for(int i=0; i<numBins; ++i){
		bins[i]=0;
	}
	//ancestry
	emptyAncestries();
	numInd=0.0;
	binsStandardized=false;
}
//--------------------------------------------------------------------
void TBasicMap::emptyAncestries(){
	//ancestry
	if(ancestryStorageInitialized){
		for(int i=0; i<numBins; ++i){
			for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]=0;
		}
	}
	numInd=0.0;
	ancestriesStandardized=false;
}
//--------------------------------------------------------------------
void TBasicMap::addProbabilities(TInd* oneInd){
	if(binsStandardized) throw "addProbabilities: Impossible to add individuals after the map has been standardized!";
	//AFTER an individual has been read...
	std::vector<long>::iterator posIt=oneInd->indPositions.begin();
	std::vector<double>::iterator prob1It=oneInd->probChr1.begin();
	std::vector<double>::iterator prob2It=oneInd->probChr2.begin();
	//iterators to stor staring positions of old bin (BINS ARE OVERLAPPING!)
	std::vector<long>::iterator posIt_old;
	std::vector<double>::iterator prob1It_old;
	std::vector<double>::iterator prob2It_old;
	long oldPos_old;

	long oldPos=(*posIt); //probabilities are always from last to this SNP
	bool go;
	++posIt; ++prob1It; ++prob2It;
	for(long i=0; i<numBins; ++i){
		while((*posIt)<binStart[i] && posIt!=oneInd->indPositions.end()){
			oldPos=(*posIt);
			++posIt; ++prob1It; ++prob2It;
		}
		if(posIt==oneInd->indPositions.end()) break;
		//save starting positions for next bin
		posIt_old=posIt;
		prob1It_old=prob1It;
		prob2It_old=prob2It;
		oldPos_old=oldPos;

		//now fill in
		if(oldPos<binEnd[i]) go=true;
		else go=false;
		while(go){
			 if(oldPos<binStart[i]){
				//only part ...
				if((*posIt)<binEnd[i]){
					bins[i]+=(double)((*posIt)-binStart[i])/(double)((*posIt)-oldPos)*((*prob1It)+(*prob2It));
				} else {
					bins[i]+=(double)(binEnd[i]-binStart[i])/(double)((*posIt)-oldPos)*((*prob1It)+(*prob2It));
				}
			} else {
				if((*posIt)<binEnd[i]){
					//all
					bins[i]+=(*prob1It)+(*prob2It);
				} else {
					bins[i]+=(double)(binEnd[i]-oldPos)/(double)((*posIt)-oldPos)*((*prob1It)+(*prob2It));
				}
			}
			if((*posIt)<binEnd[i]){
				//move to next SNP
				oldPos=(*posIt);
				++posIt; ++prob1It; ++prob2It;
				if(posIt==oneInd->indPositions.end())break;
			} else {
				//next bin
				go=false;
				posIt=posIt_old;
				prob1It=prob1It_old;
				prob2It=prob2It_old;
				oldPos=oldPos_old;
			}
		 }
		 if(posIt==oneInd->indPositions.end())break;
	}
	numInd+=1.0;
	recombination=true;
}
//--------------------------------------------------------------------
void TBasicMap::addProbabilities(TBasicMap & map){
	if(binsStandardized) throw "addProbabilities: Impossible to add individuals after the map has been standardized!";
	bool go;
	long mapP=0;
	long mapP_old;
	for(int i=0; i<numBins; ++i){
		while(map.binEnd[mapP]<binStart[i] && mapP<map.numBins){
			++mapP;
		}
		mapP_old=mapP; //store this because BINS MAY BE OVERLAPPING!
		if(mapP==map.numBins) break;
		if(map.binStart[mapP]<binEnd[i]) go=true;
		else go=false;
		while(go){
			 if(map.binStart[mapP]<binStart[i]){
				//only part ...
				if(map.binEnd[mapP]<binEnd[i]){
					bins[i]+=(double)(map.binEnd[mapP]-binStart[i])/100000000.0*map.bins[mapP];
				} else {
					bins[i]+=(double)(binEnd[i]-binStart[i])/100000000.0*map.bins[mapP];
				}
			} else {
				if(map.binEnd[mapP]<binEnd[i]){
					//all
					bins[i]+=(double)(map.binEnd[mapP]-map.binStart[mapP])/100000000.0*map.bins[mapP];
				} else {
					bins[i]+=(double)(binEnd[i]-map.binStart[mapP])/100000000.0*map.bins[mapP];
				}
			}
			if(map.binEnd[mapP]<binEnd[i]){
				//get to next bin in Map adding from
				++mapP;
				if(mapP>=map.numBins) break;
			} else {
				//fill in next bin of this map
				//_> go back to where we were before teh last bin was filled in BECAUSE BINS ARE OVERLAPPING
				go=false;
				mapP=mapP_old;
			}
		 }
		if(mapP>=map.numBins) break;
	}
	numInd+=0.5;
	recombination=true;
}
//------------------------------------------------------------------------------------
void TBasicMap::addAncestries(TInd* oneInd){
	if(ancestriesStandardized) throw "addAncestries: Impossible to add after the map has been standardized!";
	if(!ancestryStorageInitialized) initializeAncestryStorage(oneInd->numRefPop);
	if(numRefPop != oneInd->numRefPop) throw "addAncestries: Wrong number of reference populations!";
	//AFTER an individual has been read...
	std::vector<long>::iterator posIt=oneInd->indPositions.begin();
	std::vector<double*>::iterator ancIt=oneInd->probAnc.begin();
	//iterators to store staring positions of old bin (BINS ARE OVERLAPPING!)
	std::vector<long>::iterator posIt_old;
	std::vector<double*>::iterator ancIt_old;
	long oldPos_old;
	double* oldAnc_old=new double[numRefPop];
	double* oldAnc=new double[numRefPop];

	long oldPos=(*posIt); //probabilities are always from last to this SNP

	for(int p=0; p<numRefPop; ++p) oldAnc[p]=oneInd->getAncestry(*ancIt, p);

	bool go;
	++posIt; ++ancIt;
	for(long i=0; i<numBins; ++i){
		while((*posIt)<binStart[i] && posIt!=oneInd->indPositions.end()){
			oldPos=(*posIt);
			for(int p=0; p<numRefPop; ++p) oldAnc[p]=oneInd->getAncestry(*ancIt, p);
			++posIt; ++ancIt;
		}
		if(posIt==oneInd->indPositions.end()) break;
		//save where we were
		posIt_old=posIt;
		ancIt_old=ancIt;
		oldPos_old=oldPos;
		for(int p=0; p<numRefPop; ++p) oldAnc_old[p]=oldAnc[p];
		oldAnc_old=oldAnc;

		//now fill in
		if(oldPos<binEnd[i]) go=true;
		else go=false;
		while(go){
			 if(oldPos<binStart[i]){
				double mid=(double)(oldPos+(*posIt))/2.0;
				if(mid<binStart[i]){
					if((*posIt)<binEnd[i]){
						for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=((*posIt)-binStart[i]) * oneInd->getAncestry(*ancIt, p);
					} else {
						for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=(binEnd[i]-binStart[i]) * oneInd->getAncestry(*ancIt, p);
					}
				} else {
					if(mid<binEnd[i]){
						for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=(mid-binStart[i]) * oldAnc[p];
						//now add new part
						if((*posIt)<binEnd[i]){
							for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=((*posIt)-mid) * oneInd->getAncestry(*ancIt, p);
						} else {
							for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=(binEnd[i]-mid) * oneInd->getAncestry(*ancIt, p);
						}
					} else {
						for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=(binEnd[i]-binStart[i]) * oldAnc[p];
					}
				}
			} else {
				if((*posIt)<binEnd[i]){
					//average between the two ancestries
					for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=((*posIt)-oldPos)*(oneInd->getAncestry(*ancIt, p) + oldAnc[p])/2.0;
				} else {
					double mid=(double)(oldPos+(*posIt))/2.0;
					if(mid>binEnd[i]){
						for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=(binEnd[i]-oldPos) * oldAnc[p];
					} else {
						for(int p=0; p<numRefPop; ++p){
							ancestryPerPop[p][i]+=(mid-oldPos) * oldAnc[p];
							ancestryPerPop[p][i]+=(binEnd[i]-mid) * oneInd->getAncestry(*ancIt, p);
						}
					}
				}
			}
			if((*posIt)<binEnd[i]){
				//move to next SNP
				oldPos=(*posIt);
				for(int p=0; p<numRefPop; ++p) oldAnc[p]=oneInd->getAncestry(*ancIt, p);
				++posIt; ++ancIt;
				if(posIt==oneInd->indPositions.end())break;
			} else {
				//move to next bin
				//go back to where we were because BINS MAY BE OVERLAPPING
				go=false;
				posIt=posIt_old;
				ancIt=ancIt_old;
				oldPos=oldPos_old;
				oldAnc=oldAnc_old;
			}
		}
		if(posIt==oneInd->indPositions.end())break;
	}
	numInd+=1.0;
	ancestries=true;
}
//------------------------------------------------------------------------------------
void TBasicMap::addProbabilitiesAndAncestries(TInd* oneInd){
	if(ancestriesStandardized) throw "addProbabilitiesAndAncestries: Impossible to add after the map has been standardized!";
	if(binsStandardized) throw "addProbabilitiesAndAncestries: Impossible to add individuals after the map has been standardized!";
	if(!ancestryStorageInitialized) initializeAncestryStorage(oneInd->numRefPop);
	if(numRefPop != oneInd->numRefPop) throw "addAncestries: Wrong number of reference populations!";
	//AFTER an individual has been read...
	std::vector<long>::iterator posIt=oneInd->indPositions.begin();
	std::vector<double*>::iterator ancIt=oneInd->probAnc.begin();
	std::vector<double>::iterator prob1It=oneInd->probChr1.begin();
	std::vector<double>::iterator prob2It=oneInd->probChr2.begin();
	//iterators to stor staring positions of old bin (BINS ARE OVERLAPPING!)
	std::vector<long>::iterator posIt_old;
	std::vector<double*>::iterator ancIt_old;
	std::vector<double>::iterator prob1It_old;
	std::vector<double>::iterator prob2It_old;
	long oldPos_old;
	double* oldAnc=new double[numRefPop];

	long oldPos=(*posIt); //probabilities are always from last to this SNP
	for(int p=0; p<numRefPop; ++p) oldAnc[p]=oneInd->getAncestry(*ancIt, p);
	bool go;
	++posIt; ++ancIt;
	for(long i=0; i<numBins; ++i){
		while((*posIt)<binStart[i] && posIt!=oneInd->indPositions.end()){
			oldPos=(*posIt);
			for(int p=0; p<numRefPop; ++p) oldAnc[p]=oneInd->getAncestry(*ancIt, p);
			++posIt; ++ancIt; ++prob1It; ++prob2It;
		}

		if(posIt==oneInd->indPositions.end()) break;
		//save where we were
		posIt_old=posIt;
		ancIt_old=ancIt;
		prob1It_old=prob1It;
		prob2It_old=prob2It;
		oldPos_old=oldPos;

		//now fill in
		if(oldPos<binEnd[i]) go=true;
		else go=false;
		while(go){
			 if(oldPos<binStart[i]){
				//only part ...
				if((*posIt)<binEnd[i]){
					bins[i]+=(double)((*posIt)-binStart[i])/(double)((*posIt)-oldPos)*((*prob1It)+(*prob2It));
				} else {
					bins[i]+=(double)(binEnd[i]-binStart[i])/(double)((*posIt)-oldPos)*((*prob1It)+(*prob2It));
				}
				double mid=(oldPos+(*posIt))/2;
				if(mid<binStart[i]){
					if((*posIt)<binEnd[i]){
						for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=((*posIt)-binStart[i]) * oneInd->getAncestry(*ancIt, p);
					} else {
						for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=(binEnd[i]-binStart[i]) * oneInd->getAncestry(*ancIt, p);
					}
				} else {
					for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=(mid-binStart[i]) * oldAnc[p];
					//now add new part
					if((*posIt)<binEnd[i]){
						for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=((*posIt)-mid) * oneInd->getAncestry(*ancIt, p);
					} else {
						for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=(binEnd[i]-mid) * oneInd->getAncestry(*ancIt, p);
					}
				}
			} else {
				if((*posIt)<binEnd[i]){
					//all
					bins[i]+=(*prob1It)+(*prob2It);
					//average between the two ancestries
					for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=((*posIt)-oldPos)*(oneInd->getAncestry(*ancIt, p) + oldAnc[p])/2.0;
				} else {
					bins[i]+=(double)(binEnd[i]-oldPos)/(double)((*posIt)-oldPos)*((*prob1It)+(*prob2It));
					double mid=(oldPos+(*posIt))/2;

					if(mid>binEnd[i]){
						for(int p=0; p<numRefPop; ++p) ancestryPerPop[p][i]+=(binEnd[i]-oldPos) * oldAnc[p];
					} else {
						for(int p=0; p<numRefPop; ++p){
							ancestryPerPop[p][i]+=(mid-oldPos) * oldAnc[p];
							ancestryPerPop[p][i]+=(binEnd[i]-mid) * oneInd->getAncestry(*ancIt, p);
						}
					}
				}
			}
			if((*posIt)<binEnd[i]){
				//move to next SNP
				oldPos=(*posIt);
				for(int p=0; p<numRefPop; ++p) oldAnc[p]=oneInd->getAncestry(*ancIt, p);
				++posIt; ++ancIt; ++prob1It; ++prob2It;
				if(posIt==oneInd->indPositions.end())break;
			} else {
				//go back to where we were because BINS MAY BE OVERLAPPING
				go=false;
				posIt=posIt_old;
				ancIt=ancIt_old;
				prob1It=prob1It_old;
				prob2It=prob2It_old;
				oldPos=oldPos_old;
			}
		}
		if(posIt==oneInd->indPositions.end())break;
	}
	numInd+=1.0;
	ancestries=true;
	recombination=true;
}
//------------------------------------------------------------------------------------
void TBasicMap::scale(){
	//scale from individuals to cm/mb
	if(numInd>0){
		// /2*numInd (per chromosome) / windowsize * 10^6 (per megabse) * 100 (in cM)
		if(recombination && !binsStandardized) {
			for(int i=0; i<numBins; ++i){
				bins[i]=bins[i] * 100000000 / (2*numInd * (binEnd[i]-binStart[i])) ;
			}
			binsStandardized=true;
		}
		if(ancestries && !ancestriesStandardized){
			for(int i=0; i<numBins; ++i){
				for(int p=0; p<numRefPop; ++p)
					ancestryPerPop[p][i]=ancestryPerPop[p][i]/(2*numInd*(binEnd[i]-binStart[i]));
			}
			ancestriesStandardized=true;
		}
	}
}

double TBasicMap::getTotalRate(){
	double sum=0;
	for(int i=0; i<numBins; ++i) sum+=bins[i];
	return sum;
}

double TBasicMap::getAverageRatePerBasepair(){
	return getTotalRate()/getTotalBinLength();
}

double TBasicMap::getTotalAncestry(int refPop){
	double sum=0;
	for(int i=0; i<numBins; ++i) sum+=ancestryPerPop[refPop][i];
	return sum;
}

double TBasicMap::getAverageAncestry(int refPop){
	double sum=0;
	double weight=0;
	for(int i=0; i<numBins; ++i){
		weight+=(binEnd[i]-binStart[i]);
		sum+=(binEnd[i]-binStart[i])*ancestryPerPop[refPop][i];
	}
	return sum/weight/(double)(2*numInd);
}

double TBasicMap::getTotalBinLength(){
	double sum=0;
	for(int i=0; i<numBins; ++i) sum+=binEnd[i]-binStart[i];
	return sum;
}

double TBasicMap::getNonOverlappingBinLength(){
	if(!nonOverlappingBinLenghtComputed){
		//first sort bins by start -> sort id only!
		std::vector< std::pair<long, long> > sortvec;
		for(long i=0; i<numBins; ++i){
			sortvec.push_back(std::pair<long, long>(binStart[i], i));
		}
		sort(sortvec.begin(), sortvec.end());

		nonOverlappingBinLenght=0;
		long oldEnd=-1;
		for(std::vector< std::pair<long, long> >::iterator it=sortvec.begin(); it!=sortvec.end(); ++it){
			if(binEnd[it->second]>oldEnd){
				if(binStart[it->second]>oldEnd) nonOverlappingBinLenght+=(binEnd[it->second] - binStart[it->second]);
				else nonOverlappingBinLenght+=(binEnd[it->second] - oldEnd);
				oldEnd=binEnd[it->second];
			}
		}
		nonOverlappingBinLenghtComputed=false;
	}
	return nonOverlappingBinLenght;
}

bool TBasicMap::checkBins(TBasicMap* map){
	if(numBins!=map->numBins) return false;
	for(int i=0; i<numBins; ++i){
		if(binStart[i]!=map->binStart[i] || binEnd[i]!=map->binEnd[i]) return false;
	}
	return true;
}


