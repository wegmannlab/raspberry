/*
 * TMap.h
 *
 *  Created on: Sep 2, 2010
 *      Author: wegmannd
 */

#ifndef TBASICMAP_H_
#define TBASICMAP_H_

#include <stdio.h>
#include <math.h>
#include <vector>
#include <iostream>
#include "TIndividual.h"

class TBasicMap{
public:
	double numInd;
	double* bins;
	double** ancestryPerPop;
	int numRefPop;
	bool ancestryStorageInitialized;
	long* binStart;
	long* binEnd;
	long step;
	std::string filename;
	bool recombination, ancestries;
	long start, end;
	long numBins;
	long windowsize; //set to -1 if not applicable
	double nonOverlappingBinLenght;
	bool nonOverlappingBinLenghtComputed;
	bool binsStandardized, ancestriesStandardized;

	TBasicMap();
	TBasicMap(long Windowsize, long Start, long End, double Increment);
	TBasicMap(long Windowsize, long Start, long End, long Step);
	TBasicMap(long Windowsize, long Start, long End);
	void initialize();
	void initializeAncestryStorage(int numRefPop);
	virtual ~TBasicMap();

	TBasicMap(TBasicMap* map);
	TBasicMap(TBasicMap* map, long start, long end);
	TBasicMap(std::string filename, std::string format, long start, long end);
	TBasicMap(std::string filename, std::string format);
	void readHapMapFile(std::string hapmapfile);
	void readDeCodeFile(std::string decodeFile);
	void readDefinitionFile(std::string definitionFile, long start, long end);

	//some access functions
	bool recombinationComputed(){ return recombination; };
	bool ancestriesComputed(){ return ancestries; };
	long getNumBins(){ return numBins; };
	double* getPointerToRecombination();
	std::string getFilename(){ return filename; };
	long getStartOfBin(long bin){ if(bin<0) bin=0; if(bin>=numBins) bin=numBins-1; return binStart[bin]; };
	long getEndOfBin(long bin){ if(bin<0) bin=0; if(bin>=numBins) bin=numBins-1;  return binEnd[bin]; };

	void addProbabilities(TInd* oneInd);
	void addProbabilities(TBasicMap & map);
	void addAncestries(TInd* oneInd);
	void addProbabilitiesAndAncestries(TInd* oneInd);
	void addMapWithSameBins(TBasicMap & map);
	virtual void empty();
	void emptyAncestries();
	virtual void scale();
	double getTotalRate();
	double getAverageRatePerBasepair();
	double getTotalAncestry(int refPop);
	double getAverageAncestry(int refPop);
	double getTotalBinLength();
	double getNonOverlappingBinLength();
	virtual bool checkBins(TBasicMap* map);
	void fillAncestries(double* array, int refPop){
		for(int i=0; i<numBins; ++i) array[i]=ancestryPerPop[refPop][i];
	};
};
#endif /* TBASICMAP_H_ */
