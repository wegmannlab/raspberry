/*
 * correlater.h
 *
 *  Created on: Jan 5, 2011
 *      Author: wegmannd
 */

#ifndef CORRELATER_H_
#define CORRELATER_H_
#include <vector>
#include <map>
#include <algorithm>
#include <utility>

//TODO: use TEMPLATE instead of double

class TCorrelatorDataSet{
public:
	long length;
	double* data;
	double* rank;
	double* data_transformed;
	double sum, sumOfSquares, rankSum, rankSumOfSquares, mean, variance;
	bool sumComputed, sumOfSquaresComputed, meanComputed, varianceComputed;
	bool rankSumComputed, rankSumOfSquaresComputed, rankInitialized, rankAvailable;

	TCorrelatorDataSet(long Length, double* Data, double* Rank=NULL){
		rankInitialized=false;
		reset(Length, Data, Rank);
	};
	virtual ~TCorrelatorDataSet(){
		if(rankInitialized) delete[] rank;
	};
	void reset(long Length, double* Data, double* Rank=NULL){
		length=Length;
		data=Data;
		if(rankInitialized) delete[] rank;
		if(Rank==NULL) rankAvailable=false;
		else {
			rank=Rank;
			rankAvailable=true;
		}
		sumComputed=false;
		sumOfSquaresComputed=false;
		rankSumComputed=false;
		rankSumOfSquaresComputed=false;
		sum=0.0;
		sumOfSquares=0.0;
		rankSum=0.0;
		rankSumOfSquares=0.0;
		meanComputed=false;
		varianceComputed=false;
	};
	long getLength(){ return length; };
	void computeSum(){
		for(long i=0; i<length; ++i) sum+=data[i];
		sumComputed=true;
	};
	double getSum(){
		if(!sumComputed) computeSum();
		return sum;
	};
	void computeSumOfSquares(){
		for(long i=0; i<length; ++i) sumOfSquares+=data[i]*data[i];
		sumOfSquaresComputed=true;
	};
	double getSumOfSquares(){
		if(!sumOfSquaresComputed) computeSumOfSquares();
		return sumOfSquares;
	};
	void fillRanks(){
		if(rankInitialized) delete[] rank;
		std::vector< std::pair<double, long> > sortvec;
		long index=0;
		for(long i=0; i<length; ++i, ++index){
			sortvec.push_back(std::pair<double, long>(data[i], index));
		}
		sort(sortvec.begin(), sortvec.end()); //sorts by first element of pair
		//prepare rank vector
		rank=new double[length];

		//now go through sorted vector and add ranks
		double r=1.0;
		double oldVal=sortvec[0].first;
		std::vector<long> oldIndices;
		std::vector<long>::iterator oldIndicesIt;
		oldIndices.push_back(sortvec[0].second);
		for(std::vector< std::pair<double, long> >::size_type i=1;i<sortvec.size(); ++i){
			if(sortvec[i].first>oldVal){
				//save ranks
				r=r/(double) oldIndices.size();
				for(oldIndicesIt=oldIndices.begin(); oldIndicesIt!=oldIndices.end(); ++oldIndicesIt){
					rank[(*oldIndicesIt)]=r;
				}
				oldIndices.clear();
				r=0;
				oldVal=sortvec[i].first;
			}
			//save current
			oldIndices.push_back(sortvec[i].second);
			r+=(i+1);
		}
		//save last
		r=r/(double) oldIndices.size();
		for(oldIndicesIt=oldIndices.begin(); oldIndicesIt!=oldIndices.end(); ++oldIndicesIt){
			rank[(*oldIndicesIt)]=r;
		}
		oldIndices.clear();
		sortvec.clear();
		rankInitialized=true;
	};
	void computeRankSum(){
		if(!rankInitialized) fillRanks();
		for(long i=0; i<length; ++i) rankSum+=rank[i];
		rankSumComputed=true;
	};
	double getRankSum(){
		if(!rankSumComputed) computeRankSum();
		return rankSum;
	};
	void computeRankSumsOfSquares(){
		if(!rankInitialized) fillRanks();
		for(long i=0; i<length; ++i) rankSumOfSquares+=rank[i]*rank[i];
		rankSumOfSquaresComputed=true;
	};
	double getRankSumsOfSquares(){
		if(!rankSumOfSquaresComputed) computeRankSumsOfSquares();
		return rankSumOfSquares;
	};
	double* getPointerToData(){ return data; };
	double* getPointerToRank(){
		if(!rankInitialized) fillRanks();
		return rank;
	};
	double getProductSumWith(double* other){
		double prod=0.0;
		for(long i=0; i<length; ++i){
			prod+=data[i]*other[i];
		}
		return prod;
	};
	double getProductRankSumWith(double* otherRank){
		if(!rankInitialized) fillRanks();
		double prod=0.0;
		for(long i=0; i<length; ++i) prod+=rank[i]*otherRank[i];
		return prod;
	};
	void computeMean(){
		mean=getSum()/length;
		meanComputed=true;
	};
	double getMean(){
		if(!meanComputed) computeMean();
		return mean;
	};
	void computeVariance(){
		if(!meanComputed) computeMean();
		variance=0;
		for(long i=0; i<length; ++i) variance+=(data[i]-mean)*(data[i]-mean);
		variance=variance/(length-1);
		varianceComputed=true;
	};
	double getVariance(){
		if(!varianceComputed) computeVariance();
		return variance;
	};
	double getCovariance(double* other, double otherMean){
		if(!meanComputed) computeMean();
		double cov=0;
		for(long i=0; i<length; ++i) cov+=(data[i]-mean)*(other[i]-otherMean);
		cov=cov/(length-1);
		return cov;
	};
	void fillSquaredMahalanobisDistance(double* other, double otherMean, double** covmatrix_inv, double* fill){
		double s1, s2;
		for(long i=0; i<length; ++i){
			//get diff to mean
			s1=(data[i]-mean);
			s2=(other[i]-otherMean);
			//write out matrix multiplication t(s1,s2) * cov_inv * (s1,s2) -> cov_inv[0][1] == cov_inv[1][0]!
			fill[i]=s1*s1*covmatrix_inv[0][0] + 2*s1*s2*covmatrix_inv[0][1] + s2*s2*covmatrix_inv[1][1];
		}
	};
	void fillLowestRank(double* otherD, double* other, double* fill){
		//default is to exclude those with larger number -> fliP!
		if(!rankInitialized) fillRanks();
		for(long i=0; i<length; ++i){
			fill[i]=length-std::min(rank[i], other[i]);
		}
	};
	void print(){
		for(long i=0; i<length; ++i){
			std::cout << data[i];
			if(rankInitialized) std::cout <<"\t" << rank[i];
			std::cout << std::endl;
		}
	};
	virtual void transformData(std::string transformation){
		throw "Correlator: Data set can not be transformed!";
	};
};
//--------------------------------------------------------------------------------------------------
class TCorrelatorDataCopy: public TCorrelatorDataSet{
	//While the base class does not copy the data but just used an external array,
	//the filtered class produces a new data array after filtering based on a criterion pass to the constructor.
	//Thus, this class is much slower than the base class
public:
	bool dataTransformed;

	TCorrelatorDataCopy(long Length, double* Data):TCorrelatorDataSet(Length, Data){
		dataTransformed=false;

		//generate a new Data object
		data=new double[Length];
		for(long i=0; i<Length; ++i){
			data[i]=Data[i];
		}
	};
	TCorrelatorDataCopy(long Length, double* Data, double* filteredOn, double filter, bool smaller=true):TCorrelatorDataSet(Length, Data){
		//find out how many pass the filter (a bit nasty, but otherwise I would have to recode too many things now)
		long index=0;
		if(smaller){
			for(long i=0; i<Length; ++i){
				if(filteredOn[i]<filter) ++index;
			}
		} else {
			for(long i=0; i<Length; ++i){
				if(filteredOn[i]>filter) ++index;
			}
		}

		dataTransformed=false;

		//generate a new Data object
		data=new double[index];
		index=0;
		if(smaller){
			for(long i=0; i<Length; ++i){
				if(filteredOn[i]<filter){
					data[index]=Data[i];
					++index;
				}
			}
		} else {
			for(long i=0; i<Length; ++i){
				if(filteredOn[i]<filter){
					data[index]=Data[i];
					++index;
				}
			}
		}
	};

	virtual ~TCorrelatorDataCopy(){
		delete[] data;
	};
	virtual void transformData(std::string transformation){
		if(dataTransformed) throw "Correlator: Data has already been transformed!";
		//There are only few specific transformations implemented
		//The currently implemented transformations do not change the ranks
		if(transformation=="sqrt"){
			for(long i=0; i<length; ++i){
				if(data[i]<0) throw "Correlator: Attempt to take sqrt of a negative number!";
				data[i]=sqrt(data[i]);
			}
		} else if(transformation=="log10"){
			for(long i=0; i<length; ++i){
				if(data[i]<=0) throw "Correlator: Attempt to take log10 of a number <=0!";
				data[i]=sqrt(data[i]);
			}
		} else if(transformation!="none") throw "Correlator: unknown transformation '"+transformation+"!";
		dataTransformed=true;
	};
};
//--------------------------------------------------------------------------------------------------
class TCorrelator{
private:
	bool data1Initialized, data2Initialized;
	TCorrelatorDataSet* data1;
	TCorrelatorDataSet* data2;
	double pearson, spearman;
	double* trimmingDistance;
	double* trimmingDistanceSorted;
	std::string trimmingType;
	bool pearsonComputed, spearmanComputed, trimmingDistanceComputed;

public:
	TCorrelator(){
		init();
	};
	TCorrelator(long Length, double* Data1, double* Data2){
		init();
		addDataset(Length, Data1);
		addDataset(Length, Data2);
		trimmingType="";
	};
	TCorrelator(long Length, double* Data1, double* Rank1, double* Data2, double* Rank2){
		init();
		addDataset(Length, Data1, Rank1);
		addDataset(Length, Data2, Rank2);
		trimmingType="";
	};
	TCorrelator(long Length1, double* Data1, double* Rank1, long Length2, double* Data2, double* Rank2){
		init();
		addDataset(Length1, Data1, Rank1);
		addDataset(Length2, Data2, Rank2);
		trimmingType="";
	};
	~TCorrelator(){
		reset();
	};
	void init(){
		data1Initialized=false;
		data2Initialized=false;
		pearsonComputed=false;
		spearmanComputed=false;
		trimmingDistanceComputed=false;
	};
	void reset(){
		if(data1Initialized) delete data1;
		if(data2Initialized) delete data2;
		if(trimmingDistanceComputed){
			delete[] trimmingDistance;
			delete[] trimmingDistanceSorted;
		}
		init();
	};
	void addDataset(long length, double* Data, std::string transformation){
		if(!data1Initialized){
			data1=new TCorrelatorDataCopy(length, Data);
			data1->transformData(transformation);
			data1Initialized=true;
		} else {
			if(!data2Initialized){
				data2=new TCorrelatorDataCopy(length, Data);
				data2->transformData(transformation);
				data2Initialized=true;
			} else throw "Correlator has already two data sets initialized!";
		}
	};
	void addDataset(long length, double* Data, double* Rank=NULL){
		if(!data1Initialized){
			data1=new TCorrelatorDataSet(length, Data, Rank);
			data1Initialized=true;
		} else {
			if(!data2Initialized){
				data2=new TCorrelatorDataSet(length, Data, Rank);
				data2Initialized=true;
			} else throw "Correlator has already two data sets initialized!";
		}
	};
	void setTrimming(std::string TrimmingType){
		if(trimmingType!=TrimmingType){
			if(trimmingDistanceComputed){
				delete[] trimmingDistance;
				delete[] trimmingDistanceSorted;
			}
			trimmingDistanceComputed=false;
		}
		trimmingType=TrimmingType;
	};
	void computeTrimmingDistance(){
		if(!trimmingDistanceComputed){
			//compute and sort trimming distance
			trimmingDistance=new double[data1->length];
			//whiche type?
			if(trimmingType=="mahalanobis") computeSquaredMahalanobisTrimmingDistance();
			else if(trimmingType=="lowest") computeLowestTrimmingDistance();
			else throw "Unknown trimming type '"+trimmingType+"'!";
			//sort
			trimmingDistanceSorted=new double[data1->length];
			for(long i=0; i<data1->length; ++i){
				trimmingDistanceSorted[i]=trimmingDistance[i];
			}
			std::sort(trimmingDistanceSorted, trimmingDistanceSorted+data1->length);
			trimmingDistanceComputed=true;
		}
	};
	void computeSquaredMahalanobisTrimmingDistance(){
		//prepare inverted covariance matrix
		double** covmatrix_inv=new double*[2];
		for(int i=0; i<2; ++i) covmatrix_inv[i]=new double[2];
		double cov=data1->getCovariance(data2->getPointerToData(), data2->getMean());
		double s=1/(data1->getVariance()*data2->getVariance() - cov*cov);
		covmatrix_inv[0][0]=s*data2->getVariance();
		covmatrix_inv[0][1]=-s*cov;
		covmatrix_inv[1][0]=-s*cov;
		covmatrix_inv[1][1]=s*data1->getVariance();

		//now compute mahalanobis distances
		data1->fillSquaredMahalanobisDistance(data2->getPointerToData(), data2->getMean(), covmatrix_inv, trimmingDistance);
		for(int i=0; i<2; ++i) delete[] covmatrix_inv[i];
		delete[] covmatrix_inv;
	};
	void computeLowestTrimmingDistance(){
		//this distance is essentially the smallest rank of each bin across data sets
		data1->fillLowestRank(data2->getPointerToData(), data2->getPointerToRank(), trimmingDistance);
	};
	double computeTrimmingFilter(double trimmFrac){
		//whiche type?
		if(trimmingType=="mahalanobis"){
			int pos=data1->length-trimmFrac*data1->length-1;
			return trimmingDistanceSorted[pos];
		} else if(trimmingType=="lowest"){
			return data1->length-trimmFrac*data1->length-1;
		} else throw "Unknown trimming type '"+trimmingType+"'!";
	};
	double getTrimmedPearsonCorrelation(double trimmFrac, std::string transformation){
		if(!data1Initialized || !data2Initialized) throw "Correlator needs two data sets to compute correlations!";
		if(data1->getLength() != data2->getLength()) throw "Correlator: Correlation can only be computed if data sets are of equal length!";

		TCorrelatorDataCopy* newData1;
		TCorrelatorDataCopy* newData2;
		if(trimmFrac>0.0){
			//compute trimming distance
			computeTrimmingDistance();
			double filter=computeTrimmingFilter(trimmFrac);
			//create new data sets by filtering on trimming distance
			newData1=new TCorrelatorDataCopy(data1->length, data1->getPointerToData(), trimmingDistance, filter);
			newData2=new TCorrelatorDataCopy(data2->length, data2->getPointerToData(), trimmingDistance, filter);
		} else {
			newData1=new TCorrelatorDataCopy(data1->length, data1->getPointerToData());
			newData2=new TCorrelatorDataCopy(data2->length, data2->getPointerToData());
		}
		newData1->transformData(transformation);
		newData2->transformData(transformation);

		//compute Pearson correlation
		pearson=newData1->getProductSumWith(newData2->getPointerToData());
		pearson=newData1->getLength()*pearson -newData1->getSum()*newData2->getSum();
		pearson=pearson/(
			sqrt(newData1->getLength()*newData1->getSumOfSquares()-newData1->getSum()*newData1->getSum())
		  * sqrt(newData2->getLength()*newData2->getSumOfSquares()-newData2->getSum()*newData2->getSum()) );

		delete newData1;
		delete newData2;
		return pearson;
	};
	double getPearsonCorrelation(double trimmFrac=0.0, std::string transformation="none"){
		if(!data1Initialized || !data2Initialized) throw "Correlator needs two data sets to compute correlations!";
		if(data1->getLength() != data2->getLength()) throw "Correlator: Correlation can only be computed if data sets are of equal length!";

		if(trimmFrac>0.0 || transformation!="none"){
			return getTrimmedPearsonCorrelation(trimmFrac, transformation);
		} else {
			//compute Pearson correlation
			if(!pearsonComputed){
				pearson=data1->getProductSumWith(data2->getPointerToData());
				pearson=data1->getLength()*pearson - data1->getSum()*data2->getSum();
				pearson=pearson/(
						sqrt(data1->getLength()*data1->getSumOfSquares()-data1->getSum()*data1->getSum())
					  * sqrt(data2->getLength()*data2->getSumOfSquares()-data2->getSum()*data2->getSum()) );
				pearsonComputed=true;
			}
			return pearson;
		}
	};

	double getTrimmedSpearmanCorrelation(double trimmFrac, std::string transformation="none"){
		if(!data1Initialized || !data2Initialized) throw "Correlator needs two data sets to compute correlations!";
		if(data1->getLength() != data2->getLength()) throw "Correlator: Correlation can only be computed if data sets are of equal length!";

		TCorrelatorDataCopy* newData1;
		TCorrelatorDataCopy* newData2;
		if(trimmFrac>0.0){
			//compute trimming distance
			computeTrimmingDistance();
			double filter=computeTrimmingFilter(trimmFrac);
			//create new data sets by filtering on trimming distance
			newData1=new TCorrelatorDataCopy(data1->length, data1->getPointerToData(), trimmingDistance, filter);
			newData2=new TCorrelatorDataCopy(data2->length, data2->getPointerToData(), trimmingDistance, filter);
		} else {
			newData1=new TCorrelatorDataCopy(data1->length, data1->getPointerToData());
			newData2=new TCorrelatorDataCopy(data2->length, data2->getPointerToData());
		}
		newData1->transformData(transformation);
		newData2->transformData(transformation);

		//compute Spearman correlation
		spearman=newData1->getProductRankSumWith(newData2->getPointerToRank());
		spearman=newData1->getLength()*spearman - newData1->getRankSum()*newData2->getRankSum();
		spearman=spearman/(
			sqrt(newData1->getLength()*newData1->getRankSumsOfSquares()-newData1->getRankSum()*newData1->getRankSum())
		  * sqrt(newData2->getLength()*newData2->getRankSumsOfSquares()-newData2->getRankSum()*newData2->getRankSum()) );

		delete newData1;
		delete newData2;
		return spearman;
	};
	double getSpearmanCorrelation(double trimmFrac=0.0, std::string transformation="none"){
		if(!data1Initialized || !data2Initialized) throw "Correlator needs two data sets to compute correlations!";
		if(data1->getLength() != data2->getLength()) throw "Correlator: Correlation can only be computed if data sets are of equal length!";

		if(trimmFrac>0.0 || transformation!="none"){
			return getTrimmedSpearmanCorrelation(trimmFrac, transformation);
		} else {
			//compute Spearman correlation
			if(!spearmanComputed){
				spearman=data1->getProductRankSumWith(data2->getPointerToRank());
				spearman=data1->getLength()*spearman - data1->getRankSum()*data2->getRankSum();
				spearman=spearman/(
						sqrt(data1->getLength()*data1->getRankSumsOfSquares()-data1->getRankSum()*data1->getRankSum())
					  * sqrt(data2->getLength()*data2->getRankSumsOfSquares()-data2->getRankSum()*data2->getRankSum()) );
				spearmanComputed=true;
			}
			return spearman;
		}
	};

	double getSpearmanPValue(){
		//first compute Fisher Transformation
		double x=0.5*log((1+spearman)/(1-spearman));
		//now get z score
		x=x*sqrt((double)(data1->getLength()-3)/1.06);
		//use a standard normal to get p-value
		x=getNormalcumulative(x);
		if(x>0.5) x=1-x;
		return(x);
	};

	double getNormalcumulative(const double x){
		const double b1 =  0.319381530;
		const double b2 = -0.356563782;
		const double b3 =  1.781477937;
		const double b4 = -1.821255978;
		const double b5 =  1.330274429;
		const double p  =  0.2316419;
		const double c  =  0.39894228;

		if(x >= 0.0) {
		    double t = 1.0 / ( 1.0 + p * x );
		    return (1.0 - c * exp( -x * x / 2.0 ) * t *
		    ( t *( t * ( t * ( t * b5 + b4 ) + b3 ) + b2 ) + b1 ));
		}
		else {
		    double t = 1.0 / ( 1.0 - p * x );
		    return ( c * exp( -x * x / 2.0 ) * t *
		    ( t *( t * ( t * ( t * b5 + b4 ) + b3 ) + b2 ) + b1 ));
		}
	};
};

#endif /* CORRELATER_H_ */
