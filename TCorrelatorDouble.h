/*
 * correlater.h
 *
 *  Created on: Jan 5, 2011
 *      Author: wegmannd
 */

#ifndef CORRELATERDOUBLE_H_
#define CORRELATERDOUBLE_H_
#include <vector>
#include <map>
#include <algorithm>
#include <utility>

class TCorrelatorDataSetDouble{
public:
	long length;
	double* data;
	double* rank;
	double sum, sumOfSquares, rankSum, rankSumOfSquares, mean, variance;
	bool sumComputed, sumOfSquaresComputed, rankSumComputed, rankSumOfSquaresComputed, rankInitialized, rankAvailable, meanComputed, varianceComputed;

	TCorrelatorDataSetDouble(){};
	TCorrelatorDataSetDouble(long Length, double* Data, double* Rank=NULL){
		rankInitialized=false;
		reset(Length, Data, Rank);
	};
	virtual ~TCorrelatorDataSetDouble(){
		if(rankInitialized) delete[] rank;
	};
	void reset(long Length, double* Data, double* Rank=NULL){
		length=Length;
		data=Data;
		if(rankInitialized) delete[] rank;
		if(Rank==NULL) rankAvailable=false;
		else {
			rank=Rank;
			rankAvailable=true;
		}
		sumComputed=false;
		sumOfSquaresComputed=false;
		rankSumComputed=false;
		rankSumOfSquaresComputed=false;
		sum=0.0;
		sumOfSquares=0.0;
		rankSum=0.0;
		rankSumOfSquares=0.0;
		meanComputed=false;
		varianceComputed=false;
	};
	long getLength(){ return length; };
	void computeSum(){
		for(long i=0; i<length; ++i) sum+=data[i];
		sumComputed=true;
	};
	double getSum(){
		if(!sumComputed) computeSum();
		return sum;
	};
	void computeSumOfSquares(){
		for(long i=0; i<length; ++i) sumOfSquares+=data[i]*data[i];
		sumOfSquaresComputed=true;
	};
	double getSumOfSquares(){
		if(!sumOfSquaresComputed) computeSumOfSquares();
		return sumOfSquares;
	};
	void fillRanks(){
		if(rankInitialized) delete[] rank;
		std::vector< std::pair<float, long> > sortvec;
		long index=0;
		for(long i=0; i<length; ++i, ++index){
			sortvec.push_back(std::pair<double, long>(data[i], index));
		}
		sort(sortvec.begin(), sortvec.end()); //sorts by first element of pair
		//prepare rank vector
		rank=new double[length];

		//now go through sorted vector and add ranks
		double r=1.0;
		double oldVal=sortvec[0].first;
		std::vector<long> oldIndices;
		std::vector<long>::iterator oldIndicesIt;
		oldIndices.push_back(sortvec[0].second);
		for(std::vector< std::pair<float, long> >::size_type i=1;i<sortvec.size(); ++i){
			if(sortvec[i].first>oldVal){
				//save ranks
				r=r/(double) oldIndices.size();
				for(oldIndicesIt=oldIndices.begin(); oldIndicesIt!=oldIndices.end(); ++oldIndicesIt){
					rank[(*oldIndicesIt)]=r;
				}
				oldIndices.clear();
				r=0;
				oldVal=sortvec[i].first;
			}
			//save current
			oldIndices.push_back(sortvec[i].second);
			r+=(i+1);
		}
		//save last
		r=r/(double) oldIndices.size();
		for(oldIndicesIt=oldIndices.begin(); oldIndicesIt!=oldIndices.end(); ++oldIndicesIt){
			rank[(*oldIndicesIt)]=r;
		}
		oldIndices.clear();
		sortvec.clear();
		rankInitialized=true;
	};
	void computeRankSum(){
		if(!rankInitialized) fillRanks();
		for(long i=0; i<length; ++i) rankSum+=rank[i];
		rankSumComputed=true;
	};
	double getRankSum(){
		if(!rankSumComputed) computeRankSum();
		return rankSum;
	};
	void computeRankSumsOfSquares(){
		if(!rankInitialized) fillRanks();
		for(long i=0; i<length; ++i) rankSumOfSquares+=rank[i]*rank[i];
		rankSumOfSquaresComputed=true;
	};
	double getRankSumsOfSquares(){
		if(!rankSumOfSquaresComputed) computeRankSumsOfSquares();
		return rankSumOfSquares;
	};
	double* getPointerToData(){ return data; };
	double* getPointerToRank(){
		if(!rankInitialized) fillRanks();
		return rank;
	};
	double getProductSumWith(double* other){
		double prod=0.0;
		for(long i=0; i<length; ++i){
			prod+=data[i]*other[i];
		}
		return prod;
	};
	double getProductRankSumWith(double* otherRank){
		if(!rankInitialized) fillRanks();
		double prod=0.0;
		for(long i=0; i<length; ++i) prod+=rank[i]*otherRank[i];
		return prod;
	};
	void computeMean(){
		mean=getSum()/length;
		meanComputed=true;
	};
	double getMean(){
		if(!meanComputed) computeMean();
		return mean;
	};
	void computeVariance(){
		if(!meanComputed) computeMean();
		variance=0;
		for(long i=0; i<length; ++i) variance+=(data[i]-mean)*(data[i]-mean);
		variance=variance/(length-1);
		varianceComputed=true;
	};
	double getVariance(){
		if(!varianceComputed) computeVariance();
		return variance;
	};
	double getCovariance(double* other, double otherMean){
		if(!meanComputed) computeMean();
		double cov=0;
		for(long i=0; i<length; ++i) cov+=(data[i]-mean)*(other[i]-otherMean);
		cov=cov/(length-1);
		return cov;
	};
	void fillSquaredMahalanobisDistance(double* other, double otherMean, double** covmatrix_inv, double* fill){
		double s1, s2;
		for(long i=0; i<length; ++i){
			//get diff to mean
			s1=(data[i]-mean);
			s2=(other[i]-otherMean);
			//write out matrix multiplication t(s1,s2) * cov_inv * (s1,s2) -> cov_inv[0][1] == cov_inv[1][0]!
			fill[i]=s1*s1*covmatrix_inv[0][0] + 2*s1*s2*covmatrix_inv[0][1] + s2*s2*covmatrix_inv[1][1];
		}
	};
};
//--------------------------------------------------------------------------------------------------
class TCorrelatorDataSetDoubleFiltered: public TCorrelatorDataSetDouble{
	//While teh base class does not copy the data but just used an external array,
	//the filtered class produces a new data array after filtering based on a criterion pass to the contsructer.
	//Thus, this class is much slower than the base class
public:
	TCorrelatorDataSetDoubleFiltered(long Length, double* Data, double* filteredOn, double filter, bool smaller=true){
		//find out how many pass the filter (a bit nasty, but otherwise I would have to recode too many things now)
		long index=0;
		if(smaller){
			for(long i=0; i<Length; ++i){
				if(filteredOn[i]<filter) ++index;
			}
		} else {
			for(long i=0; i<Length; ++i){
				if(filteredOn[i]>filter) ++index;
			}
		}

		//call parent reset function
		rankInitialized=false;
		TCorrelatorDataSetDouble::reset(index, NULL);

		//generate a new Data object
		data=new double[index];
		if(smaller){
			for(long i=0; i<Length; ++i){
				if(filteredOn[i]<filter){
					data[index]=Data[i];
					++index;
				}
			}
		} else {
			for(long i=0; i<Length; ++i){
				if(filteredOn[i]<filter){
					data[index]=Data[i];
					++index;
				}
			}
		}
	};

	~TCorrelatorDataSetDoubleFiltered(){
		delete data;
	};
};

//--------------------------------------------------------------------------------------------------
class TCorrelatorDouble{
private:
	bool data1Initialized, data2Initialized;
	TCorrelatorDataSetDouble* data1;
	TCorrelatorDataSetDouble* data2;
	double pearson, spearman;
	double* squaredMahalanobis;
	double* squaredMahalanobisSorted;
	bool pearsonComputed, spearmanComputed, mahalanobisComputed, mahalanobisSorted;

public:
	TCorrelatorDouble(){
		init();
	};
	TCorrelatorDouble(long Length, double* Data1, double* Data2){
		init();
		addDataset(Length, Data1);
		addDataset(Length, Data2);
	};
	TCorrelatorDouble(long Length, double* Data1, double* Rank1, double* Data2, double* Rank2){
		init();
		addDataset(Length, Data1, Rank1);
		addDataset(Length, Data2, Rank2);
	};
	TCorrelatorDouble(long Length1, double* Data1, double* Rank1, long Length2, double* Data2, double* Rank2){
		init();
		addDataset(Length1, Data1, Rank1);
		addDataset(Length2, Data2, Rank2);
	};
	~TCorrelatorDouble(){
		reset();
	};
	void init(){
		data1Initialized=false;
		data2Initialized=false;
		pearsonComputed=false;
		spearmanComputed=false;
		mahalanobisComputed=false;
		mahalanobisSorted=false;
	};
	void reset(){
		if(data1Initialized) delete data1;
		if(data2Initialized) delete data2;
		if(mahalanobisComputed) delete squaredMahalanobis;
		if(mahalanobisSorted) delete squaredMahalanobisSorted;
		init();
	};
	void addDataset(long length, double* Data, double* Rank=NULL){
		if(!data1Initialized){
			data1=new TCorrelatorDataSetDouble(length, Data, Rank);
			data1Initialized=true;
		} else {
			if(!data2Initialized){
				data2=new TCorrelatorDataSetDouble(length, Data, Rank);
				data2Initialized=true;
			} else throw "Correlator has already two data sets initialized!";
		}
	};
	void computeSquaredMahalanobis(){
		squaredMahalanobis=new double[data1->length];
		//prepare inverted covariance matrix
		double** covmatrix_inv=new double*[2];
		for(int i=0; i<2; ++i) covmatrix_inv[i]=new double[2];
		double cov=data1->getCovariance(data2->getPointerToData(), data2->getMean());
		double s=1/(data1->getVariance()*data2->getVariance() - cov*cov);
		covmatrix_inv[0][0]=s*data2->getVariance();
		covmatrix_inv[0][1]=-s*cov;
		covmatrix_inv[1][0]=-s*cov;
		covmatrix_inv[1][1]=s*data1->getVariance();
		//now compute mahalanobis distances
		data1->fillSquaredMahalanobisDistance(data2->getPointerToData(), data2->getMean(), covmatrix_inv, squaredMahalanobis);
		mahalanobisComputed=true;
		if(mahalanobisSorted){
			delete squaredMahalanobisSorted;
			mahalanobisSorted=false;
		}
	};
	void sortSquaredMahalanobis(){
		if(!mahalanobisSorted) squaredMahalanobisSorted=new double[data1->length];
		for(long i=0; i<data1->length; ++i){
			squaredMahalanobisSorted[i]=squaredMahalanobis[i];
		}
		std::sort(squaredMahalanobisSorted, squaredMahalanobisSorted+data1->length);
		mahalanobisSorted=true;
	};
	double getTrimmedPearsonCorrelation(double trimmFrac){
		if(!data1Initialized || !data2Initialized) throw "Correlator needs two data sets to compute correlations!";
		if(data1->getLength() != data2->getLength()) throw "Correlator: Correlation can only be computed if data sets are of equal length!";
		//compute squared mahalanobis distance
		if(!mahalanobisComputed) computeSquaredMahalanobis();
		//create new data sets by filtering on mahalanobis distance
		int pos=data1->length-trimmFrac*data1->length-1;
		double filter=squaredMahalanobisSorted[pos];
		TCorrelatorDataSetDoubleFiltered* newData1=new TCorrelatorDataSetDoubleFiltered(data1->length, data1->getPointerToData(), squaredMahalanobisSorted, filter);
		TCorrelatorDataSetDoubleFiltered* newData2=new TCorrelatorDataSetDoubleFiltered(data2->length, data2->getPointerToData(), squaredMahalanobisSorted, filter);

		//compute Pearson correlation
		pearson=newData1->getProductSumWith(newData2->getPointerToData());
		pearson=newData1->getLength()*pearson -newData1->getSum()*newData2->getSum();
		pearson=pearson/(
			sqrt(newData1->getLength()*newData1->getSumOfSquares()-newData1->getSum()*newData1->getSum())
		  * sqrt(newData2->getLength()*newData2->getSumOfSquares()-newData2->getSum()*newData2->getSum()) );
		return pearson;
	};
	double getPearsonCorrelation(double trimmFrac=0.0){
		if(!data1Initialized || !data2Initialized) throw "Correlator needs two data sets to compute correlations!";
		if(data1->getLength() != data2->getLength()) throw "Correlator: Correlation can only be computed if data sets are of equal length!";

		if(trimmFrac>0.0){
			return getTrimmedPearsonCorrelation(trimmFrac);
		} else {
			//compute Pearson correlation
			if(!pearsonComputed){
				pearson=data1->getProductSumWith(data2->getPointerToData());
				pearson=data1->getLength()*pearson - data1->getSum()*data2->getSum();
				pearson=pearson/(
						sqrt(data1->getLength()*data1->getSumOfSquares()-data1->getSum()*data1->getSum())
					  * sqrt(data2->getLength()*data2->getSumOfSquares()-data2->getSum()*data2->getSum()) );
				pearsonComputed=true;
			}
			return pearson;
		}
	};
	double getTrimmedSpearmanCorrelation(double trimmFrac){
		if(!data1Initialized || !data2Initialized) throw "Correlator needs two data sets to compute correlations!";
		if(data1->getLength() != data2->getLength()) throw "Correlator: Correlation can only be computed if data sets are of equal length!";
		//compute squared mahalanobis distance
		if(!mahalanobisComputed) computeSquaredMahalanobis();
		//create new data sets by filtering on mahalanobis distance
		int pos=data1->length-trimmFrac*data1->length-1;
		double filter=squaredMahalanobisSorted[pos];
		TCorrelatorDataSetDoubleFiltered* newData1=new TCorrelatorDataSetDoubleFiltered(data1->length, data1->getPointerToData(), squaredMahalanobisSorted, filter);
		TCorrelatorDataSetDoubleFiltered* newData2=new TCorrelatorDataSetDoubleFiltered(data2->length, data2->getPointerToData(), squaredMahalanobisSorted, filter);

		//compute Spearman correlation
		spearman=newData1->getProductRankSumWith(newData2->getPointerToRank());
		spearman=newData1->getLength()*spearman - newData1->getRankSum()*newData2->getRankSum();
		spearman=spearman/(
			sqrt(newData1->getLength()*newData1->getRankSumsOfSquares()-newData1->getRankSum()*newData1->getRankSum())
		  * sqrt(newData2->getLength()*newData2->getRankSumsOfSquares()-newData2->getRankSum()*newData2->getRankSum()) );
		return spearman;
	};
	double getSpearmanCorrelation(double trimmFrac=0.0){
		if(!data1Initialized || !data2Initialized) throw "Correlator needs two data sets to compute correlations!";
		if(data1->getLength() != data2->getLength()) throw "Correlator: Correlation can only be computed if data sets are of equal length!";

		if(trimmFrac>0.0){
			return getTrimmedSpearmanCorrelation(trimmFrac);
		} else {
			//compute Spearman correlation
			if(!spearmanComputed){
				spearman=data1->getProductRankSumWith(data2->getPointerToRank());
				spearman=data1->getLength()*spearman - data1->getRankSum()*data2->getRankSum();
				spearman=spearman/(
						sqrt(data1->getLength()*data1->getRankSumsOfSquares()-data1->getRankSum()*data1->getRankSum())
					  * sqrt(data2->getLength()*data2->getRankSumsOfSquares()-data2->getRankSum()*data2->getRankSum()) );
				spearmanComputed=true;
			}
			return spearman;
		}
	};

	double getNormalcumulative(const double x){
		const double b1 =  0.319381530;
		const double b2 = -0.356563782;
		const double b3 =  1.781477937;
		const double b4 = -1.821255978;
		const double b5 =  1.330274429;
		const double p  =  0.2316419;
		const double c  =  0.39894228;

		if(x >= 0.0) {
		    double t = 1.0 / ( 1.0 + p * x );
		    return (1.0 - c * exp( -x * x / 2.0 ) * t *
		    ( t *( t * ( t * ( t * b5 + b4 ) + b3 ) + b2 ) + b1 ));
		}
		else {
		    double t = 1.0 / ( 1.0 - p * x );
		    return ( c * exp( -x * x / 2.0 ) * t *
		    ( t *( t * ( t * ( t * b5 + b4 ) + b3 ) + b2 ) + b1 ));
		}
	};
	double getSpearmanPValue(){
		//first compute Fisher Transformation
		double x=0.5*log((1+spearman)/(1-spearman));
		//now get z score
		x=x*sqrt((double)(data1->getLength()-3)/1.06);
		//use a standard normal to get p-value
		x=getNormalcumulative(x);
		if(x>0.5) x=1-x;
		return(x);
	};
};

#endif
