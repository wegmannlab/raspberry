/*
 * TExclude.h
 *
 *  Created on: Dec 28, 2010
 *      Author: wegmannd
 */

#ifndef TEXCLUDE_H_
#define TEXCLUDE_H_

#include "stringFunctions.h"
#include <vector>
#include <map>

class TExclude{
private:
	std::string filename;
	std::map<float,float> ranges;
	std::map<float,float>::iterator rIt;

public:
	TExclude(std::string Filename){
		filename=Filename;
		//read file
		std::ifstream file(filename.c_str());
		if(!file) throw "Failed to open file '" + filename + "'!";

		//read first line
		std::string buf;
		std::vector<double> val;
		long line=1;
		while(file.good() && !file.eof()){
			getline(file, buf);
			if(!buf.empty()){ //empty lines are skipped
				fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
				if(val.size()<2) "Too few values in file '" + filename + " on line " + toString(line) + "!";
				ranges.insert(std::pair<float,float>(val[0], val[1]));
				val.clear();
			}
			++line;
		}
		file.close();
	};

	TExclude(const long start, const long stop){
		filename="";
		addInterval(start, stop);
	};

	void addInterval(const long start, const long stop){
		if(stop>start) ranges.insert(std::pair<float, float>(start, stop));
		else ranges.insert(std::pair<float, float>(stop, start));
	};

	void print(){
		std::cout << "Ranges to be excluded:" << std::endl;
		for(rIt=ranges.begin(); rIt!=ranges.end(); ++rIt){
			std::cout << "- from " << rIt->first << " to " << rIt->second << std::endl;
		}
	};

	bool include(const float & val){
		for(rIt=ranges.begin(); rIt!=ranges.end(); ++rIt){
			if(val < rIt->first) return true;
			else {
				if(val <= rIt->second) return false;
			}
		}
		return true;
	};

	bool exclude(const float & val){
		return !include(val);
	};

	int size(){
		//return number of intervals
		return ranges.size();
	};

	float includedSize(long start, long end){
		float size=end-start;
		for(rIt=ranges.begin(); rIt!=ranges.end(); ++rIt){
			if(start <= rIt->first){
				if(end > rIt->first){
					if(end > rIt->second) size-=rIt->second-rIt->first;
					else size-=end-rIt->first;
				} else return size;
			} else {
				if(start < rIt->second){
					if(end > rIt->second) size-=rIt->second-start;
					else return 0.0;
				}
			}
		}
		return size;
	};
};


#endif /* TEXCLUDE_H_ */
