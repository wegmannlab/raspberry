/*
 * TIndividual.cpp
 *
 *  Created on: Sep 2, 2010
 *      Author: wegmannd
 */

#include "TIndividual.h"

TInd::TInd(std::string Filename, bool readSnpNames){
	filename = Filename;
	ok = readFile(readSnpNames);
	snpNamesRead = readSnpNames;
	hasSwitchProbabilities = true;
}

bool TInd::readFile(bool readSnpNames){
	std::ifstream file;
	file.open(filename.c_str());
	if(!file){
		std::cerr << "WARNING: file '" + filename + " could not be read! Skipping this file..." << std::endl;
		return false;
	}
	std::string buf;
	long line=1;

	//read header
	getline(file, buf);
	trimString(buf);
	std::vector<std::string> val;
	fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
	int numCol = val.size();

	if(numCol < 9){
		std::cerr << "WARNING: Missing columns in header line of file '" + filename + "! Skipping this file..." << std::endl;
		return false;
	}

	//check if file contains switch probabilities or not (generated with option skipSwitchPointDetection).
	if(val[6].find("probAncestry") != std::string::npos){
		//does not contain switch probs: 7th column are already ancestries
		hasSwitchProbabilities = false;
		numColumnsBeforeAncestries = 6;
	} else if(val[6].find("switchProb") != std::string::npos){
		//does contain switch probs
		hasSwitchProbabilities = true;
		numColumnsBeforeAncestries = 8;
	} else {
		std::cerr << "WARNING: wrong columns in header line of file '" + filename + "! Skipping this file..." << std::endl;
		return false;
	}

	//learn number of reference populations from num of ancestry columns (#columns = n*(n+1)/2)
	numAncestries = numCol - numColumnsBeforeAncestries;
	numRefPop = sqrt(1.0 + 8.0 * numAncestries)/2.0;

	//fill weights per pop for each ancestry
	ancestryWeightPerPop=new double*[numRefPop];
	for(int p=0; p<numRefPop; ++p){
		ancestryWeightPerPop[p] = new double[numAncestries];
		for(int i=0; i<numAncestries; ++i) ancestryWeightPerPop[p][i]=0;
	}
	int index=0;
	for(int p=0; p<numRefPop; ++p){
		for(int q=p; q<numRefPop; ++q){
			ancestryWeightPerPop[p][index]+=1.0;
			ancestryWeightPerPop[q][index]+=1.0;
			++index;
		}
	}

	//go through file
	while(file.good() && !file.eof()){
		getline(file, buf);
		trimString(buf);
		if(!buf.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
			if(val.size()!=(std::vector<std::string>::size_type) numCol){
				std::cerr << "WARNING: Unexpected number of values in file '" + filename + " on line " + toString(line) + "! Skipping this file..." << std::endl;
				return false;
			}
			//SNP name and positio
			if(readSnpNames){
				snpNames.push_back(val[0]);
			}
			indPositions.push_back(stringToLong(val[1]));

			//switch probs
			if(hasSwitchProbabilities){
				probChr1.push_back(stringToDouble(val[6]));
				probChr2.push_back(stringToDouble(val[7]));
			}

			//read ancestries
			probAnc.push_back(new double[numAncestries]);
			for(int i=0; i<numAncestries; ++i){
				probAnc.back()[i] = stringToDouble(val[numColumnsBeforeAncestries + i]);
			}
		}
		++line;
	}
	file.close();

	return true;
}

double TInd::getTotalRate(long start, long end){
	std::vector<long>::iterator posIt=indPositions.begin();
	std::vector<double>::iterator prob1It=probChr1.begin();
	std::vector<double>::iterator prob2It=probChr2.begin();

	double sum=0;
	long oldPos=start;
	while((*posIt)<=start){
		oldPos=(*posIt);
		++posIt; ++prob1It; ++prob2It;
	}
	//first may be partial
	if(start>oldPos){
		sum+=((*prob1It)+(*prob2It))*((*posIt)-start)/((*posIt)-oldPos);
		oldPos=(*posIt);
		++posIt; ++prob1It; ++prob2It;
	}

	while(posIt!=indPositions.end() && (*posIt)<end){
		sum+=(*prob1It)+(*prob2It);
		oldPos=(*posIt);
		++posIt; ++prob1It; ++prob2It;
	}

	if(posIt!=indPositions.end()){
		//last may be partial
		sum+=((*prob1It)+(*prob2It))*(end-oldPos)/((*posIt)-oldPos);
	}
	return sum;
}

double TInd::getAncestry(double* ancestries, int pop){
	//combine across all possible ancestries...
	double sum=0;
	for(int i=0; i<numAncestries; ++i){
		sum+=ancestryWeightPerPop[pop][i]*ancestries[i];
	}
	return sum;
}

bool evalWindows(TWindow* a, TWindow* b){
	return (a->prob > b->prob);
}

void TInd::callAndWriteSwitchPoints(long start, long end, int windowSize, double cutOff, std::ofstream* out){
	//this function calls individual events as windows of highest probability
	//the algorithm searches non-overlappign windows with a cumulative probability > cutoff
	//first, for all possible windows the cumulative probabilities are computed.
	//The, the windows are sorted and teh best window is taken
	//all windows overlapping with this one are discarded
	//The remaining windows are sorted again etc.
	//Possible windows start or end a either a snp position of the start and end

	//generate all possible windows
	std::map<long, TWindow*> windows;
	windows.insert(std::pair<long, TWindow*>(start, new TWindow(start, start+windowSize)));
	windows.insert(std::pair<long, TWindow*>(end-windowSize, new TWindow(end-windowSize, end)));
	std::vector<long>::iterator posIt=indPositions.begin();
	for(;posIt!=indPositions.end(); ++posIt){
		if((*posIt)+windowSize<end && windows.find(*posIt)==windows.end()) windows.insert(std::pair<long, TWindow*>((*posIt), new TWindow((*posIt), (*posIt)+windowSize)));
		if((*posIt)-windowSize>start  && windows.find((*posIt)-windowSize)==windows.end()) windows.insert(std::pair<long, TWindow*>((*posIt)-windowSize, new TWindow((*posIt)-windowSize, (*posIt))));
	}

	//compute probabilities. Use the fact that teh windows are ordered within the map by starting position!
	std::map<long, TWindow*>::iterator wIt=windows.begin();
	posIt=indPositions.begin();
	std::vector<long>::iterator posIt2;
	std::vector<double>::iterator prob1It=probChr1.begin();
	std::vector<double>::iterator prob2It=probChr2.begin();
	std::vector<double>::iterator prob1It2;
	std::vector<double>::iterator prob2It2;
	long oldPos;
	while(wIt!=windows.end()){
		//find last SNP before start or SNP at start
		//posIt is < that start because the map is ordered!
		while((*posIt)<wIt->first){
			++posIt; ++prob1It; ++prob2It;
			if(posIt==indPositions.end()) break;
		}
		if(posIt==indPositions.end()) break;
		if((*posIt)>wIt->first){ --posIt; --prob1It; --prob2It; }
		//now go thorugh all intervals until the end of the window
		//Note that the probabilities are always from the last SNP to this one
		posIt2=posIt;
		prob1It2=prob1It;
		prob2It2=prob2It;
		while((*posIt2)<wIt->second->stop){
			oldPos=(*posIt2);
			++posIt2; ++prob1It2; ++prob2It2;
			if(posIt2==indPositions.end()) break;
			//four cases:
			if(oldPos<wIt->second->start){
				if((*posIt2)>wIt->second->stop){
					//window is smaller than distance between SNPs
					wIt->second->addprob((wIt->second->stop-wIt->second->start)/((*posIt2)-oldPos)*((*prob1It2)+(*prob2It2)));
				} else {
					//window is only part of distance between SNPs
					wIt->second->addprob(((*posIt2)-wIt->second->start)/((*posIt2)-oldPos)*((*prob1It2)+(*prob2It2)));
				}
			} else {
				if((*posIt2)>wIt->second->stop){
					//window is only part of distance between SNPs
					wIt->second->addprob((wIt->second->stop-oldPos)/((*posIt2)-oldPos)*((*prob1It2)+(*prob2It2)));
				} else {
					//complete distance between SNPs is within window
					wIt->second->addprob((*prob1It2)+(*prob2It2));
				}
			}
		}
		++wIt;
	}

	//sort windows by probability
	std::vector<TWindow*> sortedWindows;
	for(wIt=windows.begin();wIt!=windows.end();++wIt){
		sortedWindows.push_back(wIt->second);
	}
	sort(sortedWindows.begin(), sortedWindows.end(), evalWindows);

	std::vector<TWindow*>::iterator swIt=sortedWindows.begin();
	std::vector<TWindow*> toWrite;
	std::vector<TWindow*>::iterator wwIt;
	bool overlap;
	while(swIt!=sortedWindows.end() && (*swIt)->prob>cutOff){
		//check if windows overlaps with an already chosen window
		overlap=false;
		for(wwIt=toWrite.begin(); wwIt!=toWrite.end(); ++wwIt){
			if(((*wwIt)->start > (*swIt)->start && (*wwIt)->start < (*swIt)->stop) || ((*wwIt)->stop > (*swIt)->start && (*wwIt)->stop < (*swIt)->stop)){
				overlap=true;
				break;
			}
		}
		if(!overlap) toWrite.push_back((*swIt));
		++swIt;
	}

	for(swIt=toWrite.begin(); swIt!=toWrite.end(); ++swIt){
		*out << filename << "\t" << (*swIt)->start << "\t" << (*swIt)->stop << "\t" << (*swIt)->prob << std::endl;
	}

}

double TInd::getAverageAncestry(long start, long end, double & weightedSum, double & sumOfWeights){
	unsigned long i=0;
	while(indPositions[i]<start) ++i;

	//first extended to start
	double size=(indPositions[i]-start)+(indPositions[i+1]-indPositions[i])/2;
	weightedSum=(probHomo2[i]+0.5*probHet[i])*size;
	sumOfWeights=size;
	++i;

	while(i<(indPositions.size()-1) && indPositions[i+1]<end){
		size=(indPositions[i+1]-indPositions[i-1])/2;
		sumOfWeights+=size;
		weightedSum+=(probHomo2[i]+0.5*probHet[i])*size;
		++i;
	}

	//last
	size=(end-indPositions[i])+(indPositions[i]-indPositions[i-1])/2;
	sumOfWeights+=size;
	weightedSum+=(probHomo2[i]+0.5*probHet[i])*size;

	return weightedSum/sumOfWeights;
}

double TInd::getAverageAncestry(long start, long end, TExclude* exclude, double & weightedSum, double & sumOfWeights){
	unsigned long i=0;
	while(indPositions[i]<start) ++i;

	//first extended to start
	double size=exclude->includedSize(start, (indPositions[i]+indPositions[i+1])/2);
	weightedSum=(probHomo2[i]+0.5*probHet[i])*size;
	sumOfWeights=size;
	++i;

	while(i<(indPositions.size()-1) && indPositions[i+1]<end){
		size=exclude->includedSize((indPositions[i-1]+indPositions[i])/2, (indPositions[i]+indPositions[i+1])/2);
		sumOfWeights+=size;
		weightedSum+=(probHomo2[i]+0.5*probHet[i])*size;
		++i;
	}

	//last
	size=exclude->includedSize((indPositions[i-1]+indPositions[i])/2, end);
	sumOfWeights+=size;
	weightedSum+=(probHomo2[i]+0.5*probHet[i])*size;

	return weightedSum/sumOfWeights;
}





















