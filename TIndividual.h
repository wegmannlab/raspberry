/*
 * TIndividual.h
 *
 *  Created on: Sep 2, 2010
 *      Author: wegmannd
 */

#ifndef TINDIVIDUAL_H_
#define TINDIVIDUAL_H_

#include <stdio.h>
#include <math.h>
#include <vector>
#include <map>
#include <iostream>
#include <algorithm>
#include "TExclude.h"

class TWindow{
public:
	long start, stop;
	double prob;
	TWindow(long Start, long Stop){
		start=Start;
		stop=Stop;
		prob=0;
	};
	void addprob(double add){ prob+=add;};
};

class TInd{
private:
	bool readFile(bool readSnpNames);
	int numColumns;
	int numColumnsBeforeAncestries;

public:
	std::vector<long> indPositions;
	std::vector<double> probChr1;
	std::vector<double> probChr2;
	//ancestries
	int numRefPop;
	int numAncestries;
	std::vector<double*> probAnc;
	double** ancestryWeightPerPop;

	std::vector<double> probHomo1;
	std::vector<double> probHet;
	std::vector<double> probHomo2;
	std::vector<std::string> snpNames;
	std::string filename;
	bool ok;
	bool snpNamesRead;
	bool hasSwitchProbabilities;

	TInd(std::string Filename, bool readSnpNames=false);
	~TInd(){
		for(int p=0; p<numRefPop; ++p){
			delete[] ancestryWeightPerPop[p];
		}
		delete[] ancestryWeightPerPop;
		for(std::vector<double*>::iterator it = probAnc.begin(); it != probAnc.end(); ++it) delete[] *it;
	};

	double getTotalRate(long start, long end);
	long getStart(){return (*indPositions.begin());};
	long getStop(){return indPositions.back();};
	double getAncestry(double* ancestries, int pop);
	void callAndWriteSwitchPoints(long start, long end, int windowSize, double cutOff, std::ofstream* out);
	double getAverageAncestry(long start, long end, double & weightedSum, double & sumOfWeights);
	double getAverageAncestry(long start, long end, TExclude* exclude, double & weightedSum, double & sumOfWeights);
};

#endif /* TINDIVIDUAL_H_ */
