/*
 * TIntervalDB.cpp
 *
 *  Created on: Dec 8, 2010
 *      Author: wegmannd
 */
#include "TIntervalDB.h"


void TIntervalDBSlice::importCounts(std::vector<std::string>* val){
	//val contains the elements of a file written by a the write function
	//third element is totCount
	//all other elements are counts for each bin
	totCount+=stringToLong(val->at(2));
	for(int i=0; i<numBins; ++i){
		countsDB[i]+=stringToLong(val->at(3+i));
	}
	//now recompute probabilities
	computeProbDB();
}

//---------------------------------------------------------------------------
TIntervalDBWindow::TIntervalDBWindow(long Window, long EdgeRemoval, int NumBins, double MaxProb){
	window=Window;
	edgeRemoval=EdgeRemoval;
	numBins=NumBins;
	maxProb=MaxProb;
	mapInitialized=false;
	myMap = NULL;
};
void TIntervalDBWindow::initializeMap(TInd* myInd){
	if(mapInitialized) delete myMap;
	myMap=new TBasicMap(window, myInd->getStart()+edgeRemoval, myInd->getStop()-edgeRemoval, (long) 1000);
	mapInitialized=true;
}

void TIntervalDBWindow::addIndividual(TInd* myInd, TIntervalDBInd* indData){
	//is map initialized?
	if(!mapInitialized) initializeMap(myInd);
	else {
		//is it the right map?
		if(myMap->start==(myInd->getStart()+edgeRemoval) && myMap->end==(myInd->getStop()-edgeRemoval)) myMap->empty();
		else initializeMap(myInd);
	}
	//now make map
	myMap->addProbabilities(myInd);

	//now add probabilities
	for(int i=0; i<myMap->numBins; ++i){
		//find number of swicth points
		addProb(indData->getNumSwitchpoints(myMap->binStart[i], myMap->binEnd[i]), myMap->bins[i]);
	}
}
void TIntervalDBWindow::importSlice(std::vector<std::string>* val){
	//val contains the elements of a file written by a the write function
	//second element is numSwitchpoints
	//all other elements are counts and will be parsed by the slice
	//slice exists?
	int NumSwitchpoints=stringToInt(val->at(1));
	slicesIt=slices.find(NumSwitchpoints);
		if(slicesIt==slices.end()){
		slices.insert(std::pair<int, TIntervalDBSlice*>(NumSwitchpoints, new TIntervalDBSlice(NumSwitchpoints, numBins, maxProb)));
		slicesIt=slices.find(NumSwitchpoints);
	}
	slicesIt->second->importCounts(val);
}
//-----------------------------------------------------------------------------
TIntervalDB::TIntervalDB(std::string PairFile, long EdgeRemoval, int NumBins, double MaxProb, TLog* Logfile){
	pairFile=PairFile;
	edgeRemoval=EdgeRemoval;
	numBins=NumBins;
	maxProb=MaxProb;
	logfile=Logfile;

	//read file
	std::ifstream file;
	logfile->listFlush("Reading file '" + pairFile + "' ...");
	file.open(pairFile.c_str());
	if(!file) throw "File '" + pairFile + " could not be read!";
	std::string buf;

	long line=1;
	std::string oldFile="";
	TIntervalDBInd* thisInd;
	long pos;
	std::vector<std::string> val;
	while(file.good() && !file.eof()){
		getline(file, buf);
		trimString(buf);
		if(!buf.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
			if(val.size()!=2)
				throw "Wrong number of values in file '" + pairFile + " on line " + toString(line) + "!";

			if(oldFile!=val.at(0)){
				individuals.push_back(new TIntervalDBInd(val.at(0)));
				thisInd=individuals.back();
				oldFile=val.at(0);
			}
			pos=stringToLong(val.at(1));
			if(pos>0) thisInd->addSwitchpoint(pos);
		}
		++line;
	}
	file.close();
	initialized=true;
	logfile->write(" done!");
	logfile->conclude("Read " + toString(individuals.size()) + " individuals");
}

TIntervalDB::TIntervalDB(std::string intervalFile){
	//alternative constructor, reading everything from a file written using the write function
	initialized=false;
	import(intervalFile);
}
void TIntervalDB::import(std::string intervalFile){
	logfile->listFlush("Importing intervals from file '" + intervalFile + "' ...");
	std::ifstream file(intervalFile.c_str());
	if(!file) throw "File '" + intervalFile + " could not be read!";

	//read the file
	std::string buf;
	int line=0;
	std::vector<std::string> val;
	int inserted = 0;
	while(file.good() && !file.eof()){
		getline(file, buf);
		trimString(buf);
		if(!buf.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
			if(line==0){
				if(val.size()!=4) throw "Wrong number of entries on first line of file '" + intervalFile + "'!";
				if(initialized){
					//check if the configuration matches
					if(numBins!=stringToInt(val[1])) throw "numBins don't match in file '" + intervalFile + "'!";
					if(maxProb!=stringToDouble(val[2])) throw "maxProb does not match in file '" + intervalFile + "'!";
					if(edgeRemoval!=stringToLong(val[3])) throw "edgeRemoval does not match in file '" + intervalFile + "'!";
				} else {
					//first line contains details about this object
					pairFile=val.at(0);
					numBins=stringToInt(val[1]);
					maxProb=stringToDouble(val[2]);
					edgeRemoval=stringToLong(val[3]);
					initialized=true;
				}
			} else {
				//generate window and pass val to it for parsing
				if(val.size()!=(unsigned int)(numBins+3)) throw "Wrong number of entries on line "+ toString(line) +" in file '" + intervalFile + "'!";
				//window exists?
				long w=stringToLong(val[0]);
				windowsIt=windows.find(w);
				if(windowsIt==windows.end()){
					windows.insert(std::pair<int, TIntervalDBWindow*>(w, new TIntervalDBWindow(w, edgeRemoval, numBins, maxProb)));
					windowsIt=windows.find(w);
					++inserted;
				}
				windowsIt->second->importSlice(&val);
			}
			++line;
		}
	}
	file.close();
	logfile->write(" done!");
	logfile->conclude("Added " + toString(inserted) + " windows");
}

void TIntervalDB::fillDB(){
	//go through all individuals
	int n=0;
	clock_t start=clock();
	logfile->listFlush("Filling DB ... (0% in 0s)");
	int oldP=0; int newP=0;
	for(std::vector<TIntervalDBInd*>::iterator indIt=individuals.begin(); indIt!=individuals.end(); ++indIt, ++n){
		newP=100*((double) n / (double) individuals.size());
		if(newP>oldP){
			logfile->listOverFlush("Filling DB ... (" + toString(newP) + "% in " + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)");
			oldP=newP;
		}
		TInd* myInd=new TInd((*indIt)->file);
		if(myInd->ok){
			for(windowsIt=windows.begin(); windowsIt!=windows.end(); ++windowsIt){
				windowsIt->second->addIndividual(myInd, (*indIt));
			}
		} else {
			logfile->warning("Problems reading file '" +(*indIt)->file +"'! skipping this file ...");
		}
		delete myInd;
	}
	logfile->listFlush("Filling DB ... done! (in " + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)  ");

	//compute probabilities
	logfile->listFlush("Computing probabilities ...");
	for(windowsIt=windows.begin(); windowsIt!=windows.end(); ++windowsIt){
		windowsIt->second->computeProbDB();
	}
	logfile->write(" done!");
}

void TIntervalDB::write(std::string filename){
	//this function writes the whole DB to a file
	logfile->listFlush("Writing interval DB to '" + filename + "' ...");
	//open file
	std::ofstream file(filename.c_str());
	//first row: pairFile, umBins, maxProb and edgeRemoval
	file << pairFile << " " << numBins << " " << maxProb << " " << edgeRemoval << std::endl;
	//now the counts: one row per combination of window and numSwitches
	for(windowsIt=windows.begin(); windowsIt!=windows.end(); ++windowsIt){
		windowsIt->second->write(&file);
	}
	file.close();
	logfile->write(" done!");
}
