/*
 * TIntervalDB.h
 *
 *  Created on: Dec 8, 2010
 *      Author: wegmannd
 */

#ifndef TINTERVALDB_H_
#define TINTERVALDB_H_

#include "TBasicMap.h"
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/time.h>
#include <sys/resource.h>
#include "TLog.h"

class TIntervalDBSlice{
public:
	int numSwitchpoints;
	double* probDB;
	long* countsDB;
	long totCount;
	int numBins;
	double maxProb;

	TIntervalDBSlice(int NumSwitchpoints, int NumBins, double MaxProb){
		numSwitchpoints=NumSwitchpoints;
		numBins=NumBins;
		maxProb=MaxProb;
		probDB=new double[numBins];
		countsDB=new long[numBins];
		for(int i=0; i<numBins; ++i) countsDB[i]=0;
		totCount=0;
	};
	~TIntervalDBSlice(){
		delete[] probDB;
		delete[] countsDB;
	};
	void addProb(const double & Prob){
		++totCount;
		if(Prob >= maxProb) throw "Observed probability (" + toString(Prob) + ") larger than maxProb!";
		++countsDB[(int)floor(Prob/maxProb*numBins)];
	};
	void computeProbDB(){
		double tot=totCount+numBins;
		for(int i=0; i<numBins; ++i){
			probDB[i]=(double) (countsDB[i]+1)/tot;
		}
	};
	double getLikelihood(double & Prob){
		if(Prob >= maxProb) return 1.0/(double)(totCount+1);
		return probDB[(int) floor(Prob/maxProb*numBins)];
	};
	void write(std::ofstream* file){
		//first write numSwitchpoints and tot counts
		*file << numSwitchpoints << " " << totCount;
		//then add counts
		for(int i=0; i<numBins; ++i){
			*file << " " << countsDB[i];
		}
		*file << std::endl;
	};
	void importCounts(std::vector<std::string>* val);
};

class TIntervalDBInd{
public:
	std::string file;
	std::vector<long> switchpoints;

	TIntervalDBInd(std::string File){file=File;};
	void addSwitchpoint(long sw){ switchpoints.push_back(sw);};
	int getNumSwitchpoints(long start, long stop){
		int num=0;
		for(std::vector<long>::iterator it=switchpoints.begin(); it!=switchpoints.end(); ++it){
			if((*it)<stop && (*it)>start) ++num;
		}
		return num;
	};
	void importCounts(std::vector<std::string>* val);
};

class TIntervalDBWindow{
public:
	long window;
	long edgeRemoval;
	int numBins;
	double maxProb;

	std::map<int, TIntervalDBSlice*> slices;
	std::map<int, TIntervalDBSlice*>::iterator slicesIt;
	TBasicMap* myMap;
	bool mapInitialized;

	TIntervalDBWindow(long Window, long EdgeRemoval, int NumBins, double MaxProb);
	~TIntervalDBWindow(){
		if(mapInitialized) delete myMap;
		for(slicesIt=slices.begin(); slicesIt!=slices.end(); ++slicesIt) delete slicesIt->second;
		slices.clear();
	};
	void initializeMap(TInd* myInd);
	void addIndividual(TInd* myInd, TIntervalDBInd* indData);
	void addProb(int NumSwitchpoints, const double & Prob){
		//slice exists?
		slicesIt=slices.find(NumSwitchpoints);
		if(slicesIt==slices.end()){
			slices.insert(std::pair<int, TIntervalDBSlice*>(NumSwitchpoints, new TIntervalDBSlice(NumSwitchpoints, numBins, maxProb)));
			slicesIt=slices.find(NumSwitchpoints);
		}
		slicesIt->second->addProb(Prob);
	};
	void computeProbDB(){
		for(slicesIt=slices.begin(); slicesIt!=slices.end(); ++slicesIt){
			slicesIt->second->computeProbDB();
		}
	};
	double getLikelihood(double & Prob, int NumSwitchpoints){
		slicesIt=slices.find(NumSwitchpoints);
		if(slicesIt==slices.end()) return 0; //what to do???
		return slicesIt->second->getLikelihood(Prob);
	};
	void write(std::ofstream* file){
		for(slicesIt=slices.begin(); slicesIt!=slices.end(); ++slicesIt){
			//write window, and then let the slices fill up
			*file << window << " ";
			slicesIt->second->write(file);
		}
	};
	void importSlice(std::vector<std::string>* val);
	int getMaxNumSwitchpoints(){
		if(slices.size()==0) throw "Probabilities for window size "+ toString(window) +" have not been computed!";
		return slices.rbegin()->first;
	};
	TIntervalDBSlice* getSlice(int NumSwitchpoints){
		slicesIt=slices.find(NumSwitchpoints);
		if(slicesIt==slices.end()) throw "IntervalDBSlice for numSwitchPoints size "+ toString(NumSwitchpoints) +" has not been created!";
		return slicesIt->second;
	}

};

class TIntervalDB{
private:
	TLog* logfile;
	std::string pairFile;
	long edgeRemoval;
	int numBins;
	double maxProb;
	bool initialized;
	std::vector<TIntervalDBInd*> individuals;
	std::map<long, TIntervalDBWindow*> windows;
	std::map<long, TIntervalDBWindow*>::iterator windowsIt;

public:
	TIntervalDB(std::string PairFile, long EdgeRemoval, int NumBins, double MaxProb, TLog* Logfile);
	TIntervalDB(std::string intervalFile);
	~TIntervalDB(){
		for(windowsIt=windows.begin(); windowsIt!=windows.end(); ++windowsIt) delete windowsIt->second;
		windows.clear();
	}
	void import(std::string intervalFile);
	void addWindow(long Window){
		//window exists?
		windowsIt=windows.find(Window);
		if(windowsIt==windows.end()){
			windows.insert(std::pair<int, TIntervalDBWindow*>(Window, new TIntervalDBWindow(Window, edgeRemoval, numBins, maxProb)));
		}
	};
	void fillDB();
	double getLikelihood(long & Window, double & Prob, int & NumSwitchpoints){
		windowsIt=windows.find(Window);
		if(windowsIt==windows.end()) throw "Probabilities for window size "+ toString(Window) +" have not been computed!";
		return windowsIt->second->getLikelihood(Prob, NumSwitchpoints);
	};
	void write(std::string file);
	int getMaxNumSwitchpoints(long & Window){
		windowsIt=windows.find(Window);
		if(windowsIt==windows.end()) throw "Probabilities for window size "+ toString(Window) +" have not been computed!";
		return windowsIt->second->getMaxNumSwitchpoints();
	};
	TIntervalDBSlice* getSlice(long & Window, int & NumSwitchpoints){
		windowsIt=windows.find(Window);
		if(windowsIt==windows.end()) throw "Probabilities for window size "+ toString(Window) +" have not been computed!";
		return windowsIt->second->getSlice(NumSwitchpoints);
	};
};



#endif /* TINTERVALDB_H_ */
