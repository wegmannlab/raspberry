/*
 * TMapEmpiricalBayes.cpp
 *
 *  Created on: Dec 13, 2010
 *      Author: wegmannd
 */

#include "TMap.h"

//--------------------------------------------------------------------
void TMapEmpiricalBayes::generalInitialization(){
	empiricalBayes=false;
	avgPerIndBinsInitialized=false;
	intervalDBInitialized=false;
	empricalBayesBinsStandardized=false;
	ranksComputed=false;
	empricalBayesRanksComputed=false;
	useSavedTotalRates=false;
};
//--------------------------------------------------------------------
//some access functions
double* TMapEmpiricalBayes::getPointerToRanks(){
	if(!ranksComputed){
		if(!recombination) throw "getPointerToRanks: recombination rates have not yet been computed!";
		ranks=new double[numBins];
		fillRanks(bins, ranks);
		ranksComputed=true;
	}
	return ranks;
}
double* TMapEmpiricalBayes::getPointerToRanksEmpricalBayes(){
	if(!empricalBayesRanksComputed){
		if(!empiricalBayes) throw "getPointerToRanksEmpricalBayes: empirical Bayes recombination rates have not yet been computed!";
		empiricalBayesRanks=new double[numBins];
		fillRanks(empiricalBayesBins, empiricalBayesRanks);
		empricalBayesRanksComputed=true;
	}
	return empiricalBayesRanks;
}
double* TMapEmpiricalBayes::getPointerToRecombinationEmpricalBayes(){
	if(!empiricalBayes) throw "getPointerToRanksEmpricalBayes: empirical Bayes recombination rates have not yet been computed!";
	return empiricalBayesBins;
}
//--------------------------------------------------------------------
TMapEmpiricalBayes::TMapEmpiricalBayes(std::string Filename, TExclude* exclude){
	//this constructor reads a map file written using the write function back in memory
	//open file
	std::ifstream file;
	file.open(Filename.c_str());
	if(!file){
		throw "File '" + Filename + " could not be read!";
	}
	generalInitialization();

	//read header
	std::string buf;
	std::vector<std::string> val;
	fillVectorFromLineWhiteSpaceSkipEmpty(file, val);
	//determine the format
	//the first three columns are always start, end and mid of bins
	//the we have a bunch of optional columns:
	// - recombination
	// - empiricalBayes recombination
	// - ancestry
	// - ranks
	int empiricalBayesCol;
	int firstAncestryCol;
	numRefPop=0;
	unsigned int lastAncestyCol;
	int rankCol;
	int empiricalBayesRankCol;
	//bool normRec=false;
	//bool normRecEmpiricalBayes=false;
	ancestryStorageInitialized=false;

	int numCols=3;
	for(unsigned int i=3; i<val.size(); ++i){
		if(stringContains(val[i],"empiricalBayes")){
			if(stringContains(val[i],"relRecNorm")){
				//normRecEmpiricalBayes=true;
				++numCols;
			}
			else if(stringContains(val[i],"cm/Mb") || stringContains(val[i],"relRec")){
				empiricalBayes=true;
				empiricalBayesCol=i;
				++numCols;
			}
			else if(stringContains(val[i],"rank")){
				empricalBayesRanksComputed=true;
				empiricalBayesRankCol=i;
				++numCols;
			}
		}
		else if(stringContains(val[i],"ancestry")){
			if(!ancestries){
				ancestries=true;
				firstAncestryCol=i;
			} else {
				if(i!=lastAncestyCol+1) throw "Unexpected ancestry column in '" + Filename + "'!";
			}
			lastAncestyCol=i;
			++numRefPop;
			++numCols;
		}
		else if(stringContains(val[i],"rank")){
			ranksComputed=true;
			rankCol=i;
			++numCols;
		}
		else if(stringContains(val[i],"relRecNorm")){
			//normRec=true;
			++numCols;
		}
		else if(stringContains(val[i],"cm/Mb")  || stringContains(val[i],"relRec")){
				recombination=true; //is always the fourth column
				++numCols;
		}
	}

	std::vector<long> startVec;
	std::vector<long> endVec;
	std::vector<double> recVec;
	std::vector<double> empBayesVec;
	std::vector<double*> ancVec;
	std::vector<long> rankVec;
	std::vector<long> empBayesRankVec;

	bool checkExclusion=false;
	if(exclude!=NULL && exclude->size()>0){
		checkExclusion=true;
		//in case of exclusion, the ranks have to be recomputed
		//ranksComputed=false;
		//empricalBayesRanksComputed=false;
	}

	long line=1;
	long tempStart, tempEnd;
	while(file.good() && !file.eof()){
		getline(file, buf);
		trimString(buf);
		if(!buf.empty()){ //skip empty lines
			fillVectorFromStringWhiteSpaceSkipEmpty(buf, val);
			if(val.size()!=(std::vector<std::string>::size_type) numCols){
				throw "Wrong number of values in file '" + Filename + " on line " + toString(line) + ": expected " + toString(numCols) + ", found " + toString(val.size()) + "!";
			}
			tempStart=stringToLong(val[0]);
			tempEnd=stringToLong(val[1]);
			if(!checkExclusion || (exclude->include(tempStart) && exclude->include(tempEnd))){
				startVec.push_back(tempStart);
				endVec.push_back(tempEnd);
				if(recombination) recVec.push_back(stringToDouble(val[3]));
				if(empiricalBayes) empBayesVec.push_back(stringToDouble(val[empiricalBayesCol]));
				if(ancestries){
					ancVec.push_back(new double[numRefPop]);
					std::vector<double*>::reverse_iterator it=ancVec.rbegin();
					for(int p=0; p<numRefPop; ++p)
						(*it)[p]=stringToDouble(val[firstAncestryCol+p]);
				}
				if(ranksComputed) rankVec.push_back(stringToLong(val[rankCol]));
				if(empricalBayesRanksComputed) empBayesRankVec.push_back(stringToLong(val[empiricalBayesRankCol]));
			}
		}
		++line;
	}
	file.close();


	//construct bins
	numBins=startVec.size();
	bins=new double[numBins];
	binStart=new long[numBins];
	binEnd=new long[numBins];
	if(empiricalBayes) empiricalBayesBins=new double[numBins];
	if(ranksComputed) ranks=new double[numBins];
	if(empricalBayesRanksComputed) empiricalBayesRanks=new double[numBins];
	if(ancestries) initializeAncestryStorage(numRefPop);
	for(int i=0; i<numBins; ++i){
		binStart[i]=startVec[i];
		binEnd[i]=endVec[i];
		if(recombination) bins[i]=recVec[i]; else bins[i]=0;
		if(ancestries){
			for(int p=0; p<numRefPop; ++p)
				ancestryPerPop[p][i]=ancVec[i][p];
		}
		if(empiricalBayes) empiricalBayesBins[i]=empBayesVec[i];
		if(ranksComputed) ranks[i]=rankVec[i];
		if(empricalBayesRanksComputed) empiricalBayesRanks[i]=empBayesRankVec[i];
	}

	//other
	numInd=0.0;
	if(recombination) binsStandardized=true; else binsStandardized=false;
	if(ancestries) ancestriesStandardized=true; else ancestriesStandardized=false;
	if(empiricalBayes) empricalBayesBinsStandardized=true; else empricalBayesBinsStandardized=false;


	//is it a map with evenly spaced bins and have all windows the same size?
	long temp=binEnd[0]-binStart[0];
	for(long i=1; i<numBins; ++i){
		if((binEnd[i]-binStart[i])!=temp){
			temp=-1;
			break;
		}
	}
	windowsize=temp;
	if(windowsize>0){
	temp=binStart[1]-binStart[0];
		for(long i=2; i<numBins; ++i){
			if((binStart[i]-binStart[i-1])!=temp){
				temp=-1;
				break;
			}
		}
	}
	step=temp;
	//save output filename
	filename=extractBeforeLast(Filename, '.');
}

void TMapEmpiricalBayes::initializeEmpiricalBayes(TIntervalDB* IntervalDB){
	if(empiricalBayes) throw "Empirical Bayes already initialized!";
	if(windowsize<1) throw "Empirical Bayes only implemented for maps with constant window sizes!";
	empiricalBayesBins=new double[numBins];
	for(int i=0; i<numBins; ++i) empiricalBayesBins[i]=0;
	intervalDB=IntervalDB;
	//compute average per individual, used for empirical Bayes
	if(!avgPerIndBinsInitialized){
		avgPerIndBins=new double[numBins];
		avgPerIndBinsInitialized=true;
	}
	for(int i=0; i<numBins; ++i){
		avgPerIndBins[i]=bins[i]/numInd;
	}
	//get maxY
	maxY=intervalDB->getMaxNumSwitchpoints(windowsize);
	//prepare pointers to get likelihoods
	intervalDBSlices=new TIntervalDBSlice*[maxY+1];
	for(int y=0; y<=maxY;++y){
		intervalDBSlices[y]=intervalDB->getSlice(windowsize, y);
	}
	//fill prior
	empiricalBayesPrior=new double*[maxY+1];
	for(int y=0; y<=maxY;++y){
		empiricalBayesPrior[y]=new double[numBins];
	}
	double fac;
	double prior_temp;
	for(int i=0; i<numBins; ++i){
		prior_temp=exp(-avgPerIndBins[i]);
		fac=1;
		empiricalBayesPrior[0][i]=prior_temp;
		for(int y=1; y<=maxY; ++y){
			fac=fac*y;
			empiricalBayesPrior[y][i]=prior_temp*pow(avgPerIndBins[i], y)/fac;
		}
	}
	//done!
	empiricalBayes=true;
	intervalDBInitialized=true;
	empricalBayesBinsStandardized=false;
}

void TMapEmpiricalBayes::addProbabilitiesEmpiricalBayes(TInd* oneInd){
	//AFTER an individual has been read...
	if(!empiricalBayes) throw "Empirical Bayes has not been initialized!";
	if(empricalBayesBinsStandardized) throw "addProbabilitiesEmpiricalBayes: Impossible to add individuals after the map has been standardized!";
	std::vector<long>::iterator posIt=oneInd->indPositions.begin();
	std::vector<double>::iterator prob1It=oneInd->probChr1.begin();
	std::vector<double>::iterator prob2It=oneInd->probChr2.begin();
	//iterators to stor staring positions of old bin (BINS ARE OVERLAPPING!)
	std::vector<long>::iterator posIt_old;
	std::vector<double>::iterator prob1It_old;
	std::vector<double>::iterator prob2It_old;
	long oldPos_old;
	double tempProb;
	long oldPos=(*posIt); //probabilities are always from last to this SNP
	bool go;
	++posIt; ++prob1It; ++prob2It;
	for(long i=0; i<numBins; ++i){
		tempProb=0;
		while((*posIt)<binStart[i] && posIt!=oneInd->indPositions.end()){
			oldPos=(*posIt);
			++posIt; ++prob1It; ++prob2It;
		}
		if(posIt==oneInd->indPositions.end()) break;
		//save starting positions for next bin
		posIt_old=posIt;
		prob1It_old=prob1It;
		prob2It_old=prob2It;
		oldPos_old=oldPos;

		//now fill in
		if(oldPos<binEnd[i]) go=true;
		else go=false;

		while(go){
			 if(oldPos<binStart[i]){
				//only part ...
				if((*posIt)<binEnd[i]){
					tempProb+=(double)((*posIt)-binStart[i])/(double)((*posIt)-oldPos)*((*prob1It)+(*prob2It));
				} else {
					tempProb+=(double)(binEnd[i]-binStart[i])/(double)((*posIt)-oldPos)*((*prob1It)+(*prob2It));
				}
			} else {
				if((*posIt)<binEnd[i]){
					//all
					tempProb+=(*prob1It)+(*prob2It);
				} else {
					tempProb+=(double)(binEnd[i]-oldPos)/(double)((*posIt)-oldPos)*((*prob1It)+(*prob2It));
				}
			}
			if((*posIt)<binEnd[i]){
				//move to next SNP
				oldPos=(*posIt);
				++posIt; ++prob1It; ++prob2It;
				if(posIt==oneInd->indPositions.end())break;
			} else {
				//next bin
				//first: empirical Bayes correction
				//idea: value to add to the map is teh expected number of switch points: SUM_y[p(S==y|probTemp)*y]
				//p(S==y|probTemp)=p(probTemp|S==y)*p(S==y|rate)/SUM_x[p(probTemp|S==x)*p(S==x|rate)]
				//rate is estimated across all individuals (has already been done, stored in double* bins
				//p(probTemp|S==x) is empirically estimated and acessible through TIntervalDB* intervalDB
				double posterior;
				double sum=0;
				double sumNormalize=0;
				for(int y=0;y<=maxY;++y){
					//get posterior
					posterior=intervalDBSlices[y]->getLikelihood(tempProb)*empiricalBayesPrior[y][i];
					sumNormalize+=posterior;
					sum+=y*posterior;
				}
				empiricalBayesBins[i]+=sum/sumNormalize;

				//update pointers/iterators
				go=false;
				posIt=posIt_old;
				prob1It=prob1It_old;
				prob2It=prob2It_old;
				oldPos=oldPos_old;
			}
		 }
		 if(posIt==oneInd->indPositions.end())break;
	}
	numInd+=1.0;
	recombination=true;
}

void TMapEmpiricalBayes::addProbabilitiesEmpiricalBayes(TMapEmpiricalBayes & map){
	//just add probabilities to the empirical bayes bins
	//implemented for non overlappign bins only!
	//assumes that provided map is not standardized
	//function is used to get hotspot usage
	if(!empiricalBayes) throw "addProbabilitiesEmpiricalBayes: empirical Bayes not initialized!";
	if(empricalBayesBinsStandardized) throw "addProbabilitiesEmpiricalBayes: Impossible to add map after the map has been standardized!";
	if(map.empricalBayesBinsStandardized) throw "addProbabilitiesEmpiricalBayes: Impossible to add an already standardized map!";
	if(!map.empiricalBayes) throw "addProbabilitiesEmpiricalBayes: empirical Bayes not computed for map to add!";

	bool go;
	long mapP=0;
	long mapP_old;
	for(int i=0; i<numBins; ++i){
		while(map.binEnd[mapP]<binStart[i] && mapP<map.numBins){
			++mapP;
		}
		mapP_old=mapP; //store this because BINS MAY BE OVERLAPPING!
		if(mapP==map.numBins) break;
		if(map.binStart[mapP]<binEnd[i]) go=true;
		else go=false;
		while(go){
			 if(map.binStart[mapP]<binStart[i]){
				//only part ...
				if(map.binEnd[mapP]<binEnd[i]){
					empiricalBayesBins[i]+=(double)(map.binEnd[mapP]-binStart[i])/(map.binEnd[mapP]-map.binStart[mapP])*map.empiricalBayesBins[mapP];
				} else {
					empiricalBayesBins[i]+=(double)(binEnd[i]-binStart[i])/(map.binEnd[mapP]-map.binStart[mapP])*map.empiricalBayesBins[mapP];
				}
			} else {
				if(map.binEnd[mapP]<binEnd[i]){
					//all
					empiricalBayesBins[i]+=(double)(map.binEnd[mapP]-map.binStart[mapP])/(map.binEnd[mapP]-map.binStart[mapP])*map.empiricalBayesBins[mapP];
				} else {
					empiricalBayesBins[i]+=(double)(binEnd[i]-map.binStart[mapP])/(map.binEnd[mapP]-map.binStart[mapP])*map.empiricalBayesBins[mapP];
				}
			}
			if(map.binEnd[mapP]<binEnd[i]){
				//get to next bin in Map adding from
				++mapP;
				if(mapP>=map.numBins) break;
			} else {
				//fill in next bin of this map
				//_> go back to where we were before teh last bin was filled in BECAUSE BINS ARE OVERLAPPING
				go=false;
				mapP=mapP_old;
			}
		 }
		if(mapP>=map.numBins) break;
	}
	numInd+=0.5;
}


void TMapEmpiricalBayes::scale(){
	//scale from individuals to cm/mb
	if(numInd>0){
		// /2*numInd (per chromosome) / windowsize * 10^6 (per megabse) * 100 (in cM)
		if(recombination && !binsStandardized) {
			for(int i=0; i<numBins; ++i){
				bins[i]=bins[i] * 100000000 / (2*numInd * (binEnd[i]-binStart[i])) ;
			}
			binsStandardized=true;
		}
		if(empiricalBayes && !empricalBayesBinsStandardized){
			for(int i=0; i<numBins; ++i){
				empiricalBayesBins[i]=empiricalBayesBins[i] * 100000000 / (2*numInd * (binEnd[i]-binStart[i]));
			}
			empricalBayesBinsStandardized=true;
		}
		if(ancestries && !ancestriesStandardized){
			for(int i=0; i<numBins; ++i){
				for(int p=0; p<numRefPop; ++p)
					ancestryPerPop[p][i]=ancestryPerPop[p][i]/(2*numInd*(binEnd[i]-binStart[i]));
			}
			ancestriesStandardized=true;
		}
	}
}

void TMapEmpiricalBayes::writeMap(std::string prefix, std::string suffix, bool doScale){
	//scale first
	if(doScale) scale();
	//compute ranks
	computeRanks();
	std::ofstream file;
	std::string temp=prefix+filename+suffix+".map";
	file.open(temp.c_str());
	//header
	file << "start\tend\tmid";
	if(recombination){
		file << "\trelRec\trelRecNorm";
		if(ranksComputed) file << "\trank";
		if(!useSavedTotalRates) totalRate=getTotalRate();
	}
	if(empiricalBayes){
		file << "\trelRec[empiricalBayes]\trelRecNorm[empiricalBayes]";
		if(empricalBayesRanksComputed) file << "\trank[empiricalBayes]";
		if(!useSavedTotalRates) totalRateEmpiricalBayes=getTotalRateEmpiricalBayes();
	}
	if(ancestries){
		for(int p=0; p<numRefPop; ++p)
			file << "\tancestry_" << p+1;
	}
	file << std::endl;
	for(int i=0; i<numBins; ++i){
		file << binStart[i] << "\t" << binEnd[i] << "\t" << (double) (binStart[i]+binEnd[i])/2.0;
		if(recombination){
			file << "\t" << bins[i];
			file << "\t" << bins[i]/totalRate;
			if(ranksComputed) file << "\t" << ranks[i];
		}
		if(empiricalBayes){
			file << "\t" << empiricalBayesBins[i];
			file << "\t" << empiricalBayesBins[i]/totalRateEmpiricalBayes;
			if(empricalBayesRanksComputed) file << "\t" << empiricalBayesRanks[i];
		}
		if(ancestries){
			for(int p=0; p<numRefPop; ++p)
				file << "\t" << ancestryPerPop[p][i];
		}
		file << std::endl;
	}
	file.close();
}

double TMapEmpiricalBayes::iterateEnrichmentAndWriteIntervals(double fraction, std::vector< std::pair<double, long> >::reverse_iterator & sortvecBinsIt, long & numBinsAdded, double & sum, std::vector<long> & acceptedBins, std::string prefix, std::string suffix){
	long needBins=fraction*numBins;
	if(needBins < 1) return 0.0;
	while(numBinsAdded<needBins){
		sum+=sortvecBinsIt->first;
		acceptedBins.push_back(sortvecBinsIt->second);
		++sortvecBinsIt;
		++numBinsAdded;
	}

	//write intervals
	std::string temp=prefix+filename+"_hotspots_" + toString(fraction) + suffix+".txt";
	std::ofstream hotspotFile, hotspotBinsFile;
	hotspotFile.open(temp.c_str());
	hotspotFile << "start\tend" << std::endl;
	temp=prefix+filename+"_hotspotBins_" + toString(fraction) + suffix+".txt";
	hotspotBinsFile.open(temp.c_str());
	hotspotBinsFile << "start\tend" << std::endl;
	//first sort accepted bins and write adjacent bisn as one larger bin
	//thus, sort accepted bins first
	sort(acceptedBins.begin(), acceptedBins.end());
	//no go through the bins and write the intervals
	std::vector<long>::iterator acceptedBinsIt=acceptedBins.begin();
	long oldId=(*acceptedBinsIt);
	long startId=(*acceptedBinsIt);

	hotspotBinsFile << binStart[(*acceptedBinsIt)] << "\t" << binEnd[(*acceptedBinsIt)] << std::endl;
	++acceptedBinsIt;
	double halfstep=(double) step / 2.0;
	double writePos;
	for(; acceptedBinsIt<acceptedBins.end(); ++acceptedBinsIt){
		hotspotBinsFile << binStart[(*acceptedBinsIt)] << "\t" << binEnd[(*acceptedBinsIt)] << "\t" << std::endl;
		if((*acceptedBinsIt)!=(oldId+1)){
			//write old interval
			writePos=((double) (binStart[startId] + binEnd[startId]) / 2.0) - halfstep;
			hotspotFile << (long) writePos;
			writePos=((double) (binStart[(*acceptedBinsIt)] + binEnd[(*acceptedBinsIt)]) / 2.0) + halfstep;
			hotspotFile << "\t" << (long) writePos << std::endl;
			startId=(*acceptedBinsIt);
		}
		oldId=(*acceptedBinsIt);
	}
	hotspotFile.close();
	hotspotBinsFile.close();

	return sum;
}

void TMapEmpiricalBayes::writeEnrichment(std::vector<double> & fractions, std::string prefix, std::string suffix, TLog* logfile){
	//this function assumes bins of equal size!
	logfile->startIndent("Writing enrichment:");
	if(windowsize<0) throw "Enrichment analysis works only with evenly spaced windows of equal size!";

	//the written size of the bins is the step size

	std::ofstream file;
	std::string temp=prefix + filename + suffix+".enrichment";
	logfile->list("Writing enrichment to '" + temp + "'");
	file.open(temp.c_str());
	//header
	file << "fraction";
	if(recombination) file << "\tstandard_max";
	if(empiricalBayes) file << "\tempiricalBayes_max";
	file << std::endl;

	//first sort
	std::vector< std::pair<double, long> > sortvecBins;
	std::vector< std::pair<double, long> > sortvecEmpiricalBayesBins;
	double totalRate;
	double totalrateEmpiricalBayes;

	if(recombination){
		logfile->listFlush("Sorting bins ...");
		for(long i=0; i<numBins; ++i){
			sortvecBins.push_back(std::pair<double, long>(bins[i], i));
		}
		sort(sortvecBins.begin(), sortvecBins.end());
		logfile->write(" done!");
		totalRate=getTotalRate();
	}

	if(empiricalBayes){
		logfile->listFlush("Sorting empirical Bayes bins ...");
		for(long i=0; i<numBins; ++i){
			sortvecEmpiricalBayesBins.push_back(std::pair<double, long>(empiricalBayesBins[i], i));
		}
		sort(sortvecEmpiricalBayesBins.begin(), sortvecEmpiricalBayesBins.end());
		logfile->write(" done!");
		totalrateEmpiricalBayes=getTotalRateEmpiricalBayes();
	}



	//go through all fractions
	double sum=0;
	double sumEmpiricalBayes=0;
	long numBinsAdded=0;
	long numBinsAddedEmpiricalBayes=0;

	std::vector< std::pair<double, long> >::reverse_iterator sortvecBinsIt;
	sortvecBinsIt=sortvecBins.rbegin();
	std::vector<long> acceptedBins;
	std::vector<long> acceptedBinsEmpiricalBayes;
	std::vector< std::pair<double, long> >::reverse_iterator sortvecEmpiricalBayesBinsIt;
	sortvecEmpiricalBayesBinsIt=sortvecEmpiricalBayesBins.rbegin();

	//sort fractions to start from smallest
	sort(fractions.begin(), fractions.end());


	std::ofstream hotspotFile;
	for(std::vector<double>::iterator it=fractions.begin(); it!=fractions.end(); ++it){
		logfile->listFlush("Iterating to " + toString(*it) + " ...");
		file << (*it);
		if(recombination){
			file << "\t" << iterateEnrichmentAndWriteIntervals((*it), sortvecBinsIt, numBinsAdded, sum, acceptedBins, prefix, suffix)/totalRate;
		}
		if(empiricalBayes){
			file << "\t" << iterateEnrichmentAndWriteIntervals((*it), sortvecEmpiricalBayesBinsIt, numBinsAddedEmpiricalBayes, sumEmpiricalBayes, acceptedBinsEmpiricalBayes, prefix, suffix+"_empiricalBayes")/totalrateEmpiricalBayes;
		}
		file << std::endl;
		logfile->write(" done!");
	}
	file.close();
	logfile->endIndent();
}

void TMapEmpiricalBayes::fillRanks(double* theseBins, double* theseRanks){
	//first sort
	std::vector< std::pair<double, long> > sortvec;
	for(long i=0; i<numBins; ++i){
		sortvec.push_back(std::pair<double, long>(theseBins[i], i));
	}
	sort(sortvec.begin(), sortvec.end());

	//no compute the ranks.
	//if we have multiple entires with the same value, they will get the mean rank across all those values!
	//now go through sorted std::vector and add ranks
	double r=1.0;
	double oldVal=sortvec[0].first;
	std::vector<long> oldIndices;
	std::vector<long>::iterator oldIndicesIt;
	oldIndices.push_back(sortvec[0].second);
	for(std::vector< std::pair<double, long> >::size_type i=1;i<sortvec.size(); ++i){
		if(sortvec[i].first>oldVal){
			//save ranks
			r=r/(double) oldIndices.size();
			for(oldIndicesIt=oldIndices.begin(); oldIndicesIt!=oldIndices.end(); ++oldIndicesIt){
				theseRanks[(*oldIndicesIt)]=r;
			}
			oldIndices.clear();
			r=0;
			oldVal=sortvec[i].first;
		}
		//save current
		oldIndices.push_back(sortvec[i].second);
		r+=(i+1);
	}
	//save last
	r=r/(double) oldIndices.size();
	for(oldIndicesIt=oldIndices.begin(); oldIndicesIt!=oldIndices.end(); ++oldIndicesIt){
		theseRanks[(*oldIndicesIt)]=r;
	}
}

void TMapEmpiricalBayes::computeRanks(bool force){
	if(recombination && (!ranksComputed || force)){
		if(!ranksComputed) ranks=new double[numBins];
		fillRanks(bins, ranks);
		ranksComputed=true;
	}
	if(empiricalBayes && (!empricalBayesRanksComputed || force)){
		if(!empricalBayesRanksComputed) empiricalBayesRanks=new double[numBins];
		fillRanks(empiricalBayesBins, empiricalBayesRanks);
		empricalBayesRanksComputed=true;
	}
}

bool TMapEmpiricalBayes::checkBins(TMapEmpiricalBayes* map){
	if(numBins!=map->numBins) return false;
	for(int i=0; i<numBins; ++i){
		if(binStart[i]!=map->binStart[i] || binEnd[i]!=map->binEnd[i]) return false;
	}
	return true;
}

double TMapEmpiricalBayes::getTotalRateEmpiricalBayes(){
	double sum=0;
	for(int i=0; i<numBins; ++i) sum+=empiricalBayesBins[i];
	return sum;
}

double TMapEmpiricalBayes::getTotalRateEmpiricalBayes(long* theseBins, long length){
	double sum=0;
	for(int i=0; i<length; ++i) sum+=empiricalBayesBins[theseBins[i]];
	return sum;
}

void TMapEmpiricalBayes::emptyEmpiricalBayes(){
	for(int i=0; i<numBins; ++i) empiricalBayesBins[i]=0;
}

void TMapEmpiricalBayes::empty(){
	for(int i=0; i<numBins; ++i){
		bins[i]=0;
	}
	emptyAncestries();
	if(ranksComputed) delete[] ranks;
	if(empricalBayesRanksComputed) delete[] empiricalBayesRanks;
	if(empiricalBayes) emptyEmpiricalBayes();
	if(avgPerIndBinsInitialized) delete[] avgPerIndBins;
	numInd=0.0;
	binsStandardized=false;
	ancestriesStandardized=false;
	empricalBayesBinsStandardized=false;
	ranksComputed=false;
	empricalBayesRanksComputed=false;
	avgPerIndBinsInitialized=false;
}

bool TMapEmpiricalBayes::fillBinID(std::vector<long> & bStart, std::vector<long> & bEnd, long* id){
	for(std::vector<long>::size_type i=0; i<bStart.size(); ++i){
		//find id
		for(long j=0; j<=numBins; ++j){
			if(j==numBins) return false;
			if(binStart[j]==bStart[i]){
				if(binEnd[j]==bEnd[i]){
					id[i]=j;
					break;
				}
			}
		}
	}
	return true;
}

void TMapEmpiricalBayes::saveTotalRates(){
	useSavedTotalRates=true;
	if(recombination) totalRate=getTotalRate();
	if(empiricalBayes) totalRateEmpiricalBayes=getTotalRateEmpiricalBayes();
}

void TMapEmpiricalBayes::saveTotalRates(TMapEmpiricalBayes & map){
	useSavedTotalRates=true;
	if(recombination){
		if(map.recombination) totalRate=map.getTotalRate();
		else totalRate=getTotalRate();
	}
	if(empiricalBayes){
		if(map.empiricalBayes) totalRateEmpiricalBayes=map.getTotalRateEmpiricalBayes();
		else totalRateEmpiricalBayes=getTotalRateEmpiricalBayes();
	}
}
