/*
 * TMapEmpiricalBayes.h
 *
 *  Created on: Dec 13, 2010
 *      Author: wegmannd
 */

#ifndef TMAP_H_
#define TMAP_H_
#include <algorithm>
#include "TIntervalDB.h"
#include "TExclude.h"
#include "TLog.h"

class TMapEmpiricalBayes:public TBasicMap{
private:
	double* avgPerIndBins;
	double* empiricalBayesBins;
	TIntervalDB* intervalDB;
	bool empiricalBayes;
	bool intervalDBInitialized;
	int maxY;
	double** empiricalBayesPrior;
	TIntervalDBSlice** intervalDBSlices;
	bool avgPerIndBinsInitialized;
	bool empricalBayesBinsStandardized;
	double* ranks;
	double* empiricalBayesRanks;
	bool ranksComputed;
	bool empricalBayesRanksComputed;
	bool useSavedTotalRates;
	double totalRateEmpiricalBayes, totalRate;

	void generalInitialization();
	void fillRanks(double* theseBins, double* theseRanks);
	double iterateEnrichmentAndWriteIntervals(double fraction, std::vector< std::pair<double, long> >::reverse_iterator & sortvecBinsIt, long & numBinsAdded, double & sum, std::vector<long> & acceptedBins, std::string prefix, std::string suffix);
public:
	TMapEmpiricalBayes(std::string filename, std::string format, long start, long end):TBasicMap(filename, format, start, end){generalInitialization();};
	TMapEmpiricalBayes(std::string filename, std::string format):TBasicMap(filename, format){generalInitialization();};

	TMapEmpiricalBayes(long Windowsize, long Start, long End, long Step):TBasicMap(Windowsize, Start, End, Step){generalInitialization();};
	TMapEmpiricalBayes(long Windowsize, long Start, long End):TBasicMap(Windowsize, Start, End){generalInitialization();};
	TMapEmpiricalBayes(long Windowsize, long Start, long End, double Increment):TBasicMap(Windowsize, Start, End, Increment){generalInitialization();};

	TMapEmpiricalBayes(TBasicMap* map):TBasicMap(map){generalInitialization();};
	TMapEmpiricalBayes(TBasicMap* map, long start, long end):TBasicMap(map, start, end){generalInitialization();};

	TMapEmpiricalBayes(std::string Filename, TExclude* exclude=NULL);

	~TMapEmpiricalBayes(){
		if(avgPerIndBinsInitialized) delete[] avgPerIndBins;
		if(intervalDBInitialized){
			for(int y=0; y<=maxY;++y){
				delete[] empiricalBayesPrior[y];
			}
			delete[] empiricalBayesPrior;
			delete[] intervalDBSlices;
		}
		if(empiricalBayes) delete[] empiricalBayesBins;
		if(ranksComputed) delete[] ranks;
		if(empricalBayesRanksComputed) delete[] empiricalBayesRanks;
	}

	//some access functions
	bool empiricalBayesComputed(){ return empiricalBayes; };
	double* getPointerToRanks();
	double* getPointerToRanksEmpricalBayes();
	double* getPointerToRecombinationEmpricalBayes();

	void initializeEmpiricalBayes(TIntervalDB* IntervalDB);
	void addProbabilitiesEmpiricalBayes(TInd* oneInd);
	void addProbabilitiesEmpiricalBayes(TMapEmpiricalBayes & map);
	void scale();
	void writeMap(std::string prefix, std::string suffix, bool doScale=true);
	void writeEnrichment(std::vector<double> & fractions, std::string prefix, std::string suffix, TLog* logfile);
	void computeRanks(bool force=false);
	virtual bool checkBins(TMapEmpiricalBayes* map);
	double getTotalRateEmpiricalBayes();
	double getTotalRateEmpiricalBayes(long* theseBins, long length);
	virtual void empty();
	void emptyEmpiricalBayes();
	bool fillBinID(std::vector<long> & bStart, std::vector<long> & bEnd, long* id);
	void saveTotalRates();
	void saveTotalRates(TMapEmpiricalBayes & map);
};

#endif /* TMAP_H_ */
