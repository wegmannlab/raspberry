/*
 * data.h
 *
 *  Created on: Feb 4, 2010
 *      Author: wegmannd
 */

#ifndef DATA_H_
#define DATA_H_
#include "stringFunctions.h"
#include "snpDB.h"
#include <map>
#include <vector>
#include <set>
#include <sstream>
#include <iostream>
#include <fstream>
#include <time.h>
#include "TLog.h"

//---------------------------------------------------------------------------
class individual{
public:
	std::vector<int> allele1;
	std::vector<int> allele2;

	individual(){};
	bool addSNP(char Allele1, char Allele2, char refAllele1, char refAllele2);
	int getNumberMissingAlleles();
	int getUnphasedGenotype(long snpId);
	std::string getEIGENSTRATphased(long snpId);
	std::string getHAPMIXhaplotype1(long snpId);
	std::string getHAPMIXhaplotype2(long snpId);
	std::string getHAPMIXgenotype(long snpId);
};
//---------------------------------------------------------------------------
class data{
public:
	TLog* logfile;
	std::map<long, long> snps; //<id, internal number>
	std::vector<individual*> myIndividuals;
	snpDB* mySnpDB;
	long nSNPs;
	int nIndividuals;
	double genotypingRate;
	std::set<std::string> chromosomes;
	std::map<long, snp*> snpMap;
	std::map<long, long> snpMapID;
	std::map<long, long>::iterator snpMapIDIt;
	std::map<long, snp*>::iterator snpMapIt;

	data(snpDB* MySnpDB, std::string filename, TLog* Logfile);
	~data(){
		snps.clear();
		for(int i=0; i<nIndividuals;++i) delete myIndividuals[i];
		myIndividuals.clear();
		chromosomes.clear();
		snpMap.clear();
		snpMapID.clear();
	};
	void readOneSNPPerLine(std::string filename, int nameColumn, int colsToSkip, bool header);
	void readTPED(std::string filename);
	void readPED(std::string filename);
	void readPHASED(std::string filename);
	double getGenotypingRate();
	std::string getFirstChr();
	bool snpIsTyped(long id);
	void fillChromosomeList();
	void fillSNPmap(std::string chr);
	void fillHaplotypeArray(bool* array, long snpId);
	void fillGenotypeArray(int* array, long snpId);
	void writeEIGENSTRAT(std::string basename, bool phased=false);
	void writeHAPMIXreference(std::string basename);
	void writeHAPMIXadmixed(std::string basename);
};

#endif /* DATA_H_ */
