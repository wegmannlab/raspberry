/*
 * forwardBackward.cpp
 *
 *  Created on: Mar 3, 2010
 *      Author: wegmannd
 */

#include "forwardBackward.h"
/*
hmmvar::hmmvar(){
	logfile = NULL;
	pars = NULL;
	data = NULL;
	tempStorage = NULL;
	transProbHaplotypes = NULL;
	transProbCopyPopIdentityTerm = NULL;
	copyPopIdentityTypeSwitchProbability = NULL;
	typeSwitchProbability = NULL;
	LL = 0;
}
*/
hmmvar::hmmvar(hmmDataStorage* Data, hmmpars* Pars, TLog* Logfile){
	logfile = Logfile;
	pars = Pars;
	data = Data;
	tempStorage = NULL;
	transProbHaplotypes = NULL;
	transProbCopyPopIdentityTerm = NULL;
	copyPopIdentityTypeSwitchProbability = NULL;
	typeSwitchProbability = NULL;
	LL = 0;
}

forward::forward(hmmDataStorage* Data, hmmpars* Pars, int RecomputeWindowSize, TLog* Logfile):hmmvar(Data, Pars, Logfile){
	recomputeWindowSize=RecomputeWindowSize;
	nSavePoints=2+((double) data->nsnps / (double) recomputeWindowSize);
	firstRecomputedSNP = 0;
	tempPrecomputedSums = NULL;
	precomputedSumsCopyPopIdentityFirst = NULL;
	precomputedSumsCopyPopIdentitySecond = NULL;
	precomputedSums = NULL;
	forwardSaved = NULL;
	forwardLocal = NULL;
	scalingTerm = NULL;
}

backward::backward(hmmDataStorage* Data, hmmpars* Pars, TLog* Logfile):hmmvar(Data, Pars, Logfile){
	doComputeSwitchProb=true;
	switchingProbability = NULL;
	numStateProbabilities = 0;
	stateProbability = NULL;
	alleleProbability = NULL;
	transProbTemp = NULL;
	precomputedSumsCopyPopIdentityFirst = NULL;
	precomputedSumsCopyPopIdentitySecond = NULL;
	tempPrecomputedSums = NULL;
	precomputedSums = NULL;
	scalingTerm = 0;
}
//---------------------------------------------------------------------------
void hmmvar::allocateMemory(){
	//initialize tempStorage variable storage to perform calculations -> we only need the last complete set for teh forward variable
	tempStorage=new double******[2];
	for(int s=0; s<2; ++s){
		tempStorage[s]=new double*****[data->nRefpop];
		for(int i=0; i<data->nRefpop; ++i){
			tempStorage[s][i]=new double****[data->nRefpop];
			for(int j=0; j<data->nRefpop; ++j){
				tempStorage[s][i][j]=new double***[data->maxStatesPerPop];
				for(unsigned int k=0; k<data->maxStatesPerPop; ++k){
					tempStorage[s][i][j][k]=new double**[data->nRefpop];
					for(int l=0; l<data->nRefpop; ++l){
						tempStorage[s][i][j][k][l]=new double*[data->nRefpop];
						for(int m=0; m<data->nRefpop; ++m){
							tempStorage[s][i][j][k][l][m]=new double[data->maxStatesPerPop];
						}
					}
				}
			}
		}
	}
	//transmission probabilities
	transProbHaplotypes=new double**[2];
	for(int i=0; i<2; ++i){
		transProbHaplotypes[i]=new double*[2];
		for(int j=0; j<2; ++j){
			transProbHaplotypes[i][j]=new double[data->nRefpop];
		}
	}
	transProbCopyPopIdentityTerm=new double[data->nRefpop];

	//type switch probabilities
	copyPopIdentityTypeSwitchProbability=new double**[data->nRefpop];
	typeSwitchProbability=new double*[data->nRefpop];
	for(int i=0; i<data->nRefpop; ++i){
		typeSwitchProbability[i]=new double[data->maxStatesPerPop];
		copyPopIdentityTypeSwitchProbability[i]=new double*[data->maxStatesPerPop];
		for(unsigned int j=0; j<data->maxStatesPerPop; ++j){
			copyPopIdentityTypeSwitchProbability[i][j]=new double[data->maxStatesPerPop];
		}
	}
}
void hmmvar::freeMemory(){
		for(int s=0; s<2; ++s){
			for(int i=0; i<data->nRefpop; ++i){
				for(int j=0; j<data->nRefpop; ++j){
					for(unsigned int k=0; k<data->maxStatesPerPop; ++k){
						for(int l=0; l<data->nRefpop; ++l){
							for(int m=0; m<data->nRefpop; ++m){
								delete[] tempStorage[s][i][j][k][l][m];
							}
							delete[] tempStorage[s][i][j][k][l];
						}
						delete[] tempStorage[s][i][j][k];
					}
					delete[] tempStorage[s][i][j];
				}
				delete[] tempStorage[s][i];
			}
			delete[] tempStorage[s];
		}
		delete[] tempStorage;

		//transmission probabilities
		for(int i=0; i<2; ++i){
			for(int j=0; j<2; ++j){
				delete[] transProbHaplotypes[i][j];
			}
			delete[] transProbHaplotypes[i];
		}
		delete[] transProbHaplotypes;
		delete[] transProbCopyPopIdentityTerm;

		//type switch probabilities
		for(int i=0; i<data->nRefpop; ++i){
			for(unsigned int j=0; j<data->maxStatesPerPop; ++j){
				delete[] copyPopIdentityTypeSwitchProbability[i][j];
			}
			delete[] typeSwitchProbability[i];
			delete[] copyPopIdentityTypeSwitchProbability[i];
		}
		delete[] copyPopIdentityTypeSwitchProbability;
		delete[] typeSwitchProbability;
}
//---------------------------------------------------------------------------
void forward::allocateMemory(){
	hmmvar::allocateMemory();

	//initialize storage for forward probabilities-> we have to store the WHOLE set, but do it only at some points
	forwardSaved=new double******[nSavePoints];
	for(long s=0; s<nSavePoints; ++s){
		forwardSaved[s]=new double*****[data->nRefpop];
		for(int i=0; i<data->nRefpop; ++i){
			forwardSaved[s][i]=new double****[data->nRefpop];
			for(int j=0; j<data->nRefpop; ++j){
				forwardSaved[s][i][j]=new double***[data->maxStatesPerPop];
				for(unsigned int k=0; k<data->maxStatesPerPop; ++k){
					forwardSaved[s][i][j][k]=new double**[data->nRefpop];
					for(int l=0; l<data->nRefpop; ++l){
						forwardSaved[s][i][j][k][l]=new double*[data->nRefpop];
						for(int m=0; m<data->nRefpop; ++m){
							forwardSaved[s][i][j][k][l][m]=new double[data->maxStatesPerPop];
						}
					}
				}
			}
		}
	}

	//initialize local forward storage
	forwardLocal=new double******[recomputeWindowSize-1];
	for(long s=0; s<(recomputeWindowSize-1); ++s){
		forwardLocal[s]=new double*****[data->nRefpop];
		for(int i=0; i<data->nRefpop; ++i){
			forwardLocal[s][i]=new double****[data->nRefpop];
			for(int j=0; j<data->nRefpop; ++j){
				forwardLocal[s][i][j]=new double***[ data->maxStatesPerPop];
				for(unsigned int k=0; k< data->maxStatesPerPop; ++k){
					forwardLocal[s][i][j][k]=new double**[data->nRefpop];
					for(int l=0; l<data->nRefpop; ++l){
						forwardLocal[s][i][j][k][l]=new double*[data->nRefpop];
						for(int m=0; m<data->nRefpop; ++m){
							forwardLocal[s][i][j][k][l][m]=new double[ data->maxStatesPerPop];
						}
					}
				}
			}
		}
	}
	//precomputed sums
	precomputedSums=new double***[data->nRefpop];
	precomputedSumsCopyPopIdentityFirst=new double***[data->nRefpop];
	precomputedSumsCopyPopIdentitySecond=new double***[data->nRefpop];
	for(int i=0; i<data->nRefpop; ++i){
		precomputedSums[i]=new double**[data->nRefpop];
		precomputedSumsCopyPopIdentityFirst[i]=new double**[data->nRefpop];
		precomputedSumsCopyPopIdentitySecond[i]=new double**[data->nRefpop];
		for(int j=0; j<data->nRefpop; ++j){
			precomputedSums[i][j]=new double*[data->nRefpop];
			for(int k=0; k<data->nRefpop; ++k){
				precomputedSums[i][j][k]=new double[data->nRefpop];
			}
			precomputedSumsCopyPopIdentityFirst[i][j]=new double*[data->maxStatesPerPop];
			precomputedSumsCopyPopIdentitySecond[i][j]=new double*[data->maxStatesPerPop];
			for(unsigned int k=0; k<data->maxStatesPerPop; ++k){
				precomputedSumsCopyPopIdentityFirst[i][j][k]=new double[data->nRefpop];
				precomputedSumsCopyPopIdentitySecond[i][j][k]=new double[data->nRefpop];
			}
		}
	}

	tempPrecomputedSums=new double*[data->nRefpop];
	for(int m=0; m<data->nRefpop; ++m){
		tempPrecomputedSums[m]=new double[data->nRefpop];
	}
	scalingTerm=new double[data->nsnps];
}
void forward::freeMemory(){
	//initialize storage for forward probabilities-> we have to store the WHOLE set, but do it only at some points
	for(long s=0; s<nSavePoints; ++s){
		for(int i=0; i<data->nRefpop; ++i){
			for(int j=0; j<data->nRefpop; ++j){
				for(unsigned int k=0; k<data->maxStatesPerPop; ++k){
					for(int l=0; l<data->nRefpop; ++l){
						for(int m=0; m<data->nRefpop; ++m){
							delete[] forwardSaved[s][i][j][k][l][m];
						}
						delete[] forwardSaved[s][i][j][k][l];
					}
					delete[] forwardSaved[s][i][j][k];
				}
				delete[] forwardSaved[s][i][j];
			}
			delete[] forwardSaved[s][i];
		}
		delete[] forwardSaved[s];
	}
	delete[] forwardSaved;

	//initialize local forward storage
	for(long s=0; s<(recomputeWindowSize-1); ++s){
		for(int i=0; i<data->nRefpop; ++i){
			for(int j=0; j<data->nRefpop; ++j){
				for(unsigned int k=0; k< data->maxStatesPerPop; ++k){
					for(int l=0; l<data->nRefpop; ++l){
						for(int m=0; m<data->nRefpop; ++m){
							delete[] forwardLocal[s][i][j][k][l][m];
						}
						delete[] forwardLocal[s][i][j][k][l];
					}
					delete[] forwardLocal[s][i][j][k];
				}
				delete[] forwardLocal[s][i][j];
			}
			delete[] forwardLocal[s][i];
		}
		delete[] forwardLocal[s];
	}
	delete[] forwardLocal;

	//precomputed sums
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nRefpop; ++k){
				delete[] precomputedSums[i][j][k];
			}
			delete[] precomputedSums[i][j];
			for(unsigned int k=0; k<data->maxStatesPerPop; ++k){
				delete[] precomputedSumsCopyPopIdentityFirst[i][j][k];
				delete[] precomputedSumsCopyPopIdentitySecond[i][j][k];
			}
			delete[] precomputedSumsCopyPopIdentityFirst[i][j];
			delete[] precomputedSumsCopyPopIdentitySecond[i][j];
		}
		delete[] precomputedSums[i];
		delete[] precomputedSumsCopyPopIdentityFirst[i];
		delete[] precomputedSumsCopyPopIdentitySecond[i];
	}
	delete[] precomputedSums;
	delete[] precomputedSumsCopyPopIdentityFirst;
	delete[] precomputedSumsCopyPopIdentitySecond;

	for(int m=0; m<data->nRefpop; ++m){
		delete[] tempPrecomputedSums[m];
	}
	delete[] tempPrecomputedSums;
	delete[] scalingTerm;
}

//---------------------------------------------------------------------------
void backward::allocateMemory(){
	hmmvar::allocateMemory();
	transProbTemp=new double****[data->nRefpop];
	for(int i=0; i<data->nRefpop; ++i){
		transProbTemp[i]=new double***[data->nRefpop];
		for(int j=0; j<data->nRefpop; ++j){
			transProbTemp[i][j]=new double**[data->nRefpop];
			for(int I=0; I<data->nRefpop; ++I){
				transProbTemp[i][j][I]=new double*[data->nRefpop];
				for(int J=0; J<data->nRefpop; ++J){
					transProbTemp[i][j][I][J]=new double[data->maxStatesPerPop];
				}
			}
		}
	}

	//precomputed sums
	//precomputedSums[refpop0][refpop1]
	//precomputedSumsCopyPopIdentity[refpop 0][copy pop 0][type 0][refpop 1][miscopy 1]
	precomputedSums=new double*[data->nRefpop];
	precomputedSumsCopyPopIdentityFirst=new double****[data->nRefpop];
	precomputedSumsCopyPopIdentitySecond=new double****[data->nRefpop];
	for(int i=0; i<data->nRefpop; ++i){
		precomputedSums[i]=new double[data->nRefpop];
		precomputedSumsCopyPopIdentityFirst[i]=new double***[data->nRefpop];
		precomputedSumsCopyPopIdentitySecond[i]=new double***[data->nRefpop];
		for(int j=0; j<data->nRefpop; ++j){
			precomputedSumsCopyPopIdentityFirst[i][j]=new double**[data->maxStatesPerPop];
			precomputedSumsCopyPopIdentitySecond[i][j]=new double**[data->maxStatesPerPop];
			for(unsigned int k=0; k<data->maxStatesPerPop; ++k){
				precomputedSumsCopyPopIdentityFirst[i][j][k]=new double*[data->nRefpop];
				precomputedSumsCopyPopIdentitySecond[i][j][k]=new double*[data->nRefpop];
				for(int m=0; m<data->nRefpop; ++m){
					precomputedSumsCopyPopIdentityFirst[i][j][k][m]=new double[2];
					precomputedSumsCopyPopIdentitySecond[i][j][k][m]=new double[2];
				}
			}
		}
	}
	//[refpop0 old][refpop1 old][ancestry switch 0][ancestry switch 1]
	tempPrecomputedSums=new double***[data->nRefpop];
	for(int i=0; i<data->nRefpop; ++i){
		tempPrecomputedSums[i]=new double**[data->nRefpop];
		for(int l=0; l<data->nRefpop; ++l){
			tempPrecomputedSums[i][l]=new double*[2];
			tempPrecomputedSums[i][l][0]=new double[2];
			tempPrecomputedSums[i][l][1]=new double[2];
		}
	}

	if(doComputeSwitchProb){
		switchingProbability=new double*[2];
		switchingProbability[0]=new double[data->nsnps];
		switchingProbability[0][0]=0;
		switchingProbability[1]=new double[data->nsnps];
		switchingProbability[1][0]=0;
	}
	numStateProbabilities=data->nRefpop*data->nRefpop;
	stateProbability=new double*[numStateProbabilities];
	for(int i=0; i<numStateProbabilities; ++i) stateProbability[i]=new double[data->nsnps];


	//alleleProbability=new double*[2];
	//alleleProbability[0]=new double[data->nsnps];
	//alleleProbability[1]=new double[data->nsnps];
}
void backward::freeMemory(){
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int I=0; I<data->nRefpop; ++I){
				for(int J=0; J<data->nRefpop; ++J){
					delete[] transProbTemp[i][j][I][J];
				}
				delete[] transProbTemp[i][j][I];
			}
			delete[] transProbTemp[i][j];
		}
		delete[] transProbTemp[i];
	}
	delete[] transProbTemp;

	//precomputed sums
	//precomputedSums[refpop0][refpop1]
	//precomputedSumsCopyPopIdentity[refpop 0][copy pop 0][type 0][refpop 1][miscopy 1]
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(unsigned int k=0; k<data->maxStatesPerPop; ++k){
				for(int m=0; m<data->nRefpop; ++m){
					delete[] precomputedSumsCopyPopIdentityFirst[i][j][k][m];
					delete[] precomputedSumsCopyPopIdentitySecond[i][j][k][m];
				}
				delete[] precomputedSumsCopyPopIdentityFirst[i][j][k];
				delete[] precomputedSumsCopyPopIdentitySecond[i][j][k];
			}
			delete[] precomputedSumsCopyPopIdentityFirst[i][j];
			delete[] precomputedSumsCopyPopIdentitySecond[i][j];
		}
		delete[] precomputedSums[i];
		delete[] precomputedSumsCopyPopIdentityFirst[i];
		delete[] precomputedSumsCopyPopIdentitySecond[i];
	}
	delete[] precomputedSums;
	delete[] precomputedSumsCopyPopIdentityFirst;
	delete[] precomputedSumsCopyPopIdentitySecond;

	//[refpop0 old][refpop1 old][ancestry switch 0][ancestry switch 1]
	for(int i=0; i<data->nRefpop; ++i){
		for(int l=0; l<data->nRefpop; ++l){
			delete[] tempPrecomputedSums[i][l][0];
			delete[] tempPrecomputedSums[i][l][1];
			delete[] tempPrecomputedSums[i][l];
		}
		delete[] tempPrecomputedSums[i];
	}
	delete[] tempPrecomputedSums;

	if(doComputeSwitchProb){
		delete[] switchingProbability[0];
		delete[] switchingProbability[1];
		delete[] switchingProbability;
	}


	for(int i=0; i<numStateProbabilities; ++i) delete[] stateProbability[i];
	delete[] stateProbability;

	//delete[] alleleProbability[0];
	//delete[] alleleProbability[1];
	//delete[] alleleProbability;
}
//---------------------------------------------------------------------------
double hmmvar::getHaplotypeEmissionprob(int &i, int &j, int &k, bool haplotype, long &snp){
	if(i==j){
		if(data->refpopCollapsed[i][snp][k]==haplotype) return pars->oneMinusMutation[i];
		else return pars->mutation[i];
	} else {
		if(data->refpopCollapsed[j][snp][k]==haplotype) return pars->oneMinusMiscopyMutation;
		else return pars->miscopyMutation;
	}
}
//---------------------------------------------------------------------------
double hmmvar::getEmissionprob2(int i, int j, int k, int l, int m, int n, long &snp){
	return getEmissionprob(i,j,k,l,m,n,snp);
}
double hmmvar::getEmissionprob(int &i, int &j, int &k, int &l, int &m, int &n, long &snp){
	//return the emission probability.
	//indices are as in the diploid case
	switch(data->getGenotypeAtSnp(snp)){
		case 0: return getHaplotypeEmissionprob(i,j,k,0,snp)*getHaplotypeEmissionprob(l,m,n,0,snp); break;
		case 1: return getHaplotypeEmissionprob(i,j,k,1,snp)*getHaplotypeEmissionprob(l,m,n,0,snp) + getHaplotypeEmissionprob(i,j,k,0,snp)*getHaplotypeEmissionprob(l,m,n,1,snp); break;
		case 2: return getHaplotypeEmissionprob(i,j,k,1,snp)*getHaplotypeEmissionprob(l,m,n,1,snp); break;
		case 9: return 1; break;
		default: return 0;
	}
}
//---------------------------------------------------------------------------
void forward::precomputeSums(long snp, long previousSNP, double****** storageArray){
	//some sums in the recursion of the forward/backward variable have to be computed many times
	//here we precompute and store them.
	//this is possible since the transition probabilities are the same for many comparisons.
	//We thus compute the sum of all state probabilities, and then multiply it by the transition probability (see forwardRecursion / backwardRecursion)

	//-> each combinations of the four major haplotypic types (0,1,2,4). Types 3 and 5 are as 2 and 4 except to one term.
	//stored as follows: [ancestry change1][ancestry change2][miscopying 1][miscopying 2][refpop][refpop2]

	//First precompute the sums of the additional terms in the case of copy pop identity -> they can be used for the other sums
	//In our notes, these sums are named Q and R for the forward variable
	//-> storageArray points to the previous SNP

	//type switch probabilities
	for(int J=0; J<data->nRefpop; ++J){
		for(int K=0; K<data->nTypes[J][snp]; ++K){
			typeSwitchProbability[J][K]=(double)data->typeCounts[J][snp][K]/ (double)data->nref[J];
			for(int k=0; k<data->nTypes[J][previousSNP]; ++k){
				copyPopIdentityTypeSwitchProbability[J][k][K]=(double)data->typeSwitches[J][snp][k][K] /(double)data->typeCounts[J][previousSNP][k];
			}
		}
	}

	for(int I=0; I<data->nRefpop; ++I){
		for(int J=0; J<data->nRefpop; ++J){
			for(int K=0; K< data->nTypes[J][previousSNP]; ++K){ //for each type within the current copy population
				for(int L=0; L<data->nRefpop; ++L){
					precomputedSumsCopyPopIdentityFirst[I][J][K][L]=0;
					precomputedSumsCopyPopIdentitySecond[I][J][K][L]=0; //we switch indices
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][previousSNP]; ++n){
							precomputedSumsCopyPopIdentityFirst[I][J][K][L]+=storageArray[I][J][K][L][m][n];
							precomputedSumsCopyPopIdentitySecond[I][J][K][L]+=storageArray[L][m][n][I][J][K]; //switch indeces to get other haplotype
						}
					}
				}
			}
		}
	}

	//compute the sum over all previous states with the same ancestry using the sums computed above.
	//In our notes, these sums are denoted by S and H for the forward and backward variable, respectively.
	for(int i=0; i<data->nRefpop; ++i){
		for(int l=0; l<data->nRefpop; ++l){
			tempPrecomputedSums[i][l]=0;
			for(int j=0; j<data->nRefpop; ++j){
				for(int k=0; k< data->nTypes[j][previousSNP]; ++k){
					tempPrecomputedSums[i][l]+=precomputedSumsCopyPopIdentityFirst[i][j][k][l];
				}
			}
		}
	}
	//now compute the sums for the 4 major types but without the collapsing-type specific term
	//In our notes, these sums are denoted by Z and Y for the forward and backward variable, respectively.
	//to populations....
	bool miscopy0, miscopy1;
	double temp, temp2;
	for(int I=0; I<data->nRefpop; ++I){
		for(int J=0; J<data->nRefpop; ++J){
			if(I==J) miscopy0=false;
			else miscopy0=true;
			for(int L=0; L<data->nRefpop; ++L){
				for(int M=0; M<data->nRefpop; ++M){
					if(L==M) miscopy1=false;
					else miscopy1=true;
					temp=0;
					for(int l=0; l<data->nRefpop; ++l){
						if(l!=L) temp+=tempPrecomputedSums[I][l];
					}
					precomputedSums[I][J][L][M]=transProbHaplotypes[0][miscopy0][I]*(transProbHaplotypes[0][miscopy1][L]*tempPrecomputedSums[I][L]+transProbHaplotypes[1][miscopy1][L]*temp);
					temp=0;
					temp2=0;
					for(int i=0; i<data->nRefpop; ++i){
						if(i!=I){
							temp+=tempPrecomputedSums[i][L];
							for(int l=0; l<data->nRefpop; ++l){
								if(l!=L){
									temp2+=tempPrecomputedSums[i][l];
								}
							}
						}
					}
					precomputedSums[I][J][L][M]+=transProbHaplotypes[1][miscopy0][I]*(transProbHaplotypes[0][miscopy1][L]*temp + transProbHaplotypes[1][miscopy1][L]*temp2);
				}
			}
		}
	}
}
void backward::precomputeSums(long snp, long previousSNP, double****** storageArray){
	//some sums in the recursion of the forward/backward variable have to be computed many times
	//here we precompute and store them.
	//this is possible since the transition probabilities are the same for many comparisons.
	//We thus compute the sum of all state probabilities, and then multiply it by the transition probability (see forwardRecursion / backwardRecursion)

	//First precompute the sums of the additional terms in the case of copy pop identity -> they can be used for the other sums
	//In our notes, these sums are named F and G for the backward variable
	//-> storageArray points to the previous SNP
	double temp0, temp1;
	bool miscopy;

	//type switch probabilities
	for(int J=0; J<data->nRefpop; ++J){
		for(int k=0; k<data->nTypes[J][previousSNP]; ++k){
			typeSwitchProbability[J][k]=(double)data->typeCounts[J][previousSNP][k]/ (double)data->nref[J];
			for(int K=0; K<data->nTypes[J][snp]; ++K){
				copyPopIdentityTypeSwitchProbability[J][K][k]=(double)data->typeSwitches[J][previousSNP][K][k] /(double)data->typeCounts[J][snp][K];
			}
		}
	}

	for(int I=0; I<data->nRefpop; ++I){
		for(int J=0; J<data->nRefpop; ++J){
			for(int K=0; K< data->nTypes[J][previousSNP]; ++K){ //for each type within the current copy population
				for(int L=0; L<data->nRefpop; ++L){
					precomputedSumsCopyPopIdentityFirst[I][J][K][L][0]=0;
					precomputedSumsCopyPopIdentityFirst[I][J][K][L][1]=0;
					precomputedSumsCopyPopIdentitySecond[I][J][K][L][0]=0; //we switch indices
					precomputedSumsCopyPopIdentitySecond[I][J][K][L][1]=0; //we switch indices
					for(int m=0; m<data->nRefpop; ++m){
						temp0=0; temp1=0;
						if(m==L) miscopy=false; else miscopy=true;
						for(int n=0; n<data->nTypes[m][previousSNP]; ++n){
							temp0+=typeSwitchProbability[m][n]*storageArray[I][J][K][L][m][n]*getEmissionprob(I,J,K,L,m,n,previousSNP);
							temp1+=typeSwitchProbability[m][n]*storageArray[L][m][n][I][J][K]*getEmissionprob(L,m,n,I,J,K,previousSNP); //switch indices to get other haplotype
						}
						precomputedSumsCopyPopIdentityFirst[I][J][K][L][0]+=transProbHaplotypes[0][miscopy][L]*temp0;
						precomputedSumsCopyPopIdentityFirst[I][J][K][L][1]+=transProbHaplotypes[1][miscopy][L]*temp0;
						precomputedSumsCopyPopIdentitySecond[I][J][K][L][0]+=transProbHaplotypes[0][miscopy][L]*temp1;
						precomputedSumsCopyPopIdentitySecond[I][J][K][L][1]+=transProbHaplotypes[1][miscopy][L]*temp1;
					}
				}
			}
		}
	}


	//Now compute the sums only depending on i and l -> denoted by H in our notes
	for(int i=0; i<data->nRefpop; ++i){
		for(int l=0; l<data->nRefpop; ++l){
			tempPrecomputedSums[i][l][0][0]=0;
			tempPrecomputedSums[i][l][1][0]=0;
			tempPrecomputedSums[i][l][0][1]=0;
			tempPrecomputedSums[i][l][1][1]=0;
			for(int j=0; j<data->nRefpop; ++j){
				temp0=0; temp1=0;
				if(j==i) miscopy=false; else miscopy=true;
				for(int k=0; k< data->nTypes[j][previousSNP]; ++k){
					temp0+=typeSwitchProbability[j][k]*precomputedSumsCopyPopIdentityFirst[i][j][k][l][0];
					temp1+=typeSwitchProbability[j][k]*precomputedSumsCopyPopIdentityFirst[i][j][k][l][1];
				}
				tempPrecomputedSums[i][l][0][0]+=transProbHaplotypes[0][miscopy][i]*temp0;
				tempPrecomputedSums[i][l][1][0]+=transProbHaplotypes[1][miscopy][i]*temp0;
				tempPrecomputedSums[i][l][0][1]+=transProbHaplotypes[0][miscopy][i]*temp1;
				tempPrecomputedSums[i][l][1][1]+=transProbHaplotypes[1][miscopy][i]*temp1;
			}
		}
	}

	//Finally precompute the sums for a given I and L -> the sums of all H as denotyed by Y in our notes
	for(int I=0; I<data->nRefpop; ++I){
		for(int L=0; L<data->nRefpop; ++L){
			precomputedSums[I][L]=tempPrecomputedSums[I][L][0][0];
			for(int i=0; i<data->nRefpop; ++i){
				if(i!=L) precomputedSums[I][L]+=tempPrecomputedSums[I][i][0][1]; //Switch indices
				if(i!=I){
					precomputedSums[I][L]+=tempPrecomputedSums[i][L][1][0];
					for(int l=0; l<data->nRefpop; ++l){
						if(l!=L) precomputedSums[I][L]+=tempPrecomputedSums[i][l][1][1];
					}
				}
			}
		}
	}
}
//---------------------------------------------------------------------------
void hmmvar::computeTransitionMatrix(long &snp){
	//We compute the collapsing-type independent terms of the transition matrix for the four major types and the state identity terms
	//(see supplementary notes of Hapmix paper and our notes for a description and type numbers.)
	//the indices in the comments refer to the Hapmix paper FOR THE HAPLOTYPIC MODEL
	//NOTE: some terms are independent of the recombination rate -> these are precomputed in "setParameters"

	//we store according to [ancestry change][miscopying][refpop new]
	//In our notes, these variables are named xi_{ancestry change, miscopying}^{snp}(refpop)
	double noSwitch=exp(-data->rates[snp]*pars->timeSinceAdm);
	double withSwitch=(1-noSwitch);
	double temp;
	for(int i=0; i<data->nRefpop; ++i){
		//Type 0: different ancestry (i!=l), no miscopying (m==l)
		transProbHaplotypes[1][0][i]=withSwitch*pars->precomputedTransitionProbabilityTerms[0][i];
		//Type 1: different ancestry (i!=l) with miscopying (m!=l)
		transProbHaplotypes[1][1][i]=withSwitch*pars->precomputedTransitionProbabilityTerms[1][i];
		//Type 2: same ancestry (i==l), no miscopying (m==l) but obligate ancestral recombination (j!=m)
		temp=exp(-data->rates[snp]*pars->ancestralRate[i]);
		transProbHaplotypes[0][0][i]=noSwitch*(1-temp)*pars->miscopyRate[0]+withSwitch*pars->precomputedTransitionProbabilityTerms[0][i];
		//Type 4: same ancestry (i==l) with miscopying (m!=l) and obligate ancestral recombination (j!=m)
		transProbHaplotypes[0][1][i]=noSwitch*(1-temp)*pars->miscopyRate[1]+withSwitch*pars->precomputedTransitionProbabilityTerms[1][i];

		//The types 3 and 5 are special states of the types 2 and 4. We just save the additional term!
		//Type 3: state identity without miscopying (i==l && m==l && j==m) -> almost as Type 2
		//Type 5: state identity with miscopying (i==l && m!=l && j==m) -> almost as Type 4
		//This is variable phi in our notes
		transProbCopyPopIdentityTerm[i] = noSwitch * temp;
	}
}
//---------------------------------------------------------------------------
void forward::initialize(double****** storageArray){
	bool miscopy1, miscopy2;
	long snp=0;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			if(i==j) miscopy1=false;
			else miscopy1=true;
			for(int l=0; l<data->nRefpop; ++l){
				for(int m=0; m<data->nRefpop; ++m){
					if(l==m) miscopy2=false;
					else miscopy2=true;
					for(int k=0; k<data->nTypes[j][0]; ++k){
						for(int n=0; n<data->nTypes[m][0]; ++n){
							storageArray[i][j][k][l][m][n]=pars->globalAncestry[i]*pars->miscopyRate[miscopy1]*(double)data->typeCounts[j][0][k]/(double)data->nref[j] * pars->globalAncestry[l]*pars->miscopyRate[miscopy2]*(double)data->typeCounts[m][0][n]/(double)data->nref[m] * getEmissionprob(i,j,k,l,m,n,snp);
						}
					}
				}
			}
		}
	}
	scalingTerm[0]=1;
	firstRecomputedSNP=data->nsnps;
}
//---------------------------------------------------------------------------
void backward::initialize(double****** storageArray){
	long lastSnp=data->nsnps-1;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][lastSnp]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][lastSnp]; ++n){
							storageArray[i][j][k][l][m][n]=1;
						}
					}
				}
			}
		}
	}
	scalingTerm=1;
}
//---------------------------------------------------------------------------
//used to debug only

void backward::inductionSLOW(int &I, int &J, int &K, int &L, int &M, int &N, long &snp, long &oldSnp, double****** storageArrayOld, double****** storageArrayNew){
	double temp0, temp1;
	storageArrayNew[I][J][K][L][M][N]=0;
	bool miscopy;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][oldSnp]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][oldSnp]; ++n){
							//get transmission probabilities....
							if(i==j) miscopy=false; else miscopy=true;
							if(i==I){
								temp0=transProbHaplotypes[0][miscopy][i]*(double)data->typeCounts[j][oldSnp][k]/(double)data->nref[j];
								if(j==J){
									temp0+=transProbCopyPopIdentityTerm[i]*(double)data->typeSwitches[j][oldSnp][K][k]/(double)data->typeCounts[J][snp][K];
								}
							} else {
								temp0=transProbHaplotypes[1][miscopy][i]*(double)data->typeCounts[j][oldSnp][k]/(double)data->nref[j];
							}
							//and second...
							if(l==m) miscopy=false; else miscopy=true;
							if(l==L){
								temp1=transProbHaplotypes[0][miscopy][l]*(double)data->typeCounts[m][oldSnp][n]/(double)data->nref[m];
								if(m==M){
									temp1+=transProbCopyPopIdentityTerm[l]*(double)data->typeSwitches[m][oldSnp][N][n]/(double)data->typeCounts[M][snp][N];
								}
							} else {
								temp1=transProbHaplotypes[1][miscopy][l]*(double)data->typeCounts[m][oldSnp][n]/(double)data->nref[m];
							}
							storageArrayNew[I][J][K][L][M][N]+=temp0*temp1*storageArrayOld[i][j][k][l][m][n]*getEmissionprob(i,j,k,l,m,n,oldSnp);
						}
					}
				}
			}
		}
	}
	//multiply by arbitrary scaling term
	storageArrayNew[I][J][K][L][M][N]*=scalingTerm;
}


void forward::inductionSLOW(int &I, int &J, int &K, int &L, int &M, int &N, long &snp, long &oldSnp, double****** storageArrayOld, double****** storageArrayNew){
	double temp0, temp1;
	storageArrayNew[I][J][K][L][M][N]=0;
	bool miscopy;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][oldSnp]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][oldSnp]; ++n){
							//get transmission probabilities....
							if(I==J) miscopy=false; else miscopy=true;
							if(i==I){
								temp0=transProbHaplotypes[0][miscopy][I]*(double)data->typeCounts[J][snp][K]/ (double)data->nref[J];
								if(j==J){
									temp0+=transProbCopyPopIdentityTerm[I]*(double)data->typeSwitches[J][snp][k][K]/(double)data->typeCounts[j][oldSnp][k];
								}
							} else {
								temp0=transProbHaplotypes[1][miscopy][I]*(double)data->typeCounts[J][snp][K]/ (double)data->nref[J];
							}
							//and second...
							if(L==M) miscopy=false; else miscopy=true;
							if(l==L){
								temp1=transProbHaplotypes[0][miscopy][L]*(double)data->typeCounts[M][oldSnp][N]/ (double)data->nref[M];
								if(m==M){
									temp1+=transProbCopyPopIdentityTerm[L]*(double)data->typeSwitches[M][snp][n][N]/(double)data->typeCounts[m][oldSnp][n];
								}
							} else {
								temp1=transProbHaplotypes[1][miscopy][L]*(double)data->typeCounts[M][snp][N]/ (double)data->nref[M];
							}
							storageArrayNew[I][J][K][L][M][N]+=temp0*temp1*storageArrayOld[i][j][k][l][m][n];
						}
					}
				}
			}
		}
	}
	storageArrayNew[I][J][K][L][M][N]*=scalingTerm[oldSnp]*getEmissionprob(I,J,K,L,M,N,snp);
}
//---------------------------------------------------------------------------

void forward::induction(int &I, int &J, int &K, int &L, int &M, int &N, long &snp, long &oldSnp, double****** storageArrayOld, double****** storageArrayNew){
	double temp1, temp2;
	//make use of precomputed sums
	storageArrayNew[I][J][K][L][M][N]=precomputedSums[I][J][L][M]* typeSwitchProbability[J][K]* typeSwitchProbability[M][N];
	//we missed some terms in the case of copy pop identity
	//there are three types: both haplotypes are in copy pop identity or either of them is.
	//first type: we should have calculated forward(t-1)*(a1+b1)*(a2+b2). But we only did forward(t-1)*a1*a2.
	//Thus we have to add forward(t-1)*(a0*b1+a1*b0+b0*b1)
	//However, the terms forward(t-1)*(a0*b1) and forward(t-1)*(a1*b0) will be added automatically when dealing with the other types!
	//So we add here forward(t-1)*(b0*b1)
	temp2=0;
	for(int k=0; k<data->nTypes[J][oldSnp]; ++k){ //for each type within the current=previous copy population
		temp1=0;
		for(int n=0; n<data->nTypes[M][oldSnp]; ++n){ //for each type within the current=previous copy population
			temp1+=storageArrayOld[I][J][k][L][M][n]*copyPopIdentityTypeSwitchProbability[M][n][N];
		}
		temp2+=temp1*copyPopIdentityTypeSwitchProbability[J][k][K];
	}
	storageArrayNew[I][J][K][L][M][N]+=transProbCopyPopIdentityTerm[I]*transProbCopyPopIdentityTerm[L]*temp2;
	//the other types involve summation over all possible states in the other population
	//for type two (copy pop identity for haplotype 0): we should have calculated forward(t-1)*(a0+b0)*(a1). But we only did forward(t-1)*a0*a1.
	//Thus we have to add forward(t-1)*b0*a1
	//But we missed this term for ALL possible transitions in haplotype1 (including the state identity in haplotype1 -> has not to be calculated in type 1)
	bool miscopy;
	if(L==M) miscopy=false;
	else miscopy=true;

	temp2=0;
	for(int l=0; l<data->nRefpop; ++l){
		temp1=0;
		for(int k=0; k<data->nTypes[J][oldSnp]; ++k){ //for each type within the current=previous copy population
			temp1+=precomputedSumsCopyPopIdentityFirst[I][J][k][l]*copyPopIdentityTypeSwitchProbability[J][k][K];
		}
		if(l==L) temp2+=temp1*transProbHaplotypes[0][miscopy][L];
		else temp2+=temp1*transProbHaplotypes[1][miscopy][L];
	}
	storageArrayNew[I][J][K][L][M][N]+=temp2 * transProbCopyPopIdentityTerm[I]* typeSwitchProbability[M][N];


	//the same for type 3
	if(I==J) miscopy=false;
	else miscopy=true;
	temp2=0;
	for(int i=0; i<data->nRefpop; ++i){
		temp1=0;
		for(int n=0; n<data->nTypes[M][oldSnp]; ++n){ //for each type within the current=previous copy population
			temp1+=precomputedSumsCopyPopIdentitySecond[L][M][n][i]*copyPopIdentityTypeSwitchProbability[M][n][N];
		}
		if(i==I) temp2+=temp1*transProbHaplotypes[0][miscopy][I];
		else temp2+=temp1*transProbHaplotypes[1][miscopy][I];
	}
	storageArrayNew[I][J][K][L][M][N]+=temp2 * transProbCopyPopIdentityTerm[L]*typeSwitchProbability[J][K];
	//multiply by arbitrary scaling term
	storageArrayNew[I][J][K][L][M][N]*=scalingTerm[oldSnp]*getEmissionprob(I,J,K,L,M,N,snp);
}

void backward::induction(int &I, int &J, int &K, int &L, int &M, int &N, long &snp, long &oldSnp, double****** storageArrayOld, double****** storageArrayNew){
	double temp1, temp2;
	//make use of precomputed sums
	storageArrayNew[I][J][K][L][M][N]=precomputedSums[I][L];
	//we missed some terms in the case of copy pop identity -> see notes
	temp2=0;
	for(int k=0; k<data->nTypes[J][oldSnp]; ++k){
		temp1=0;
		for(int n=0; n<data->nTypes[M][oldSnp]; ++n){
			temp1+=copyPopIdentityTypeSwitchProbability[M][N][n]*storageArrayOld[I][J][k][L][M][n]*getEmissionprob(I,J,k,L,M,n,oldSnp);
		}
		temp2+=copyPopIdentityTypeSwitchProbability[J][K][k]*temp1;
	}
	storageArrayNew[I][J][K][L][M][N]+=transProbCopyPopIdentityTerm[I]*transProbCopyPopIdentityTerm[L]*temp2;


	temp2=0;
	for(int k=0; k<data->nTypes[J][oldSnp]; ++k){ //for each type within the current=previous copy population
		temp1=0;
		for(int l=0; l<data->nRefpop; ++l){ //for each type within the current=previous copy population
			if(l!=L) temp1+=precomputedSumsCopyPopIdentityFirst[I][J][k][l][1];
		}
		temp2+=copyPopIdentityTypeSwitchProbability[J][K][k]*(precomputedSumsCopyPopIdentityFirst[I][J][k][L][0]+temp1);
	}
	storageArrayNew[I][J][K][L][M][N]+=transProbCopyPopIdentityTerm[I]*temp2;


	temp2=0;
	for(int n=0; n<data->nTypes[M][oldSnp]; ++n){ //for each type within the current=previous copy population
		temp1=0;
		for(int i=0; i<data->nRefpop; ++i){ //for each type within the current=previous copy population
			if(i!=I) temp1+=precomputedSumsCopyPopIdentitySecond[L][M][n][i][1];
		}
		temp2+=copyPopIdentityTypeSwitchProbability[M][N][n]*(precomputedSumsCopyPopIdentitySecond[L][M][n][I][0]+temp1);
	}
	storageArrayNew[I][J][K][L][M][N]+=transProbCopyPopIdentityTerm[L]*temp2;

	//multiply by arbitrary scaling term
	storageArrayNew[I][J][K][L][M][N]*=scalingTerm;
}
//---------------------------------------------------------------------------
void forward::initialRun(){
	clock_t start=clock();
	logfile->listFlush("Initial forward run ...");
	//some vars
	bool step=0;
	long savePoint=0;
	double****** next;
	double****** previous;
	//initialize -> the first SNP is stored -> write to storage directly!
	initialize(forwardSaved[savePoint]);
	//write(savePoint, forwardSaved[savePoint], "initial_");
	//compute first sum to get scaling term
	double firstSum=0;
	double sum=0;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][0]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][0]; ++n){
							firstSum+=forwardSaved[savePoint][i][j][k][l][m][n];
						}
					}
				}
			}
		}
	}
	next=forwardSaved[savePoint];
	//step through
	long previousSnp;
	for(long snp=1; snp<data->nsnps; ++snp){
		previousSnp=snp-1;
		sum=0;
		//set previous and next data storage -> use forwardSave if necessary
		previous=next;
		if(snp % recomputeWindowSize == 0){
			++savePoint;
			next=forwardSaved[savePoint];
		} else {
			next=tempStorage[step];
			step=1-step;
		}
		computeTransitionMatrix(snp);
		precomputeSums(snp, previousSnp, previous);
		for(int i=0; i<data->nRefpop; ++i){
			for(int j=0; j<data->nRefpop; ++j){
				for(int k=0; k<data->nTypes[j][snp]; ++k){
					for(int l=0; l<data->nRefpop; ++l){
						for(int m=0; m<data->nRefpop; ++m){
							for(int n=0; n<data->nTypes[m][snp]; ++n){
								induction(i,j,k,l,m,n,snp, previousSnp, previous, next);
								//inductionSLOW(i,j,k,l,m,n,snp, previousSnp, previous, next);
								sum+=next[i][j][k][l][m][n];
							}
						}
					}
				}
			}
		}
		//write(snp, next, "initial");
		//compute next scaling Term
		scalingTerm[snp]=firstSum/sum;
		if(snp%10 == 0) logfile->listOverFlush("Initial forward run ... SNP " +toString(snp) + "/" + toString(data->nsnps-1) +  " (" + toString ( (clock() - start)/CLOCKS_PER_SEC ) + "s)");
	}
	//compute likelihood of data....
	//likelihood is essentially the sum over all possible states at time T
	//this is exactly the sum computed and stored in "sum"
	//However, we need to take the scaling into account... -> multiply the sum by the product of all scaling terms (except the last...)
	//This has to be done in the log space...
	LL=log10(sum);
	for(long snp=1; (snp<data->nsnps-1); ++snp){
		LL-=log10(scalingTerm[snp]);
	}
	logfile->overList("Initial forward run ... done! (" + toString ( (clock() - start)/CLOCKS_PER_SEC ) + "s)         ");
	logfile->conclude("log10(likelihood) = " + toString(LL));
}
//---------------------------------------------------------------------------
double****** forward::getAtSNP(long &snp){
	//this function manages the local recomputation of the forward variable
	//check if the backward variable for this SNP is stored
	if((snp % recomputeWindowSize) == 0){
		return forwardSaved[snp/recomputeWindowSize];
	}
	else {
		//check if we have already recomputed...
		if(snp < firstRecomputedSNP){
			//we need to recompute locally...
			localRun(floor(snp/recomputeWindowSize)); //this function also updates lastRecomputedSNP
		}
		//get position within forwardLocal -> use updated lastRecomputedSNP
		return forwardLocal[snp-firstRecomputedSNP];
	}
}
//---------------------------------------------------------------------------
void forward::localRun(long savePoint){
	//get first snp to compute
	//note: scalingTerm already computed!
	long snp=savePoint*recomputeWindowSize+1;
	firstRecomputedSNP=snp; //the snp just after the last savepoint
	//initialize
	double****** next=forwardSaved[savePoint]; //will be switched in loop
	double****** previous;
	long oldSnp=snp-1;
	//write(oldSnp, next, "savepoint_");
	for(int step=0; step<(recomputeWindowSize-1) && snp<data->nsnps; ++step, ++snp){
		//set next and previous
		previous=next;
		next=forwardLocal[step];
		oldSnp=snp-1;
		computeTransitionMatrix(snp);
		precomputeSums(snp, snp-1, previous);
		for(int i=0; i<data->nRefpop; ++i){
			for(int j=0; j<data->nRefpop; ++j){
				for(int k=0; k<data->nTypes[j][snp]; ++k){
					for(int l=0; l<data->nRefpop; ++l){
						for(int m=0; m<data->nRefpop; ++m){
							for(int n=0; n<data->nTypes[m][snp]; ++n){
								induction(i,j,k,l,m,n,snp, oldSnp, previous, next);
							}
						}
					}
				}
			}
		}
		//write(snp, next, "local_");
	}
}
//---------------------------------------------------------------------------
void forward::write(long &snp, double****** storage, std::string tag){
	//used to Debug only...
	std::ofstream out;
	std::string filename="forward_" + tag + toString(snp) + ".txt";
	out.open(filename.c_str());
	out << "storage" << std::endl;
	out << "i" << "\t" << "j" << "\t" << "k" << "\t" << "l" << "\t" << "m" << "\t" << "n" << std::endl;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][snp]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][snp]; ++n){
							out << i << "\t" << j << "\t" << k << "\t" << l << "\t" << m << "\t" << n << "\t" << storage[i][j][k][l][m][n] << std::endl;
						}
					}
				}
			}
		}
	}
	/*
	out << "sums" << endl;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int l=0; l<data->nRefpop; ++l){
				for(int m=0; m<data->nRefpop; ++m){
					out << i << "\t" << j << "\t" <<  l << "\t" << m << "\t"  << precomputedSums[i][j][l][m] << endl;
				}
			}
		}
	}
	*/
	out << std::endl << "Xi" << std::endl;
	for(int i=0; i<data->nRefpop; ++i){
		out << i << "\t00\t" << transProbHaplotypes[0][0][i] << std::endl;
		out << i << "\t01\t" << transProbHaplotypes[0][1][i] << std::endl;
		out << i << "\t10\t" << transProbHaplotypes[1][0][i] << std::endl;
		out << i << "\t11\t" << transProbHaplotypes[1][1][i] << std::endl;
	}
	out << std::endl << "dist rs" << "\t" << data->rates[snp] << std::endl;

	out << "phi" << std::endl;
	for(int i=0; i<data->nRefpop; ++i){
		out << i << "\t" << transProbCopyPopIdentityTerm[i] << std::endl;
	}
	/*
	out << "precomputedSumsCopyPopIdentityFirst" << endl;
	for(int I=0; I<data->nRefpop; ++I){
		for(int J=0; J<data->nRefpop; ++J){
			for(int K=0; K< data->nTypes[J][snp]; ++K){ //for each type within the current copy population
				for(int L=0; L<data->nRefpop; ++L){
					out << I << "\t" << J << "\t" << K << "\t" << L << "\t" << precomputedSumsCopyPopIdentityFirst[I][J][K][L] << endl;
				}
			}
		}
	}
	*/
	out << std::endl << "emission" << std::endl;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][snp]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][snp]; ++n){
							out << i << "\t" << j << "\t" << k << "\t" << l << "\t" << m << "\t" << n << "\t" << getEmissionprob(i,j,k,l,m,n,snp) << std::endl;
						}
					}
				}
			}
		}
	}


	out.close();
}
//---------------------------------------------------------------------------
void backward::runAndComputeSwitchingProbabilities(forward* myForward){
	clock_t start=clock();
	std::string tag;
	if(doComputeSwitchProb) tag="backward run and computation of switching probabilities ...";
	else tag="backward run ...";
	logfile->listFlush(tag);
	//some vars
	bool step=0;
	long previousSnp=data->nsnps-1;
	double sumToNormalize;
	double****** forwardT;

	//initialize
	double****** next=tempStorage[step]; //will be switched in loop
	double****** previous;
	initialize(next);
	//compute first sum to get scaling term
	double firstSum=0;
	double sum;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][0]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][0]; ++n){
							firstSum+=next[i][j][k][l][m][n];
						}
					}
				}
			}
		}
	}

	//step through
	for(long snp=data->nsnps-2; snp>=0; --snp){
		step=1-step;
		sum=0;
		previous=next;
		next=tempStorage[step];
		computeTransitionMatrix(previousSnp);
		precomputeSums(snp, previousSnp, previous);
		for(int i=0; i<data->nRefpop; ++i){
			for(int j=0; j<data->nRefpop; ++j){
				for(int k=0; k<data->nTypes[j][snp]; ++k){
					for(int l=0; l<data->nRefpop; ++l){
						for(int m=0; m<data->nRefpop; ++m){
							for(int n=0; n<data->nTypes[m][snp]; ++n){
								induction(i,j,k,l,m,n,snp, previousSnp, previous, next);
								//inductionSLOW(i,j,k,l,m,n,snp, previousSnp, previous, next);
								sum+=next[i][j][k][l][m][n];
							}
						}
					}
				}
			}
		}
		//write(snp, next);
		//compute switching probability for this snp
		forwardT=myForward->getAtSNP(snp);
		sumToNormalize=computeNormalizingSum(snp, forwardT, next);
		computeStateProbabilities(snp, sumToNormalize, forwardT, next);
		//computeAncestralAlleleProbabilities(snp, sumToNormalize, forwardT, next);
		if(doComputeSwitchProb) computeSwitchingProbabilities(snp, sumToNormalize, forwardT, next, previous);
		previousSnp=snp;
		scalingTerm=firstSum/sum;
		if(snp%10==0) logfile->listOverFlush(tag + " SNP " + toString(snp) + "/" + toString(data->nsnps-1) +  " (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)");
	}
	logfile->overList(tag + " done! (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)              ");
}
//---------------------------------------------------------------------------
double backward::computeNormalizingSum(long &snp, double****** forwardT, double****** backwardT){
	//computing normalizing sum used to get state and switching probabilities
	double sumToNormalize=0;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][snp]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][snp]; ++n){
							sumToNormalize+=forwardT[i][j][k][l][m][n] //forward at t
						                   *backwardT[i][j][k][l][m][n]; //backward at t
						}
					}
				}
			}
		}
	}
	return sumToNormalize;
}
/*
void backward::computeStateProbabilities(long &snp, double &sumToNormalize, double****** forwardT, double****** backwardT){
	//compute state probabilities as in hapmix
	//-> probability to have 0,1 or 2 copies from population 0
	stateProbability[0][snp]=0;
	stateProbability[1][snp]=0;
	stateProbability[2][snp]=0;

	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][snp]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][snp]; ++n){
							if(i==0){
								if(l==0) stateProbability[2][snp]+=forwardT[i][j][k][l][m][n]*backwardT[i][j][k][l][m][n];
								else stateProbability[1][snp]+=forwardT[i][j][k][l][m][n]*backwardT[i][j][k][l][m][n];
							} else {
								if(l==0) stateProbability[1][snp]+=forwardT[i][j][k][l][m][n]*backwardT[i][j][k][l][m][n];
								else stateProbability[0][snp]+=forwardT[i][j][k][l][m][n]*backwardT[i][j][k][l][m][n];
							}
						}
					}
				}
			}
		}
	}
	stateProbability[0][snp]/=sumToNormalize;
	stateProbability[1][snp]/=sumToNormalize;
	stateProbability[2][snp]/=sumToNormalize;
}
*/
void backward::computeStateProbabilities(long &snp, double &sumToNormalize, double****** forwardT, double****** backwardT){
	//compute state probabilities as in hapmix, but extended to n populations
	//we have thus to enumerate all states
	for(int i=0; i<numStateProbabilities; ++i) stateProbability[i][snp]=0;
	int id=0;

	for(int i=0; i<data->nRefpop; ++i){
		for(int l=0; l<data->nRefpop; ++l){
			id=data->nRefpop * i + l;
			for(int j=0; j<data->nRefpop; ++j){
				for(int k=0; k<data->nTypes[j][snp]; ++k){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][snp]; ++n){
							stateProbability[id][snp]+=forwardT[i][j][k][l][m][n]*backwardT[i][j][k][l][m][n];
						}
					}
				}
			}
		}
	}
	for(int i=0; i<numStateProbabilities; ++i) stateProbability[i][snp]/=sumToNormalize;
}

/*
void backward::computeAncestralAlleleProbabilities(long &snp, double &sumToNormalize, double****** forwardT, double****** backwardT){
	//also compute the ancestral allele probabilities
	//-> probability that the ancestral allele was 0
	alleleProbability[0][snp]=0;
	alleleProbability[1][snp]=0;

	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][snp]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][snp]; ++n){
							if(data->refpopCollapsed[j][snp][k]==1) alleleProbability[0][snp]+=forwardT[i][j][k][l][m][n]*backwardT[i][j][k][l][m][n];
							if(data->refpopCollapsed[m][snp][n]==1) alleleProbability[1][snp]+=forwardT[i][j][k][l][m][n]*backwardT[i][j][k][l][m][n];
						}
					}
				}
			}
		}
	}
	alleleProbability[0][snp]/=sumToNormalize;
	alleleProbability[1][snp]/=sumToNormalize;
}
*/

void backward::computeSwitchingProbabilities(long &snp, double &sumToNormalize, double****** forwardT, double****** backwardT, double****** backwardTplusOne){
	//see our notes for a detailed description of the algorithm
	bool ancestryChange, miscopy;
	long nextSnp=snp+1;
	//precompute transition probabilities WITHOUT copy pop identity term
	//[from pop][from copy pop][to pop][to copypop][to type]
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int I=0; I<data->nRefpop; ++I){
				if(i!=I) ancestryChange=true;
				else ancestryChange=false;
				for(int J=0; J<data->nRefpop; ++J){
					if(I==J) miscopy=false;
					else miscopy=true;
					for(int K=0; K<data->nTypes[J][nextSnp]; ++K){
						transProbTemp[i][j][I][J][K]=transProbHaplotypes[ancestryChange][miscopy][I]* (double) data->typeCounts[J][nextSnp][K]/(double)data->nref[J];
					}
				}
			}
		}
	}

	//We have to loop over all states at t and all states at t+1 for which the ancestry is the same
	//And this seperately for each haplotype -> just loop once and switch indices to get it
	//We compute backward(t+1)*emission(t+1)*transition*forward(t)
	//The product of emission*transition does not depend on the type we are in t (it does not depend on k and n)
	//Therefore, we loop over these indices at the end and can precompute some terms

	//for each current state (small letters)
	double sumHaplo0=0;
	double sumHaplo1=0;
	double sumHaplo0temp=0;
	double sumHaplo1temp=0;
	double temp, temp0, temp1;
	double transProbNoCopyPopIdentity;
	double* copypopIdentityTemp=new double[data->maxStatesPerPop];
	for(int i=0; i<data->nRefpop; ++i){ //for the ancestry, which is the same for both states
		//for each following state given that the ancestry matches (i==I) (capital letters)
		for(int J=0; J<data->nRefpop; ++J){
			for(int K=0; K<data->nTypes[J][nextSnp]; ++K){
				for(int L=0; L<data->nRefpop; ++L){
					for(int M=0; M<data->nRefpop; ++M){
						for(int N=0; N<data->nTypes[M][nextSnp]; ++N){
							//for all current states (but same ancestry)
							sumHaplo0temp=0;
							sumHaplo1temp=0;
							for(int j=0; j<data->nRefpop; ++j){
								for(int l=0; l<data->nRefpop; ++l){
									for(int m=0; m<data->nRefpop; ++m){
										transProbNoCopyPopIdentity=transProbTemp[i][j][i][J][K]*transProbTemp[l][m][L][M][N];
										//for all types in the current states -> they do not influence the transition and emission probabilities -> allows some terms to be precomputed
										//check for the additional terms in case of copypop identity -> make seperate sums to speed up
										if(j==J){ //we now that i==I
											if(l==L && M==m){
												//both have copy pop identity -> precompute!
												for(int n=0; n<data->nTypes[m][snp]; ++n){
													//copypopIdentityTemp[n]=transProbCopyPopIdentityTerm[l]*(double)data->typeSwitches[m][nextSnp][n][N]/(double)data->typeCounts[m][snp][n];
													copypopIdentityTemp[n]=transProbCopyPopIdentityTerm[l]*copyPopIdentityTypeSwitchProbability[m][n][N];
												}
												for(int k=0; k<data->nTypes[j][snp]; ++k){
													//temp0=transProbCopyPopIdentityTerm[i]*(double)data->typeSwitches[j][nextSnp][k][K]/(double)data->typeCounts[j][snp][k];
													temp0=transProbCopyPopIdentityTerm[i]*copyPopIdentityTypeSwitchProbability[j][k][K];
													for(int n=0; n<data->nTypes[m][snp]; ++n){
														temp=transProbNoCopyPopIdentity+transProbTemp[i][j][i][J][K]*copypopIdentityTemp[n]+transProbTemp[l][m][L][M][N]*temp0+temp0*copypopIdentityTemp[n];
														sumHaplo0temp+=forwardT[i][j][k][l][m][n]*temp; //forward at t
														//by switching the indices we get the same thing for the other haplotype
														sumHaplo1temp+=forwardT[l][m][n][i][j][k]*temp; //forward at t
													}
												}
											} else {
												//only first has
												for(int k=0; k<data->nTypes[j][snp]; ++k){
													//temp=transProbNoCopyPopIdentity+transProbTemp[l][m][L][M][N]*transProbCopyPopIdentityTerm[i]*(double)data->typeSwitches[J][nextSnp][k][K]/(double)data->typeCounts[j][snp][k];
													temp=transProbNoCopyPopIdentity+transProbTemp[l][m][L][M][N]*transProbCopyPopIdentityTerm[i]*copyPopIdentityTypeSwitchProbability[j][k][K];
													temp0=0; temp1=0;
													for(int n=0; n<data->nTypes[m][snp]; ++n){
														temp0+=forwardT[i][j][k][l][m][n]; //forward at t
														//by switching the indices we get the same thing for the other haplotype
														temp1+=forwardT[l][m][n][i][j][k]; //forward at t
													}
													sumHaplo0temp+=temp*temp0;
													sumHaplo1temp+=temp*temp1;
												}
											}
										} else {
											if(l==L && M==m){
												//only second has
												for(int n=0; n<data->nTypes[m][snp]; ++n){
													//temp=transProbNoCopyPopIdentity+transProbTemp[i][j][i][J][K]*transProbCopyPopIdentityTerm[l]*(double)data->typeSwitches[M][nextSnp][n][N]/(double)data->typeCounts[m][snp][n];
													temp=transProbNoCopyPopIdentity+transProbTemp[i][j][i][J][K]*transProbCopyPopIdentityTerm[l]*copyPopIdentityTypeSwitchProbability[m][n][N];
													temp0=0; temp1=0;
													for(int k=0; k<data->nTypes[j][snp]; ++k){
														temp0+=forwardT[i][j][k][l][m][n]; //forward at t
														//by switching the indices we get the same thing for the other haplotype
														temp1+=forwardT[l][m][n][i][j][k]; //forward at t
													}
													sumHaplo0temp+=temp*temp0;
													sumHaplo1temp+=temp*temp1;
												}
											} else {
												//none has
												temp0=0; temp1=0;
												for(int k=0; k<data->nTypes[j][snp]; ++k){
													for(int n=0; n<data->nTypes[m][snp]; ++n){
														temp0+=forwardT[i][j][k][l][m][n]; //forward at t
														//by switching the indices we get the same thing for the other haplotype
														temp1+=forwardT[l][m][n][i][j][k]; //forward at t
													}
												}
												sumHaplo0temp+=transProbNoCopyPopIdentity*temp0;
												sumHaplo1temp+=transProbNoCopyPopIdentity*temp1;
											}
										}
									}
								}
							} // end j,k,l,m,n loop
							sumHaplo0+=sumHaplo0temp*backwardTplusOne[i][J][K][L][M][N]*getEmissionprob(i,J,K,L,M,N,nextSnp);
							sumHaplo1+=sumHaplo1temp*backwardTplusOne[L][M][N][i][J][K]*getEmissionprob(L,M,N,i,J,K,nextSnp);
						}
					}
				}
			}
		}
	}
	delete[] copypopIdentityTemp;
	//save
	switchingProbability[0][nextSnp]=1-scalingTerm*(sumHaplo0/sumToNormalize);
	switchingProbability[1][nextSnp]=1-scalingTerm*(sumHaplo1/sumToNormalize);
	//set to zero if below zero (happens due to rounding since we are working with doubles:
	if(switchingProbability[0][nextSnp]<0) switchingProbability[0][nextSnp]=0;
	if(switchingProbability[1][nextSnp]<0) switchingProbability[1][nextSnp]=0;
}
//---------------------------------------------------------------------------
void backward::writeProbabilities(std::ofstream* out, int ind, snpDB* mySnpDB){
	clock_t start=clock();
	logfile->listFlush("Writing switching probabilities ...");
	char a1, a2;
	double genPos=0;
	int id;
	*out << "snp\tsnpPosition\tgeneticPosition[cM]\tallele1\tallele2\tadmixedGenotype";
	if(doComputeSwitchProb) *out << "\tswitchProbHaplo1\tswitchProbHaplo2";
	for(int i=1; i<=data->nRefpop; ++i){
		for(int l=i; l<=data->nRefpop; ++l){
			*out << "\t" << "probAncestry(" << i << "," << l << ")";
		}
	}
	*out << std::endl;
	//*out << "\tprob0copiesOfPop0\t\tprob1copyOfPop0\tprob2copiesOfPop0"
	//*out << "\tprobHaplo0CarriersAllele1\tprobHaplo1CarriersAllele1" << endl;

	std::map<long,long>::iterator snpIt=data->snpMap->begin();
	for(long snp=0; snp<(data->nsnps-1); ++snp, ++snpIt){
		*out << std::fixed << std::setprecision(5)
			 << mySnpDB->getName(snpIt->second) << "\t"
			 << snpIt->first << "\t";
			 genPos+=100*data->rates[snp];
	    *out << genPos << "\t";
			 a1=mySnpDB->getAllele1(snpIt->second);
			 a2=mySnpDB->getAllele2(snpIt->second);
		*out << a1 << "\t" << a2 << "\t";
		if(data->admpop[snp][ind]==9) *out << "?" << "\t";
		else {
			if(data->admpop[snp][ind]>0){
				*out << a1 << "/";
				if(data->admpop[snp][ind]==2) *out << a1 << "\t";
				else *out << a2 << "\t";
			} else *out << a2 << "/" << a2 << "\t";
		}

		if(doComputeSwitchProb){
			*out << switchingProbability[0][snp] << "\t"
				 << switchingProbability[1][snp];
		}

		for(int i=0; i<data->nRefpop; ++i){
			for(int l=i; l<data->nRefpop; ++l){
				id=data->nRefpop * i + l;
				if(i==l) *out << "\t" << stateProbability[id][snp];
				else *out << "\t" << stateProbability[id][snp] + stateProbability[id][snp];
			}
		}

			 //<< stateProbability[0][snp] << "\t"
			 //<< stateProbability[1][snp] << "\t"
			 //<< stateProbability[2][snp] << "\t"
		//*out << "\t" << alleleProbability[0][snp]	 << "\t" << alleleProbability[1][snp] << endl;
		*out << std::endl;
	}
	logfile->write(" done! (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s!");
}
//---------------------------------------------------------------------------
void backward::write(long &snp, double****** storage){
	//used to Debug only...
	std::ofstream out;
	std::string filename="backward_" + toString(snp) + ".txt";
	out.open(filename.c_str());
	out << "storage" << std::endl;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][snp]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][snp]; ++n){
							out << i << "\t" << j << "\t" << k << "\t" << l << "\t" << m << "\t" << n << "\t" << storage[i][j][k][l][m][n] << std::endl;
						}
					}
				}
			}
		}
	}
	/*
	out << "sums" << endl;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int l=0; l<data->nRefpop; ++l){
				for(int m=0; m<data->nRefpop; ++m){
					out << i << "\t" << j << "\t" <<  l << "\t" << m << "\t"  << precomputedSums[i][j][l][m] << endl;
				}
			}
		}
	}
	*/
	out << std::endl << "transprob" << std::endl;
	long oldsnp=snp+1;
	for(int i=0; i<data->nRefpop; ++i){
		for(int j=0; j<data->nRefpop; ++j){
			for(int k=0; k<data->nTypes[j][snp]; ++k){
				for(int l=0; l<data->nRefpop; ++l){
					for(int m=0; m<data->nRefpop; ++m){
						for(int n=0; n<data->nTypes[m][snp]; ++n){
							out << i << "\t" << j << "\t" << k << "\t" << l << "\t" << m << "\t" << n << "\t" << getEmissionprob(i,j,k,l,m,n,oldsnp);
						}
					}
				}
			}
		}
	}


	out << "Xi" << std::endl;
	for(int i=0; i<data->nRefpop; ++i){
		out << i << "\t10\t" << transProbHaplotypes[1][0][i] << std::endl;
		out << i << "\t11\t" << transProbHaplotypes[1][1][i] << std::endl;
		out << i << "\t00\t" << transProbHaplotypes[0][0][i] << std::endl;
		out << i << "\t01\t" << transProbHaplotypes[0][1][i] << std::endl;
	}
	out << "phi" << std::endl;
	for(int i=0; i<data->nRefpop; ++i){
		out << i << "\t" << transProbCopyPopIdentityTerm[i] << std::endl;
	}
	out << "precomputedSumsCopyPopIdentityFirst" << std::endl;
	for(int I=0; I<data->nRefpop; ++I){
		for(int J=0; J<data->nRefpop; ++J){
			for(int K=0; K< data->nTypes[J][snp]; ++K){ //for each type within the current copy population
				for(int L=0; L<data->nRefpop; ++L){
					out << I << "\t" << J << "\t" << K << "\t" << L << "\t" << precomputedSumsCopyPopIdentityFirst[I][J][K][L] << std::endl;
				}
			}
		}
	}
	out.close();
}
//------------------------------------------------------------------------
