/*
 * forwardBackward.h
 *
 *  Created on: Mar 3, 2010
 *      Author: wegmannd
 */

#ifndef FORWARDBACKWARD_H_
#define FORWARDBACKWARD_H_

#include "hmmDataStorage.h"
#include "snpDB.h"
#include <time.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <map>
#include <vector>
#include <stdio.h>
#include <math.h>
#include "TLog.h"

class hmmvar{
public:
	hmmDataStorage* data;
	hmmpars* pars;
	TLog* logfile;
	double******* tempStorage; //tempStorage[step][ancestry1][copyPop1][haplo1][ancestry2][copypop2][haplo2]
	double*** transProbHaplotypes; //there are 4 types per haplotype (see function) per new refpop; transProbHaplotypes[same ancestry][miscopying][refpop]
	double* transProbCopyPopIdentityTerm; //the additional terms of the two types where we stay in the same haplotype. [refpop]
	double*** copyPopIdentityTypeSwitchProbability;
	double** typeSwitchProbability;
	double LL; // the likelihood

	hmmvar(hmmDataStorage* Data, hmmpars* Pars, TLog* Logfile);
	//hmmvar();
	virtual ~hmmvar(){
		freeMemory();
	};

	virtual void precomputeTransitionProbabilityTerms(){};
	virtual void allocateMemory();
	virtual void freeMemory();
	double getHaplotypeEmissionprob(int &i, int &j, int &k, bool haplotype, long &snp);
	double getEmissionprob(int &i, int &j, int &k, int &l, int &m, int &n, long &snp);
	double getEmissionprob2(int i, int j, int k, int l, int m, int n, long &snp);
	virtual void precomputeSums(long snp, double****** storageArray){};
	virtual void computeTransitionMatrix(long &snp);
	virtual void initialize(double****** storageArray){};
	virtual void inductionSLOW(int &I, int &J, int &K, int &L, int &M, int &N, long &snp, long &oldSnp, double****** storageArrayOld, double****** storageArrayNew){};
	virtual void induction(int &I, int &J, int &K, int &L, int &M, int &N, long &snp, long &oldSnp, double****** storageArrayOld, double****** storageArrayNew){};
};

class forward:public hmmvar{
public:
	//we perform two forward runs: a first global, and a second, local one.
	//During the global run we store the forward variable only every recomputeWindowSize SNP.
	//We then recompute the local forward variable when needed using the stored steps.
	int recomputeWindowSize;
	int nSavePoints;
	long firstRecomputedSNP;
	double** tempPrecomputedSums; //[refpop0 old][refpop1 old]
	double**** precomputedSumsCopyPopIdentityFirst; //the sums of the additional terms in the case of copy pop identity. precomputedSumsCopyPopIdentity[refpop 0][copy pop 0][type 0][refpop 1]
	double**** precomputedSumsCopyPopIdentitySecond; //the sums of the additional terms in the case of copy pop identity. precomputedSumsCopyPopIdentity[refpop 1][copy pop 1][type 1][refpop 0]
	double**** precomputedSums; //[refpop0][copypop0][refpop1][copypop1]

	double******* forwardSaved; //we save it every recomputeWindowSize snp. backward[savepoint][ancestry1][copyPop1][haplo1][ancestry2][copypop2][haplo2]
	double******* forwardLocal; //backward[savepoint][ancestry1][copyPop1][haplo1][ancestry2][copypop2][haplo2]
	double* scalingTerm; // A term used to keep the forward/backward variable at a reasonable order of magnitude. This term is names \delta or \epslion in our notes. scalingTerm[snp]

	forward(hmmDataStorage* data, hmmpars* pars, int RecomputeWindowSize, TLog* Logfile);
	virtual ~forward(){
		freeMemory();
	};
	virtual void allocateMemory();
	virtual void freeMemory();
	virtual void initialize(double****** storageArray);
	virtual void precomputeSums(long snp, long previousSNP, double****** storageArray);
	virtual void induction(int &I, int &J, int &K, int &L, int &M, int &N, long &snp, long &oldSnp, double****** storageArrayOld, double****** storageArrayNew);
	virtual void inductionSLOW(int &I, int &J, int &K, int &L, int &M, int &N, long &snp, long &oldSnp, double****** storageArrayOld, double****** storageArrayNew);
	void initialRun();
	double****** getAtSNP(long &snp);
	void localRun(long savePoint);
	void write(long &snp, double****** storage, std::string tag);
};

class backward:public hmmvar{
public:
	double** switchingProbability; //switchingProbabilities[haplotype][snp]. first=0, always pro of switch TO snp
	//double** stateProbability; //stateProbability[#copy pop0][snp] -> as in hapmix, #copy pop0 = 0,1 or 2
	int numStateProbabilities;
	double** stateProbability; //stateProbability[id][snp] -> id identifies the genotype uniquely
	//this is done as min(N*P1 + P2, N*P2 + P1), where N = #pops, P1 pop of haplotype 1m P2 pop of haplotype 2
	double** alleleProbability; //allelProbability[chr1 or 2][snp];
	double***** transProbTemp; //used when computing switching probabilities. transProbTemp;[from pop][to pop][to copypop][to type]
	double***** precomputedSumsCopyPopIdentityFirst; //the sums of the additional terms in the case of copy pop identity. precomputedSumsCopyPopIdentity[refpop 0][copy pop 0][type 0][refpop 1][miscopy 1]
	double***** precomputedSumsCopyPopIdentitySecond; //the sums of the additional terms in the case of copy pop identity. precomputedSumsCopyPopIdentity[refpop 1][copy pop 1][type 1][refpop 0][miscopy 0]
	double**** tempPrecomputedSums; //[refpop0 old][refpop1 old][ancestry switch 0][ancestry switch 1]
	double** precomputedSums; //[refpop0][refpop1]
	double scalingTerm; // A term used to keep the forward/backward variable at a reasonable order of magnitude. This term is names \delta or \epslion in our notes. scalingTerm[snp]
	bool doComputeSwitchProb;

	backward(hmmDataStorage* data, hmmpars* pars, TLog* Logfile);
	~backward(){
		freeMemory();
	};
	virtual void allocateMemory();
	virtual void freeMemory();
	virtual void initialize(double****** storageArray);
	void runAndComputeSwitchingProbabilities(forward* myForward);
	virtual void precomputeSums(long snp, long previousSnp, double****** storageArray);
	virtual void inductionSLOW(int &I, int &J, int &K, int &L, int &M, int &N, long &snp, long &oldSnp, double****** storageArrayOld, double****** storageArrayNew);
	virtual void induction(int &I, int &J, int &K, int &L, int &M, int &N, long &snp, long &oldSnp, double****** storageArrayOld, double****** storageArrayNew);
	double computeNormalizingSum(long &snp, double****** forwardT, double****** backwardT);
	void computeStateProbabilities(long &snp, double &sumToNormalize, double****** forwardT, double****** backwardT);
	void computeAncestralAlleleProbabilities(long &snp, double &sumToNormalize, double****** forwardT, double****** backwardT);
	void computeSwitchingProbabilities(long &snp, double &sumToNormalize, double****** forwardT, double****** backwardT, double****** backwardTplusOne);
	void writeProbabilities(std::ofstream* out, int ind, snpDB* mySnpDB);
	void write(long &snp, double****** storage);
};

#endif /* FORWARDBACKWARD_H_ */
