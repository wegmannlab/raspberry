/*
 * hmm.cpp
 *
 *  Created on: Feb 21, 2010
 *      Author: wegmannd
 */

#include "hmm.h"
//---------------------------------------------------------------------------
hmm::hmm(int NRefPop, int* refPopSizes, int Nadm, std::map<long,long>* SnpMap, TLog* Logfile){
	logfile = Logfile;
	data=new hmmDataStorage(NRefPop, refPopSizes, Nadm, SnpMap, logfile);
	pars=new hmmpars();
	myForward = NULL;
	myBackward = NULL;
}
//---------------------------------------------------------------------------
void hmm::initializeHMMvariables(int RecomputeWindowSize){
	clock_t start=clock();
	logfile->listFlush("Allocating memory ...");
	if(RecomputeWindowSize<1) throw "RecomputeWindowSize must be a positive integer!";
	myForward=new forward(data, pars, RecomputeWindowSize, logfile);
	myBackward=new backward(data, pars, logfile);
	myForward->allocateMemory();
	myBackward->allocateMemory();
	logfile->write(" done (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)!");
}
//---------------------------------------------------------------------------
void hmm::runHMM(std::string outputBasename, std::ofstream* usedParamFile, int id, bool onlyLL, snpDB* mySnpDB){
	std::string filename;
	std::ofstream output; //current output. opened and closed for each individual
	for(int ind=0; ind<data->nadm; ++ind){
		logfile->startIndent("Performing HMM for individual " + toString(ind+1) + ":");
		data->setCurrentIndividual(ind);
		pars->setParametersForIndividual(ind);
		myForward->initialRun();
		if(!onlyLL){
			myBackward->runAndComputeSwitchingProbabilities(myForward);
			filename=outputBasename+"_Ind_"+toString(ind+1)+".txt";
			output.open(filename.c_str());
			myBackward->writeProbabilities(&output, ind, mySnpDB);
			output.close();
		}
		//write parameters used and LL to file
		*usedParamFile << (id+1) << "\t" << pars->timeSinceAdm;
		for(int i=0; i<data->nRefpop; ++i) *usedParamFile << "\t" << pars->globalAncestry[i];
		for(int i=0; i<data->nRefpop; ++i) *usedParamFile << "\t" << pars->ancestralRate[i];
		for(int i=0; i<data->nRefpop; ++i) *usedParamFile << "\t" << pars->mutation[i];
		*usedParamFile << "\t" << pars->miscopyRate[1];
		*usedParamFile << "\t" << pars->miscopyMutation;
		*usedParamFile << "\t" << ind << "\t" << myForward->LL << std::endl;
		logfile->endIndent();
	}
}
//---------------------------------------------------------------------------
