/*
 * hmm.h
 *
 *  Created on: Feb 21, 2010
 *      Author: wegmannd
 */

#ifndef HMM_H_
#define HMM_H_

#include "hmmDataStorage.h"
#include "forwardBackward.h"
#include <time.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
#include <vector>
#include <stdio.h>
#include <math.h>
#include "TLog.h"

class hmm{
public:
	hmmDataStorage* data;
	hmmpars* pars;
	forward* myForward;
	backward* myBackward;
	TLog* logfile;

	hmm(int NRefPop, int* refPopSizes, int Nadm, std::map<long,long>* SnpMap, TLog* Logfile);
	~hmm(){
		delete myForward;
		delete myBackward;
		delete data;
		delete pars;
	};
	void initializeHMMvariables(int RecomputeWindowSize);
	void setGeneticDistance(std::string filename, double defaultRate){data->setGeneticDistance(filename, defaultRate);};
	void setGeneticDistance(double defaultRate){data->setGeneticDistance(defaultRate);};
	void setParameters(int TimeSinceAdm, double* GlobalAncestry, double* AncestralRate, double* Mutation, double MiscopyRate, double MiscopyMutation){
		pars->setParameters(TimeSinceAdm, GlobalAncestry, AncestralRate, Mutation, MiscopyRate, MiscopyMutation, data->nRefpop);
	};
	void setParameters(int TimeSinceAdm, double** GlobalAncestry, double* AncestralRate, double* Mutation, double MiscopyRate, double MiscopyMutation, int nAncestryProp){
		//check if the number of individual specific ancestry proportions matches the number of admixed individuals
		if(nAncestryProp!=data->nadm) throw "The number of individual specific ancestry proportions does not match the number of admixed individuals!";
		pars->setParameters(TimeSinceAdm, GlobalAncestry, AncestralRate, Mutation, MiscopyRate, MiscopyMutation, data->nRefpop, nAncestryProp);
	};
	void skipswitchPointProbCalvculations(){myBackward->doComputeSwitchProb=false;};
	void collapsReferencePopulations(double CollapsingDistance, int maxNTypes){data->collapsReferencePopulations(CollapsingDistance, maxNTypes);};
	bool* getHaplotypeStoragePointer(int refpopNumber, long snp){ return data->getHaplotypeStoragePointer(refpopNumber, snp);}
	int* getAdmixpopGenotypeStoragePointer(long snp){ return data->getAdmixpopGenotypeStoragePointer(snp);}
	void runHMM(std::string outputBasename, std::ofstream* usedParamFile, int id, bool onlyLL, snpDB* mySnpDB);
};


#endif /* HMM_H_ */
