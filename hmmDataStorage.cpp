/*
 * hmmDataStorage.cpp
 *
 *  Created on: Mar 3, 2010
 *      Author: wegmannd
 */

#include "hmmDataStorage.h"
//---------------------------------------------------------------------------
hmmpars::hmmpars(){
	storageInitialized=false;
	nRefpop = -1;
	timeSinceAdm = -1;
	globalAncestry = NULL; //fraction if global ancestry (mu) -> one for each ancestral population, but mu1=1-mu2
	individualSpecificGlobalAncestry = false;
	individualGlobalAncestry = NULL;
	nAncestryProp = -1;
	ancestralRate = NULL;
	miscopyRate = NULL;
	mutation = NULL;
	oneMinusMutation = NULL;
	miscopyMutation = -1.0;
	oneMinusMiscopyMutation = -1.0;
	precomputedTransitionProbabilityTerms = NULL;
};

void hmmpars::setParameters(int TimeSinceAdm, double** GlobalAncestry, double* AncestralRate, double* Mutation, double MiscopyRate, double MiscopyMutation, int NRefpop, int adm){
	//this means that we have individual specific global ancestry proportions!
	individualSpecificGlobalAncestry=true;
	nRefpop=NRefpop;
	individualGlobalAncestry=GlobalAncestry;
	setFixedParameters(TimeSinceAdm, AncestralRate, Mutation, MiscopyRate, MiscopyMutation);
}
void hmmpars::setParameters(int TimeSinceAdm, double* GlobalAncestry, double* AncestralRate, double* Mutation, double MiscopyRate, double MiscopyMutation, int NRefpop){
	individualSpecificGlobalAncestry=false;
	nRefpop=NRefpop;
	setFixedParameters(TimeSinceAdm, AncestralRate, Mutation, MiscopyRate, MiscopyMutation);
	for(int i=0; i<nRefpop; ++i){
		globalAncestry[i]=GlobalAncestry[i];
	}
	//precompute some terms for the transition probabilities
	for(int i=0; i<nRefpop; ++i){
		precomputedTransitionProbabilityTerms[0][i]=globalAncestry[i] * miscopyRate[0];
		precomputedTransitionProbabilityTerms[1][i]=globalAncestry[i] * miscopyRate[1];
	}
}

void hmmpars::setFixedParameters(int TimeSinceAdm, double* AncestralRate, double* Mutation, double MiscopyRate, double MiscopyMutation){
	if(!storageInitialized){
		ancestralRate=new double[nRefpop];
		mutation=new double[nRefpop];
		oneMinusMutation=new double[nRefpop];
		globalAncestry=new double[nRefpop];
		precomputedTransitionProbabilityTerms=new double*[2];
		precomputedTransitionProbabilityTerms[0]=new double[nRefpop];
		precomputedTransitionProbabilityTerms[1]=new double[nRefpop];
		miscopyRate=new double[2];
		storageInitialized=true;
	}

	timeSinceAdm=TimeSinceAdm;
	for(int i=0; i<nRefpop; ++i){
		ancestralRate[i]=AncestralRate[i];
		mutation[i]=Mutation[i];
		oneMinusMutation[i]=1-Mutation[i];
	}
	miscopyMutation=MiscopyMutation;
	oneMinusMiscopyMutation=1-MiscopyMutation;
	//miscopy as array!

	miscopyRate[1]=MiscopyRate;
	miscopyRate[0]=1-MiscopyRate;
}
void hmmpars::setParametersForIndividual(int ind){
	if(individualSpecificGlobalAncestry){
		for(int i=0; i<nRefpop; ++i){
			globalAncestry[i]=individualGlobalAncestry[ind][i];
		}
		//precompute some terms for the transition probabilities
		for(int i=0; i<nRefpop; ++i){
			precomputedTransitionProbabilityTerms[0][i]=globalAncestry[i] * miscopyRate[0];
			precomputedTransitionProbabilityTerms[1][i]=globalAncestry[i] * miscopyRate[1];
		}
	}
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
hmmDataStorage::hmmDataStorage(int NRefPop, int* refPopSizes, int Nadm, std::map<long,long>* SnpMap, TLog* Logfile){
	logfile = Logfile;
	snpMap=SnpMap;
	nRefpop=NRefPop;
	nref=new int[nRefpop];
	maxNrefpop=0;
	for(int i=0; i<nRefpop; ++i){
		nref[i]=refPopSizes[i];
		if(nref[i]>maxNrefpop) maxNrefpop = nref[i];
	}
	nadm=Nadm;
	nsnps=snpMap->size();

	//initialize data structure
	refpop=new bool**[nRefpop];
	refpopCollapsed=new bool**[nRefpop];
	for(int i=0; i<nRefpop; ++i){
		refpop[i]=new bool*[nsnps];
		refpopCollapsed[i]=new bool*[nsnps];
	}
	admpop=new int*[nsnps];
	for(int s=0; s<nsnps; ++s){
		for(int i=0; i<nRefpop; ++i) refpop[i][s]=new bool[nref[i]];
		admpop[s]=new int[nadm];
	}
	rates=new double[nsnps];

	//other vars
	nTypes = NULL;
	currentIndividual = -1;
	maxStatesPerPop = -1;
	collapsingDistance = -1;
	collapsingDistanceHalf = -1;
	typeCounts = NULL;
	typeSwitches = NULL;
}
//---------------------------------------------------------------------------
void hmmDataStorage::collapsReferencePopulations(double CollapsingDistance, unsigned int maxNTypes){
	clock_t start=clock();
	std::string progressTag = "Collapsing reference populations within " + toString(CollapsingDistance) + " cM ...";
	logfile->listFlush(progressTag);
	collapsingDistance=CollapsingDistance/100;
	collapsingDistanceHalf=collapsingDistance/2;
	//maxNTypes==0 means no restriction -> set to a very large value
	if(maxNTypes==0) maxNTypes=9999999;
	else ++maxNTypes; // allows me to test < rather than <=

	//initialize some variables
	nTypes=new int*[nRefpop];
	typeSwitches=new int***[nRefpop];
	typeCounts=new int**[nRefpop];
	for(int i=0; i<nRefpop; ++i){
		typeSwitches[i]=new int**[nsnps];
		typeCounts[i]=new int*[nsnps];
		nTypes[i]=new int[nsnps];
	}

	long first, last;
	double dist;
	std::vector<int> prototypes; //vector pointing to the first haplotype of a type, the prototype
	bool match, matchFound, numTypesOk;
	int*** indType; //a DB matching individuals to types. indType[step][haplo]->type
	indType=new int**[2];
	for(int a=0; a<2; ++a){
		indType[a]=new int*[nRefpop];
		for(int i=0; i<nRefpop; ++i) indType[a][i]=new int[maxNrefpop];
	}
	bool step=0;
	maxStatesPerPop=0;
	int prog, oldProg=0;
	//run through all snps
	for(long snp=0; snp<nsnps; ++snp){
		//find snps within window -> get first and last snp -> they are the same for all populations!
		//each window consists of at least two snps on each side
		first=snp;
		dist=0;
		while(first>0 && ((snp-first)<2 || (dist + rates[first])<collapsingDistanceHalf)){
			dist+=rates[first];
			--first;
		}
		last=snp;
		dist=0;
		while(last<(nsnps-1) && ((last-snp)<2 || (dist + rates[last+1])<collapsingDistanceHalf)){
			++last;
			dist+=rates[last];
		}
		++last; //the snp OUTSIDE the range -> easier below

		//now get the types within that window
		for(int i=0; i<nRefpop; ++i){
			numTypesOk=false;
			while(!numTypesOk){
				prototypes.clear();
				prototypes.push_back(0);
				//save first type
				indType[step][i][0]=0;
				//go through all haplotypes
				for(int h=1; h<nref[i]; ++h){
					//compare to all known types
					matchFound=false;
					for(unsigned int t=0; t<prototypes.size(); ++t){
						match=true;
						for(long s=first; s<last; ++s){
							if(refpop[i][s][h]!=refpop[i][s][prototypes[t]]){
								match=false;
								break;
							}
						}
						if(match){
							indType[step][i][h]=t;
							matchFound=true;
							break;
						}
					}
					if(!matchFound){ //insert new type
						indType[step][i][h]=prototypes.size();
						prototypes.push_back(h);
					}
				}
				if(prototypes.size()<maxNTypes || (last-first)<6) numTypesOk=true; //make sure there is at least two SNPs on each side
				else {
					//remove a snp with least impact
					if(rates[last-1]<rates[first+1] && (last-snp)>2) --last;
					else ++first;
				}
			}

			//save
			nTypes[i][snp]=prototypes.size();
			if(prototypes.size() > maxStatesPerPop) maxStatesPerPop=prototypes.size();
			typeCounts[i][snp]=new int[prototypes.size()];
			refpopCollapsed[i][snp]=new bool[prototypes.size()];
			for(unsigned int t=0; t<prototypes.size(); ++t){
				refpopCollapsed[i][snp][t]=refpop[i][snp][prototypes[t]];
				typeCounts[i][snp][t]=0;
			}
			for(int h=0; h<nref[i]; ++h){
				++typeCounts[i][snp][indType[step][i][h]];
			}
			//check type switching rates
			if(snp>0){
				typeSwitches[i][snp]=new int*[nTypes[i][snp-1]];
				for(int a=0; a<nTypes[i][snp-1]; ++a){
					typeSwitches[i][snp][a]=new int[nTypes[i][snp]];
					for(int t=0; t<nTypes[i][snp]; ++t) typeSwitches[i][snp][a][t]=0;
				}
				for(int h=0; h<nref[i]; ++h){
					++typeSwitches[i][snp][indType[1-step][i][h]][indType[step][i][h]];
				}
				for(int a=0; a<nTypes[i][snp]; ++a){
					for(int b=0; b<nTypes[i][snp-1]; ++b){
					}
				}
			}
		}
		step=1-step;
		//output progress
		prog=100.0 * (double) snp / (double) nsnps;
		if(prog > oldProg){
			oldProg=prog;
			logfile->listOverFlush(progressTag + " (" + toString(prog) + "%)");
		}
	}
	//cleaning up
	for(int a=0; a<2; ++a){
		for(int i=0; i<nRefpop; ++i) delete[] indType[a][i];
		delete[] indType[a];
	}
	delete[] indType;

	logfile->overList(progressTag + " done! (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)!   ");
}
//---------------------------------------------------------------------------
void hmmDataStorage::setGeneticDistance(std::string filename, double defaultRate){
	clock_t start=clock();
	logfile->startIndent("Computing genetic distances:");
	logfile->list("Using default rate of " + toString(defaultRate) + "  cM/Mb");
	logfile->listFlush("Reading rates from file '" + filename + "' ...");
	//default rate is in cM per Megabase.... and so are other rates!
	defaultRate=defaultRate/100000000.0; //per base pair
	//read file...
	std::ifstream ratesFile;
	ratesFile.open(filename.c_str());
	if(!ratesFile) throw "failed to open rates file '" + filename + "'!";
	std::map<long,double> readRates;
	std::string tmp;
	std::vector<std::string> temp;
	long line=0;
	getline(ratesFile, tmp); //this file has a header....
	while(ratesFile.good() && !ratesFile.eof()){
		++line;
		fillVectorFromLineWhiteSpaceSkipEmpty(ratesFile, temp);
		if(temp.size()>0){
			if(temp.size()!=3) throw "Wrong number of values in rates file '" + filename + " on line " + toString(line) + ".";
			//rates are cm/megabase
			readRates.insert(std::pair<long,double>(stringToLongCheck(temp[0]), stringToDoubleCheck(temp[1])/100000000.0));
			temp.clear();
		}
	}
	ratesFile.close();
	logfile->write(" done! (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)");

	//now go through snps and update rates...
	start=clock();
	logfile->listFlush("Calculating rate for each SNP ...");
	std::map<long, long>::iterator snpIt, prevSnpIt;
	prevSnpIt=snpMap->begin();
	snpIt=snpMap->begin(); ++snpIt;
	std::map<long, double>::iterator beforeSnpMapIt, afterSnpIt;
	beforeSnpMapIt=readRates.begin();
	afterSnpIt=beforeSnpMapIt; ++afterSnpIt;
	long maxPos=readRates.rbegin()->first;
	long minPos=readRates.begin()->first;
	long i=1;
	rates[0]=0.0;//first position does not have a rate
	for(;snpIt!=snpMap->end();++snpIt, ++prevSnpIt, ++i){
		//make sure pointers are fine
		//default rate if interval is before min Pos
		if(snpIt->first<=minPos) rates[i]=defaultRate*(snpIt->first - prevSnpIt->first);
		else {
			if(snpIt->first > maxPos){
				//default rate if both prev and current snp > max pos
				if(prevSnpIt->first > maxPos) rates[i]=defaultRate*(snpIt->first - prevSnpIt->first);
				else {
					//if snp > maxpos but prev < maxpos: known rate up to maxpos, and then default rate
					rates[i]=beforeSnpMapIt->second * (afterSnpIt->first - prevSnpIt->first);
					while(afterSnpIt->first < maxPos){
						++beforeSnpMapIt; ++afterSnpIt;
						rates[i]+=beforeSnpMapIt->second * (afterSnpIt->first - beforeSnpMapIt->first);
					}
					rates[i]+=defaultRate*(snpIt->first - maxPos);
				}
			} else {
				//minPos < snp < maxpos
				if(prevSnpIt->first < minPos){
					//default rate up to minPos, then known rate
					rates[i]=defaultRate*(minPos - prevSnpIt->first);
					while(afterSnpIt->first < snpIt->first){
						rates[i]+=beforeSnpMapIt->second * (afterSnpIt->first - beforeSnpMapIt->first);
						++beforeSnpMapIt; ++afterSnpIt;
					}
					rates[i]+=beforeSnpMapIt->second * (snpIt->first - beforeSnpMapIt->first);
				} else {
					//minpos < prev < snp < maxpos -> normal case
					if(snpIt->first < afterSnpIt->first){
						rates[i]=beforeSnpMapIt->second * (snpIt->first - prevSnpIt->first);
					} else {
						rates[i]=beforeSnpMapIt->second * (afterSnpIt->first - prevSnpIt->first);
						++beforeSnpMapIt; ++afterSnpIt;
						while(afterSnpIt->first < snpIt->first){
							rates[i]+=beforeSnpMapIt->second * (afterSnpIt->first - beforeSnpMapIt->first);
							++beforeSnpMapIt; ++afterSnpIt;
						}
						rates[i]+=beforeSnpMapIt->second * (snpIt->first - beforeSnpMapIt->first);
					}
				}
			}
		}
	}
	logfile->write(" done! (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)");
	logfile->endIndent();
}
//---------------------------------------------------------------------------
void hmmDataStorage::setGeneticDistance(double defaultRate){
	clock_t start=clock();
	logfile->startIndent("Computing genetic distances:");
	logfile->list("Using default rate of " + toString(defaultRate) + "  cM/Mb");
	logfile->listFlush("Calculating rate for each SNP ...");
	//Default rate is in cM per Megabase.
	defaultRate=defaultRate/100000000.0; //per base pair
	rates[0]=0.0;
	std::map<long, long>::iterator snpIt, prevSnpIt;
	prevSnpIt=snpMap->begin();
	long i=1;
	snpIt=snpMap->begin();
	++snpIt;
	for(;snpIt!=snpMap->end();++snpIt, ++prevSnpIt, ++i){
		rates[i]=defaultRate*(snpIt->first-prevSnpIt->first);
	}
	logfile->write(" done! (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)");
	logfile->endIndent();
}
