/*
 * hmmDataStorage.h
 *
 *  Created on: Mar 3, 2010
 *      Author: wegmannd
 */

#ifndef HMMDATASTORAGE_H_
#define HMMDATASTORAGE_H_

#include "stringFunctions.h"
#include <time.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
#include <vector>
#include <stdio.h>
#include <math.h>
#include "TLog.h"

struct hmmpars{
	bool storageInitialized;
	int nRefpop;
	int timeSinceAdm; //time since admixture in generations (T)
	double* globalAncestry; //fraction if global ancestry (mu) -> one for each ancestral population, but mu1=1-mu2
	bool individualSpecificGlobalAncestry;
	double** individualGlobalAncestry;
	int nAncestryProp;
	double* ancestralRate; //ancestral recombination rate for each ancestral population (rho)
	double* miscopyRate; //miscopying parameter (p) -> [0] = no miscopying, [1]=miscopying
	double* mutation; //mutation rate for each ancestral population (theta)
	double* oneMinusMutation; //1-mutation
	double miscopyMutation, oneMinusMiscopyMutation; //(theta3)
	double** precomputedTransitionProbabilityTerms; // [haplotypicTransitionType][refpop]

	hmmpars();
	~hmmpars(){
		delete[] globalAncestry;
		delete[] ancestralRate;
		delete[] miscopyRate;
		delete[] mutation;
		delete[] oneMinusMutation;
		delete[] precomputedTransitionProbabilityTerms[0];
		delete[] precomputedTransitionProbabilityTerms[1];
		delete[] precomputedTransitionProbabilityTerms;

	};
	void setParameters(int TimeSinceAdm, double* GlobalAncestry, double* AncestralRate, double* Mutation, double MiscopyRate, double MiscopyMutation, int NRefpop);
	void setParameters(int TimeSinceAdm, double** GlobalAncestry, double* AncestralRate, double* Mutation, double MiscopyRate, double MiscopyMutation, int nRefpop, int adm);
	void setFixedParameters(int TimeSinceAdm, double* AncestralRate, double* Mutation, double MiscopyRate, double MiscopyMutation);
	void setParametersForIndividual(int ind);
};

class hmmDataStorage{
public:
	TLog* logfile;
	//variable to store data
	std::map<long,long>* snpMap;
	int nRefpop; //number of reference populations
	bool*** refpop; //haplotypes refpop[refpop][snp][haplo]
	int** admpop; //genotypes
	int* nref; //vector with the population sizes
	int nadm;
	long nsnps;
	double* rates; //distance from previous. First=0;
	int currentIndividual; //the current admixed individual
	int maxNrefpop;
	//variables used for collapsing
	unsigned int maxStatesPerPop;
	double collapsingDistance, collapsingDistanceHalf; //the window size in centimorgans aroudn a SNP used for collapsing
	bool*** refpopCollapsed; //haplotypes refpopCollapsed[refpop][snp][type] where type is the collapsed type at this location
	int*** typeCounts; //the number of haplotypes for each type typeCounts[refpop][snp][type]
	int**** typeSwitches; //the number of switches from type i to j between snp-1 and snp WITHOUT recombination event. typeSwitches[refpop][snp][type i][type j]. first=0
	int** nTypes; //number of types at each snp nTypes[refpop][snp]


	hmmDataStorage(int NRefPop, int* refPopSizes, int Nadm, std::map<long,long>* SnpMap, TLog* Logfile);
	~hmmDataStorage(){
		for(int i=0; i<nRefpop; ++i){
			for(int s=1; s<nsnps; ++s){
				for(int a=0; a<nTypes[i][s-1]; ++a) delete[] typeSwitches[i][s][a];
				delete[] typeSwitches[i][s];
			}
			for(int s=0; s<nsnps; ++s){
				delete[] refpopCollapsed[i][s];
				delete[] typeCounts[i][s];
				delete[] refpop[i][s];
			}
			delete[] typeSwitches[i];
			delete[] nTypes[i];
			delete[] refpopCollapsed[i];
			delete[] typeCounts[i];
			delete[] refpop[i];
		}
		delete[] typeSwitches;
		delete[] nTypes;
		delete[] refpopCollapsed;
		delete[] typeCounts;
		delete[] refpop;

		for(int s=0; s<nsnps; ++s){
			delete[] admpop[s];
		}
		delete[] admpop;
		delete[] rates;
		delete[] nref;
	};
	bool* getHaplotypeStoragePointer(int refpopNumber, long snp){ return refpop[refpopNumber][snp]; }
	int* getAdmixpopGenotypeStoragePointer(long snp){ return admpop[snp]; }
	void collapsReferencePopulations(double CollapsingDistance, unsigned int maxNTypes);
	void setGeneticDistance(std::string filename, double defaultRate);
	void setGeneticDistance(double defaultRate);
	void setCurrentIndividual(int &ind){currentIndividual=ind;};
	int getGenotypeAtSnp(long &snp){return admpop[snp][currentIndividual];};
};


#endif /* HMMDATASTORAGE_H_ */
