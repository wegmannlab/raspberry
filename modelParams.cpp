/*
 * modelParams.cpp
 *
 *  Created on: Feb 26, 2010
 *      Author: wegmannd
 */
#include "modelParams.h"

modelParams::modelParams(TParameters* MyInputfile, TLog * Logfile){
	logfile = Logfile;
	logfile->startIndent("Reading model parameters:");
	parametercombinationsBuilt=false;
	//read number of reference populations
	myInputfile=MyInputfile;
	nRefpop=myInputfile->getParameterDouble("nRefPop");
	logfile->list("expecting data from " + toString(nRefpop) + " reference populations.");

	//read hmm parameters (apart from genetic map) from input file
	//each may be a list (delimited with ';')

	//first read parameters which are NOT population specific
	//time since admixture
	std::string temp=myInputfile->getParameterString("timeSinceAdmixture");
	fillVectorFromString(temp, timeSinceAdmixture, ';');
	for(std::vector<int>::iterator it=timeSinceAdmixture.begin(); it!=timeSinceAdmixture.end(); ++it)
		if(*it<1) throw "timeSinceAdmixture below 1!";
	std::string delim = ", ";
	std::string tmp;
	concatenateString(timeSinceAdmixture, tmp, delim);
	logfile->list("time(s) since admixture: " + tmp);

	//miscopying rates
	temp=myInputfile->getParameterString("miscopyRate");
	fillVectorFromString(temp, miscopyRate, ';');
	for(std::vector<double>::iterator it=miscopyRate.begin(); it!=miscopyRate.end(); ++it){
		if(*it>1.0) throw "Miscopy rates above one!";
		if(*it< 0.0) throw "Negative miscopy rates!";
	}
	concatenateString(miscopyRate, tmp, delim);
		logfile->list("miscopying rates: " + tmp);

	//miscopy mutation
	temp=myInputfile->getParameterString("miscopyMutation");
	fillVectorFromString(temp, miscopyMutation, ';');
	for(std::vector<double>::iterator it=miscopyMutation.begin(); it!=miscopyMutation.end(); ++it){
		if(*it>1.0) throw "miscopyMutation above one!";
		if(*it<0.0) throw "Negative miscopy rates!";
	}
	concatenateString(miscopyMutation, tmp, delim);
	logfile->list("miscopying mutation rate(s): " + tmp);



	//now read those parameters, which are specific for each ancestral population
	//ancestry proportions -> a file with proportions specific to each individual?
	if(myInputfile->parameterExists("ancestryPropFile")){
		//a file is read which contains one line per individual with the individual specific ancestry proportions
		individualSpecificAncestryProp=true;
		std::string filename = myInputfile->getParameterString("ancestryPropFile");
		logfile->listFlush("Reading ancestry proportions from file '" + filename + "' ...");
		nAncestryProp=readIndividualSpecificPerPopParameter(filename, ancestryProp);
		logfile->write(" done!");
		logfile->conclude("Read " + toString(nAncestryProp) + " sets");
	} else {
		individualSpecificAncestryProp=false;
		nAncestryProp=readPerPopParameter("ancestryProp", ancestryProp);
		logfile->list("ancestry proportions: will use " + toString(nAncestryProp) + " sets");
	}
	//check ancestry proportions, do they sum to 1?
	double sum;
	for(int i=0; i<nAncestryProp; ++i){
		sum=0;
		for(int j=0; j<nRefpop; ++j){
			sum+=ancestryProp[i][j];
			//close to 1 is adjusted to 1....
			if(fabs(sum-1.0)<0.00001) ancestryProp[i][nRefpop-1]=ancestryProp[i][nRefpop-1]+(1-sum);
		}
		if(fabs(sum-1.0)>0.0001) throw "Ancestry proportions do not sum to 1.0 (they sum to " + toString(sum) + ")!";
	}

	//ancestral "recombination rates"
	nAncestralRate=readPerPopParameter("ancestralRate", ancestralRate);
	logfile->list("ancestral rates: will use " + toString(nAncestralRate) + " sets");

	//mutation rates
	nMutation=readPerPopParameter("mutation", mutation);
	logfile->list("mutation rates: will use " + toString(nMutation) + " sets");
	logfile->endIndent();
}
//---------------------------------------------------------------------------
int modelParams::readPerPopParameter(std::string name, double**& storage){
	//read first
	std::vector<double> tempVec;
	std::string temp;
	temp=myInputfile->getParameterString(name + "1");
	fillVectorFromString(temp, tempVec, ';');

	//prepare storage
	std::vector<double>::size_type s=tempVec.size();
	storage=new double*[s];
	for(std::vector<double>::size_type i=0; i<s; ++i){
		storage[i]=new double[nRefpop];
		storage[i][0]=tempVec[i];
		if(tempVec[i]<0) throw "Negative "+ name +" values!";
	}
	//read the others
	for(int i=1; i<nRefpop; ++i){
		tempVec.clear();
		temp=myInputfile->getParameterString(name + toString(i+1));
		fillVectorFromString(temp, tempVec, ';');
		if(tempVec.size() != s) throw "Unequal number of " + name + " values for the different ancestral populations!";
		for(std::vector<double>::size_type j=0; j<s; ++j){
			storage[j][i]=tempVec[j];
			if(tempVec[j]<0) throw "Negative "+ name +" values!";
		}
	}
	return s;
}
//---------------------------------------------------------------------------
int modelParams::readIndividualSpecificPerPopParameter(std::string  filename, double**& storage){
	//read file and store in std::vector, we need to know the number of individuals
	std::ifstream file;
	file.open(filename.c_str());
	if(!file) throw "File '"+ filename +"' could not be opened!";

	std::vector<std::string> lines;
	std::string s;
	//read all lines and store them. There is no header
	while(file.good() && !file.eof()){
		getline(file, s);
		trimString(s);
		if(!s.empty()) lines.push_back(s);
	}

	//prepare and fill storage
	std::vector<double> tempVec;
	storage=new double*[lines.size()];
	for(unsigned int i=0; i<lines.size(); ++i){
		storage[i]=new double[nRefpop];
		fillVectorFromStringWhiteSpaceSkipEmpty(lines[i], tempVec);
		if(tempVec.size()!=(std::vector<double>::size_type) nRefpop){
			throw "Wrong number of values in file '" + filename + "' on line "+ toString(i) + "! One entry per reference population expected.";
		}
		for(int j=0; j<nRefpop; ++j){
			if(tempVec.at(j)<0) throw "Invalid values in file '"+ filename +"'  on line " + toString(i) + "!";
			storage[i][j]=tempVec.at(j);
		}
	}
	return lines.size();
}
//---------------------------------------------------------------------------
void modelParams::buildParameterCombinations(){
	//are the ancestry proportion individual specific? If yes, set nAncestryProp=1;
	logfile->listFlush("Building parameter combinations ...");
	int nUsableAncestryProp=nAncestryProp;
	if(individualSpecificAncestryProp) nUsableAncestryProp=1;

	nCombinations=timeSinceAdmixture.size()*miscopyMutation.size()*miscopyRate.size()*nUsableAncestryProp*nAncestralRate*nMutation;
	timeSinceAdmixtureP=new int*[nCombinations];
	miscopyMutationP=new double*[nCombinations];
	miscopyRateP=new double*[nCombinations];
	ancestryPropP=new double*[nCombinations];
	ancestralRateP=new double*[nCombinations];
	mutationP=new double*[nCombinations];
	parametercombinationsBuilt=true;

	//build DB
	int id=0;
	for(int ap=0; ap<nUsableAncestryProp; ++ap){
		for(unsigned int t=0; t<timeSinceAdmixture.size(); ++t){
			for(int ar=0; ar<nAncestralRate; ++ar){
				for(int m=0; m<nMutation; ++m){
					for(unsigned int mr=0; mr<miscopyRate.size(); ++mr){
						for(unsigned int mm=0; mm<miscopyMutation.size(); ++mm){
							timeSinceAdmixtureP[id]=&timeSinceAdmixture[t];
							miscopyMutationP[id]=&miscopyMutation[mm];
							miscopyRateP[id]=&miscopyRate[mr];
							ancestryPropP[id]=ancestryProp[ap];
							ancestralRateP[id]=ancestralRate[ar];
							mutationP[id]=mutation[m];
							++id;
						}
					}
				}
			}
		}
	}
	logfile->write(" done!");
	logfile->conclude("build " + toString(nCombinations) + " combinations.");
}
//---------------------------------------------------------------------------
void modelParams::runAllParameterCombinations(hmm* myHMM, std::string outputPrefix, bool onlyLL, bool doComputeSwitchProb, snpDB* mySnpDB){
	logfile->startIndent("Running HMM for all parameter combinations:");
	//run through all combinations
	std::string filename=outputPrefix+"_parameter_combinations.txt";
	logfile->list("Writing parameter combinations and log likelihood to '" + filename + "'");

	std::ofstream usedParamFile;
	usedParamFile.open(filename.c_str());
	//write header
	usedParamFile << "id\ttimeSinceAdmixture";
	for(int i=0; i<nRefpop; ++i) usedParamFile << "\tancestryProp" << i;
	for(int i=0; i<nRefpop; ++i) usedParamFile << "\tancestralRate" << i;
	for(int i=0; i<nRefpop; ++i) usedParamFile << "\tmutation" << i;
	usedParamFile << "\tmiscopyRate\tmiscopyMutation\tInd\tlog10(LL)" << std::endl;


	//Write only LL / skipping switch point prob calculation
	if(onlyLL) logfile->list("Calculating log likelihood only (backward algorithm will be skipped).");
	else {

		if(!doComputeSwitchProb){
			logfile->list("The calculation of switch point probabilities will be skipped.");
			myHMM->skipswitchPointProbCalvculations();
		}
	}

	//run through all parameter combinations
	for(int id=0; id<nCombinations; ++id){
		logfile->startIndent("Running parameter combination " + toString(id+1) + ":");
		//set parameters
		if(individualSpecificAncestryProp) myHMM->setParameters(*timeSinceAdmixtureP[id], ancestryProp, ancestralRateP[id], mutationP[id], *miscopyRateP[id], *miscopyMutationP[id], nAncestryProp);
		else myHMM->setParameters(*timeSinceAdmixtureP[id], ancestryPropP[id], ancestralRateP[id], mutationP[id], *miscopyRateP[id], *miscopyMutationP[id]);
		//run...
		filename=outputPrefix+"_paramId_"+toString(id+1);
		logfile->list("Writing results to '" + filename + "'");
		myHMM->runHMM(filename, &usedParamFile, id, onlyLL, mySnpDB);
		logfile->endIndent();
	}
	usedParamFile.close();
	logfile->endIndent();
}
//---------------------------------------------------------------------------
