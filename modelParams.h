/*
 * modelParams.h
 *
 *  Created on: Feb 26, 2010
 *      Author: wegmannd
 */

#ifndef MODELPARAMS_H_
#define MODELPARAMS_H_

#include "stringFunctions.h"
#include "TParameters.h"
#include "hmm.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "TLog.h"


class modelParams{
public:
	int nRefpop;
	TParameters* myInputfile;
	TLog* logfile;
	//parameter vectors for population unspecific parameters
	std::vector<int> timeSinceAdmixture;
	std::vector<double> miscopyMutation;
	std::vector<double> miscopyRate;
	//parameter arrays for population specific parameters
	double** ancestryProp;
	int nAncestryProp;
	bool individualSpecificAncestryProp;
	double** ancestralRate;
	int nAncestralRate;
	double** mutation;
	int nMutation;
	//arrays of pointers for all combinations of parameters
	int nCombinations;
	int** timeSinceAdmixtureP;
	double** miscopyMutationP;
	double** miscopyRateP;
	double** ancestryPropP;
	double** ancestralRateP;
	double** mutationP;
	bool parametercombinationsBuilt;

	modelParams(TParameters* MyInputfile, TLog * Logfile);
	~modelParams(){
		if(parametercombinationsBuilt){
			delete[] timeSinceAdmixtureP;
			delete[] miscopyMutationP;
			delete[] miscopyRateP;
			delete[] ancestryPropP;
			delete[] ancestralRateP;
			delete[] mutationP;
			for(int i=0; i<nAncestryProp; ++i) delete[] ancestryProp[i];
			delete[] ancestryProp;
			for(int i=0; i<nAncestralRate; ++i) delete[] ancestralRate[i];
			delete[] ancestralRate;
			for(int i=0; i<nMutation; ++i) delete[] mutation[i];
			delete[] mutation;


		}
	};
	int readPerPopParameter(std::string  name, double**& storage);
	int readIndividualSpecificPerPopParameter(std::string  filename, double**& storage);
	void buildParameterCombinations();
	void runAllParameterCombinations(hmm* myHmm, std::string outputPrefix, bool onlyLL, bool doComputeSwitchProb, snpDB* mySnpDB);
};


#endif /* MODELPARAMS_H_ */
