/*
 * snpDB.cpp
 *
 *  Created on: Feb 4, 2010
 *      Author: wegmannd
 */
#include "snpDB.h"

//---------------------------------------------------------------------------
snpDB::snpDB(std::string filename, TLog* logfile){
	clock_t start=clock();
	//read a bim file into the DB
	logfile->listFlush("Reading the SNPs from the bim file '" + filename + "' ...");
	std::ifstream file;
	file.open(filename.c_str());
	if(!file)  throw "bim file '" + filename + " could not be read!";


	long line=1;
	std::vector<std::string> val;
	while(file.good() && !file.eof()){
		fillVectorFromLineWhiteSpaceSkipEmpty(file, val);
		if(val.size()>0){
			if(val.size()!=6) throw "Wrong number of values in bim file '" + filename + " on line " + toString(line) + ".";
			DBwithID.insert(std::pair<long, snp*>(line, new snp(val[1], val[0], val[4].c_str()[0], val[5].c_str()[0], stringToLongCheck(val[3]), line, stringToDoubleCheck(val[2]))));
			DB.insert(std::pair<std::string,snp*>(val[1], getSNP(line)));
		}
		++line;
	}
	file.close();
	logfile->write("done (" + toString( (clock() - start)/CLOCKS_PER_SEC ) + "s)!");
	logfile->conclude("Read a total of " + toString(DB.size()) + " SNPs.");
}
//---------------------------------------------------------------------------
snp* snpDB::getSNP(std::string snpName){
	return DB.find(snpName)->second;
}
snp* snpDB::getSNP(long id){
	return DBwithID.find(id)->second;
}
//---------------------------------------------------------------------------
char snpDB::getAllele1(long id){
	return DBwithID.find(id)->second->allele1;
}
char snpDB::getAllele2(long id){
	return DBwithID.find(id)->second->allele2;
}
//---------------------------------------------------------------------------
std::string snpDB::getName(long id){
	return DBwithID.find(id)->second->name;
}
//---------------------------------------------------------------------------
bool snpDB::snpExists(std::string snpName){
	if(DB.find(snpName)==DB.end()) return false;
	else return true;
}
//---------------------------------------------------------------------------
long snpDB::getSnpId(std::string snpName){
	std::map<std::string, snp*>::iterator thisSnp=DB.find(snpName);
	if(thisSnp==DB.end()) return -1;
	else return thisSnp->second->id;
}
//---------------------------------------------------------------------------
long snpDB::getNumSnps(){
	return DB.size();
}
//---------------------------------------------------------------------------
void snpDB::fillSnpMap(std::map<long,long>* snpMap, std::string chr){
	snpMap->clear();
	std::map<long, snp*>::iterator snpIt;
	for(snpIt=DBwithID.begin();snpIt!=DBwithID.end();++snpIt){
		if(snpIt->second->chromosome==chr){
			snpMap->insert(std::pair<long, long>(snpIt->second->position, snpIt->second->id));
		}
	}
}
//---------------------------------------------------------------------------
void snpDB::print(){
	std::map<std::string, snp*>::iterator it;
	for(it=DB.begin(); it!=DB.end(); ++it) it->second->print();
}
