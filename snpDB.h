/*
 * snpDB.h
 *
 *  Created on: Feb 4, 2010
 *      Author: wegmannd
 */
#ifndef SNPDB_H_
#define SNPDB_H_

#include "stringFunctions.h"
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <time.h>
#include "TLog.h"

class snp{
public:
	std::string name, chromosome;
	char allele1, allele2;
	long position, id;
	float geneticPosition;

	snp(std::string Name, std::string Chromosome, char Allele1, char Allele2, long Position, long Id){
		name=Name;
		chromosome=Chromosome;
		allele1=Allele1;
		allele2=Allele2;
		position=Position;
		geneticPosition=0;
		id=Id;
	};
	snp(std::string Name, std::string Chromosome, char Allele1, char Allele2, long Position, long Id, float GeneticPosition){
		name=Name;
		chromosome=Chromosome;
		allele1=Allele1;
		allele2=Allele2;
		position=Position;
		geneticPosition=GeneticPosition;
		id=Id;
	};
	void print(){
		std::cout << name << "\t" << chromosome << "\t" << allele1 << "\t" << allele2 << "\t" << position << "\t" << geneticPosition << std::endl;
	}
	std::string getAllele1(){ return &allele1; };
	std::string getAllele2(){ return &allele2; };
};

class snpDB{
public:
	std::map<std::string, snp*> DB;
	std::map<long, snp*> DBwithID;

	snpDB(std::string filename, TLog* logfile);
	~snpDB(){
		for(std::map<long, snp*>::iterator snpIt=DBwithID.begin();snpIt!=DBwithID.end();++snpIt){
			delete snpIt->second;
		}
		DBwithID.clear();
	};
	snp* getSNP(std::string snpName);
	snp* getSNP(long id);
	char getAllele1(long id);
	char getAllele2(long id);
	std::string getName(long id);
	bool snpExists(std::string snpName);
	long getSnpId(std::string snpName);
	long getNumSnps();
	void fillSnpMap(std::map<long,long>* snpMap, std::string chr);
	void print();

};



#endif /* SNPDB_H_ */
